package butcheredpig.tests;

import static net.hypki.libs5.utils.reflection.SystemUtils.getResourceURL;

import java.io.IOException;

import org.junit.Test;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.Store;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.unittests.connectors.ConnectorTestCase;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class AssertLargeUnitTest extends ButcheredPigTestCase {
	
	
	@Test
	public void testAssertOK2Csv() throws IOException, ValidationException {
		ButcheredPigScript bp = new ButcheredPigScript()
			.setUserId(UUID.random())
			.setNotebookId(UUID.random())
			.setToParse(compileButcheredPigScript("assertCsv-ok2.bpig"))
			.save();
		
		runAllJobs();
		
		bp = ButcheredPigScriptFactory.getButcheredPigScript(bp.getId());
		
		LibsLogger.info(AssertLargeUnitTest.class, "ButcheredPigScript status " + bp.getProgress());
		
		assertTrue(bp.getProgress().getStatus() == Status.DONE, "ButcheredPig status expected " + Status.DONE);
		
		printStore(bp);
	}

}
