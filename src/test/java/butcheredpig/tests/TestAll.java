package butcheredpig.tests;

import static net.hypki.libs5.utils.LibsLogger.info;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butcheredpig.ButcheredPigMain;
import butcheredpig.Init;
import butcheredpig.Settings;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.plugins.ConnectorUtils;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class TestAll {
	
	private static List<UUID> startedBPScripts = new ArrayList<>();

	public static void main(String[] args) throws IOException, ValidationException {
		LibsLogger.info(TestAll.class, "Searching for ", "script.bpig", " in ", new FileExt().getAbsolutePath());

		Init.init(args);
		
		// TODO DEBUG 
//		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE_LOWERCASE, Chunk.COLUMN_FAMILY);
//		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE_LOWERCASE, Job.COLUMN_FAMILY);
		
		// searching for all script.bpig files (recursively)
		for (FileExt script : new LazyFileIterator(new FileExt(), true, true, false, pathname -> {
			if (pathname.isDirectory())
				return true;
			return pathname.getName().equals("script.bpig") && !pathname.getAbsolutePath().contains("/target/") ;
		})) {
			startedBPScripts.add(runTestCase(new FileExt(script.getDirectoryOnly())));
		}
		
		// checking for all scripts to finish
		boolean allFinished = false;
		while (!allFinished) {
			allFinished = true;
			
			for (UUID bpId : startedBPScripts) {
				ButcheredPigScript bps = ButcheredPigScriptFactory.getButcheredPigScript(bpId);
				
				if (!bps.isFinished()) {
					bps.run();

					allFinished = false;
				}
			}
			
			try {
				JobsManager.runAllJobs(Init.getAllChannelsNames());
				
				Thread.sleep(Settings.SCRIPT_BUTCHEREDPIG_SLEEP_MS);
			} catch (Exception e) {
				LibsLogger.error(ButcheredPigMain.class, "Cannot sleep", e);
				break;
			}
		}
		
		// validate the output files
		LibsLogger.info(TestAll.class, "====================== VALIDATION ======================");
		for (UUID bpId : startedBPScripts) {
			ButcheredPigScript bps = ButcheredPigScriptFactory.getButcheredPigScript(bpId);
			for (FileExt f : new LazyFileIterator(bps.getContext().getWorkingDirFileExt(),
					false, 
					true, 
					false, 
					pathname -> pathname.getAbsolutePath().endsWith("-test.csv"))) {
				String base = f.getFilenameOnly().substring(0, f.getFilenameOnly().lastIndexOf("-test.csv"));
				FileExt out = new FileExt(bps.getContext().getWorkingDir(), base + "-out.csv");
				
				boolean thesame = ConnectorUtils.theSame(new PlainConnector(out), new PlainConnector(f));
				if (!thesame) {
					bps.getProgress().fail("File " + out + " and " + f + " are not the same");
					bps.save();
				} else {
					LibsLogger.info(TestAll.class, "File " + out + " and " + f + " are the same");
				}
			}
		}
		
		// all scripts are finished
		LibsLogger.info(TestAll.class, "======================= SUMMARY =======================");
		List<String> failed = new ArrayList<>();
		for (UUID bpId : startedBPScripts) {
			ButcheredPigScript bps = ButcheredPigScriptFactory.getButcheredPigScript(bpId);
			LibsLogger.info(TestAll.class, bps.toString());
			if (bps.getProgress().isFailed())
				failed.add(bps.getContext().getWorkingDir());
		}
		if (failed.size() > 0) {
			LibsLogger.info(TestAll.class, "======================= FAILED =======================");
			failed.stream().forEach(f -> info(TestAll.class, "FAILED " + f));
		} else {
			info(TestAll.class, "No failed jobs");
		}
		
		// closing
		Init.shutdown();
		LibsLogger.info(TestAll.class, "Finished!");
	}
	
	private static UUID runTestCase(FileExt folder) throws ValidationException, IOException {
		LibsLogger.info(TestAll.class, "Running Test Case in " + folder.getAbsolutePath());
		
		User user = UserFactory.getUser("arkadiusz@hypki.net");
		
		String script = new FileExt(folder, "script.bpig").readString();
		script = compile(script, folder);
		
		ButcheredPigScript bp = new ButcheredPigScript()
				.setUserId(user.getUserId())
				.setNotebookId(UUID.random())
				.setToParse(script);
		
		bp
			.getContext()
			.setWorkingDir(folder.getAbsolutePath());
		
		bp.save();
		
		bp.run();
		
		return bp.getId();
	}
	
	private static String compile(String bpig, FileExt folder) {
		while (true) {
			String gr = RegexUtils.firstGroup("\\'([\\w\\d\\-\\_]+\\.csv)\\'", bpig);
			
			if (nullOrEmpty(gr))
				break;
			
			if (new FileExt(gr).exists()) {
				// this file exists on the disk, just change to absolute path
				bpig = bpig.replace(gr, new FileExt(gr).getAbsolutePath());
				continue;
			}
			
			bpig = bpig.replace(gr, new FileExt(folder.getAbsolutePath(), gr).getAbsolutePath());
		}
		return bpig;
	}
}
