package butcheredpig.tests.connectors;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import butcheredpig.tests.ButcheredPigTestCase;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;

public class ConnectorWithBagsTestCase extends ButcheredPigTestCase {

	@Test
	public void testBag() throws IOException {
		final UUID h5Id = UUID.random();
		H5Connector h5 = new H5Connector(h5Id, new File("withBags.h5"));
		
		h5
			.setColumnDefs(new ColumnDefList()
						.addColumnDef(new ColumnDef("a", null, ColumnType.INTEGER))
						.addColumnDef(new ColumnDef("b", null, ColumnType.INTEGER))
						.addColumnDef(new ColumnDef("c", null, ColumnType.STRING))
						);
		
		
		H5Connector bag = new H5Connector(h5Id, new File("bag.h5"));
		bag
			.setColumnDefs(new ColumnDefList()
						.addColumnDef(new ColumnDef("country", null, ColumnType.STRING))
						.addColumnDef(new ColumnDef("population", null, ColumnType.INTEGER))
						);
		Row r = new Row(UUID.random());
		r.set("country", "PL");
		r.set("population", 35);
		bag.write(r);
		r = new Row(UUID.random());
		r.set("country", "DE");
		r.set("population", 70);
		bag.write(r);
		bag.close();
		
		r = new Row(UUID.random());
		r.set("a", 5);
		r.set("b", 15);
		r.set("c", bag.getInitParams().toString());
		
		h5.write(r);
		h5.close();
		
		for (Row r1 : h5.iterateRows(null))
			LibsLogger.info(ConnectorWithBagsTestCase.class, "Row: " + r1);
		
		LibsLogger.info(ConnectorWithBagsTestCase.class, "H5 connector init params ", h5.getInitParams());
		
		h5.clear();
	}
}
