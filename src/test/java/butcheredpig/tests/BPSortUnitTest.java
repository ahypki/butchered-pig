package butcheredpig.tests;

import static net.hypki.libs5.weblibs.WeblibsConst.CHANNEL_NORMAL;

import java.io.IOException;

import org.junit.Test;

import butcheredpig.chunks.ChunkFactory;
import butcheredpig.chunks.Status;
import butcheredpig.commands.Foreach;
import butcheredpig.commands.Order;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.unittests.connectors.ConnectorTestCase;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.jobs.JobsManager;

public class BPSortUnitTest extends ConnectorTestCase {
	
//	@Test
	public void XtestSave() throws ValidationException, IOException {
		FileExt h5File = new FileExt("./test.h5");
		
		Order sort = new Order();
//		sort.setSourceConnector(new H5Connector(UUID.random(), h5File)); // TODO implement it
		sort.save();
		
		Order fromDb = (Order) ChunkFactory.getChunk(sort.getId(), Order.class);
		
		assertTrue(fromDb != null, "BP not found in DB");
	}

//	@Test
	public void testSorting() {
		Foreach bp = new Foreach();
		
//		bp.setSource(source);
		
//		BP sorted = bp.sort(null);
		
//		Double lastValue = null;
//		for (Row row : sorted) {
//			if (lastValue != null)
//				assertTrue(row.getAsDouble("sm1") >= lastValue, "Sorted failed");
//			
//			lastValue = row.getAsDouble("sm1");
//		}
		
		LibsLogger.info(BPSortUnitTest.class, "Finished");
	}
	
	@Test
	public void testSorting2() throws IOException, InterruptedException, ValidationException {
		final int nrOfWorkers = 4;
		final int nrOfRowsToSort = 56_480_131;
//		final int nrOfRowsToSort = 1_000_000;
		
//		final FileExt h5File = new FileExt("./mocca.h5");
		final FileExt h5File = new FileExt("toSort.h5");
		final H5Connector conn = new H5Connector(UUID.random(), h5File);
		if (h5File.exists()) {
			// do nothing, just read
//			h5File.deleteIfExists();
//			writeRows(conn, nrOfRowsToSort);
		} else {			
			// create test data
			writeRows(conn, nrOfRowsToSort);
		}
		
		new FileExt("bpTemp").mkdirs();
		
		// sum c1 column
		int c1Sum = 0;
		for (Row row : conn.iterateRows(null)) {
			c1Sum += row.getAsInt("c1");
		}
		
		// start Jobs workers
		for (int i = 0; i < nrOfWorkers; i++) {			
			JobsManager.init(CHANNEL_NORMAL);
		}
		
		Thread.sleep(5000);
		Watch w = new Watch();
		
		// sort in ButcheredPig
		Order sort = new Order();
//		sort.setSourceConnector(conn); // TODO implement it
		sort.getSortBy().add("c1");
		sort.save();
		
		while (true) {
			sort = (Order) ChunkFactory.getChunk(sort.getId(), Order.class);
			
			if (sort.getProgress().getStatus() == Status.DONE)
				break;
			
			LibsLogger.debug(BPSortUnitTest.class, "Sort ", sort.getId(), " not done yet, sleeping..");
			Thread.sleep(1000);
		}
		
		LibsLogger.info(BPSortUnitTest.class, "Finished in ", w.toString(), " with ", nrOfWorkers, " workers");
		
		// checking if the connector is sorted, checking the c1 sum
		long readRows = 0;
		Row lastRow = null;
		int c1SumCheck = 0;
		for (Row row : sort.iterateOutputRows(null, 0, 1)) {
			if (lastRow != null)
				if (row.getAsInt("c1") < lastRow.getAsInt("c1")) {
					LibsLogger.info(BPSortUnitTest.class, "break");
				}
			
			if (lastRow != null)
				assertTrue(row.getAsInt("c1") >= lastRow.getAsInt("c1"), 
						"It is not sorted");
			
			c1SumCheck += row.getAsInt("c1");
			
			lastRow = row;
			readRows++;
		}
		
		assertTrue(readRows > 0, "No rows read");
		assertTrue(c1Sum == c1SumCheck, "Sum is different " + c1Sum + " vs. " + c1SumCheck);
				
		// wait to all jobs be done
		while (JobsManager.getJobsCount(CHANNEL_NORMAL) > 0) {
			LibsLogger.info(BPSortUnitTest.class, "Not all jobs are done, sleeping..");
			Thread.sleep(1000);
		}
		
	}
}
