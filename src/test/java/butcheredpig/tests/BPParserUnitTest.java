package butcheredpig.tests;

import java.io.IOException;

import org.junit.Test;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.string.TrickyString;

public class BPParserUnitTest extends BeansTestCase {

	@Test
	public void testParser() throws IOException, ValidationException {
		final String content = SystemUtils.readFileContent(BPParserUnitTest.class, "butcheredpig/toParse1.pig");
		
		ButcheredPigScript script = new ButcheredPigScript(content, UUID.random());
		
		for (Chunk bp : script)
			LibsLogger.info(BPParserUnitTest.class, "BP meat: " + bp);
		
//		EscapeString wholeLine = new EscapeString(content)
//				.escapeRegex("'(.*)'")
//				.escapeRegex("(\".*\")");
//		
//		TrickyString all = new TrickyString(wholeLine.getEscapedText());
				
//		LibsLogger.info(BPParserUnitTest.class, all.getLine());
		
		LibsLogger.info(BPParserUnitTest.class, "Finished");
	}
}
