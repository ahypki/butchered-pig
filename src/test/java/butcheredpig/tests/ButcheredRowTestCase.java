package butcheredpig.tests;

import org.junit.Test;

import butcheredpig.commands.utils.Bag;
import butcheredpig.commands.utils.ButcheredRow;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;

public class ButcheredRowTestCase {

	@Test
	public void testRow1() {
		ButcheredRow br = new ButcheredRow(UUID.random(), null);
		
		br.addColumn("country", "Poland");
		br.addColumn("population", 38);
		
		LibsLogger.info(ButcheredRowTestCase.class, "ButcheredRow: " + br);
	}
	
	@Test
	public void testRow2() {
		ButcheredRow br = new ButcheredRow(UUID.random(), null);
		
		br.addColumn("country", "Poland");
		br.addColumn("population", 38);
		br.addColumn("people", new Bag());
		
		LibsLogger.info(ButcheredRowTestCase.class, "ButcheredRow: " + br);
	}
}
