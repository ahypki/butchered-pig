package butcheredpig.tests;

import static net.hypki.libs5.utils.reflection.SystemUtils.getResourceURL;

import java.io.IOException;

import org.junit.Test;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import butcheredpig.commands.Store;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.unittests.connectors.ConnectorTestCase;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class DistinctUnitTest extends ButcheredPigTestCase {

	@Test
	public void testDistinct() throws IOException, ValidationException {
		final UUID userId = UUID.random();
		final UUID tableId = UUID.random();
		final UUID notebookId = UUID.random();
		final FileExt toDistintFile = new FileExt("toDistinct.h5");
		
		toDistintFile.deleteIfExists();
		
		H5Connector countries = new H5Connector(tableId, toDistintFile);
		countries.setColumnDefs(new ColumnDefList()
								.addColumnDef(new ColumnDef("name", null, ColumnType.STRING))
								.addColumnDef(new ColumnDef("citizens", null, ColumnType.DOUBLE)));
		countries.write(new Row(UUID.random().getId())
							.addColumn("name", "Poland")
							.addColumn("citizens", 38.0));
		countries.write(new Row(UUID.random().getId())
							.addColumn("name", "Germany")
							.addColumn("citizens", 78.0));
		countries.write(new Row(UUID.random().getId())
							.addColumn("name", "Poland")
							.addColumn("citizens", 38.0));
		countries.close();
		
		for (Row row : countries.iterateRows(null)) {
			LibsLogger.info(DistinctUnitTest.class, "H5 source row: " + row);
		}
		
		ButcheredPigScript bp = new ButcheredPigScript()
			.setUserId(userId)
			.setNotebookId(notebookId)
			.setToParse(SystemUtils.readFileContent(BPParserUnitTest.class, "butcheredpig/distinctH5.bpig"))
			.save();
		
		runAllJobs();
		
		bp = ButcheredPigScriptFactory.getButcheredPigScript(bp.getId());
		
		LibsLogger.info(DistinctUnitTest.class, "ButcheredPigScript status " + bp.getProgress());
		
		printStore(bp);
	}
	
	@Test
	public void testDistinctCsv() throws IOException, ValidationException {
		ButcheredPigScript bp = new ButcheredPigScript()
			.setUserId(UUID.random())
			.setNotebookId(UUID.random())
			.setToParse(compileButcheredPigScript("distinctCsv.bpig"))
			.save();
		
		runAllJobs();
		
		bp = ButcheredPigScriptFactory.getButcheredPigScript(bp.getId());
		
		LibsLogger.info(DistinctUnitTest.class, "ButcheredPigScript status " + bp.getProgress());
		
		printStore(bp);
	}
}
