package butcheredpig.tests;

import static net.hypki.libs5.utils.reflection.SystemUtils.getResourceURL;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.commands.Store;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.unittests.connectors.ConnectorTestCase;
import net.beanscode.pojo.tables.ColumnDef;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class ButcheredPigTestCase extends ConnectorTestCase {
	
	protected void createCsvFile(FileExt outputFile,
			int nrRows,
			ColumnDefList colDefList) throws IOException {
		FileWriter fw = new FileWriter(outputFile);
		
		// printing header
		fw.append("# ");
		for (net.beanscode.model.plugins.ColumnDef columnDef : colDefList) {
			fw.append(" " + columnDef.getName());
		}
		
		for (int i = 0; i < nrRows; i++) {
			fw.append("\n");
			for (net.beanscode.model.plugins.ColumnDef columnDef : colDefList) {
				if (columnDef.getType() == ColumnType.DOUBLE) {
					fw.append("" + RandomUtils.nextDouble());
				} else if (columnDef.getType() == ColumnType.INTEGER) {
					fw.append("" + RandomUtils.nextInt(1_000_000));
				} else if (columnDef.getType() == ColumnType.LONG) {
					fw.append("" + RandomUtils.nextInt(10_000));
				} else
					throw new IOException("Cannot generate CSV file, unimplemented type " + columnDef);
			}
		}
		
		fw.close();
	}

	protected String compileButcheredPigScript(String scriptName) {
		String bpig = SystemUtils
				.readFileContent(BPParserUnitTest.class, "butcheredpig/" + scriptName);
		
		while (true) {
			String gr = RegexUtils.firstGroup("\\'([\\w\\d\\-\\_]+\\.csv)\\'", bpig);
			
			if (nullOrEmpty(gr))
				break;
			
			if (new FileExt(gr).exists()) {
				// this file exists on the disk, just change to absolute path
				bpig = bpig.replace(gr, new FileExt(gr).getAbsolutePath());
				continue;
			}
			
			bpig = bpig.replace(gr, 
					new FileExt(getResourceURL(BPParserUnitTest.class, "butcheredpig/" + gr).getFile()).getAbsolutePath());
		}
		
		return bpig;
	}
	
	/**
	 * Prints all output tables which were created with STORE command.
	 * 
	 * @param bp
	 * @throws IOException
	 * @throws ValidationException
	 */
	protected void printStore(ButcheredPigScript bp) throws IOException, ValidationException {
		for (Chunk chunk : bp) {
			if (chunk instanceof Store) {
				Store store = (Store) chunk;
				for (Row row : store.iterateOutputRows(null, 0, 1))
					LibsLogger.info(ButcheredPigTestCase.class, store.getDestName() + ": " + row);
			}
		}
	} 
}
