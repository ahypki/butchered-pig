package butcheredpig.tests;

import static net.hypki.libs5.utils.reflection.SystemUtils.getResourceURL;

import java.io.IOException;
import java.util.Iterator;

import org.junit.Test;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.Join;
import butcheredpig.commands.Store;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.unittests.connectors.ConnectorTestCase;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class JoinUnitTest extends ButcheredPigTestCase {
	
	@Test
	public void testGroupCsv1() throws IOException, ValidationException {
		ButcheredPigScript bp = new ButcheredPigScript()
			.setUserId(UUID.random())
			.setNotebookId(UUID.random())
			.setToParse(compileButcheredPigScript("joinCsv1.bpig"))
			.save();
		
		bp.run();
		
		while (bp.getProgress().getStatus() != Status.DONE
				&& bp.getProgress().getStatus() != Status.FAILED) {
			bp = ButcheredPigScriptFactory.getButcheredPigScript(bp.getId());
			
			runAllJobs(1000);
			
			LibsLogger.info(JoinUnitTest.class, "ButcheredPigScript status " + bp.getProgress());
		}
		
		assertTrue(bp.getProgress().getStatus() == Status.DONE, "ButcheredPig status expected " + Status.DONE + 
				" but is " + bp.getProgress());
		
		for (Chunk chunk : bp) {
			if (chunk instanceof Join) {
				Join j = (Join) chunk;
				for (Row row : j.iterateOutputRows(null, 0, 1)) {
					LibsLogger.info(JoinUnitTest.class, "JOIN " + row);
				}
			}
		}
	}

	
	
	
}
