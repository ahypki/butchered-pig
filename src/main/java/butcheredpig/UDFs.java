package butcheredpig;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;

import net.beanscode.plugin.pig.udfs.DsId;
import net.beanscode.plugin.pig.udfs.Floor;
import net.beanscode.plugin.pig.udfs.Pow;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.utils.reflection.ReflectionUtility;

public class UDFs {
	
	private static Map<String, EvalFunc> nameToEvalFunc = new HashMap<>();
	
	static {
		// default UDFs
		try {
			putEvalFunc("FLOOR", Floor.class.getName());
			putEvalFunc("DSID", DsId.class.getName());
			putEvalFunc("POW", Pow.class.getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Iterable<String> iterateShortNames() {
		return nameToEvalFunc.keySet();
	}

	public static void putEvalFunc(String shortname, String fullname) throws IOException {
		Class evalClass = ReflectionUtility.findClass(fullname);
		
		if (evalClass == null)
			throw new IOException("Cannot find UDF EvalFunc class " + fullname);
		
		try {
			nameToEvalFunc.put(shortname, (EvalFunc) evalClass.newInstance());
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IOException("Cannot create an instance of UDF EvalFunc class " + fullname);
		}
	}

	public static EvalFunc getEvalFunc(String shortname) {
		return nameToEvalFunc.get(shortname);
	}

	public static ColumnType pigTypeToColumnType(byte pigType) {
		if (pigType == DataType.LONG)
			return ColumnType.LONG;
		
		else if (pigType == DataType.INTEGER)
			return ColumnType.INTEGER;
        
		else if (pigType == DataType.CHARARRAY)
        	return ColumnType.STRING;
        
		else if (pigType == DataType.DOUBLE)
        	return ColumnType.DOUBLE;
        
		else
        	throw new NotImplementedException("Pig column type " + pigType 
        			+ " not supported " + DataType.findTypeName(pigType));
	}
}
