package butcheredpig;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.file.FileExt;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Context {

	@Expose
	@NotNull
	@AssertValid
	private String workingDir = null;
	
	public Context() {
		
	}

	public String getWorkingDir() {
		return workingDir;
	}
	
	public FileExt getWorkingDirFileExt() {
		return new FileExt(workingDir);
	}

	public Context setWorkingDir(String workingDir) {
		this.workingDir = workingDir;
		return this;
	}
}
