package butcheredpig.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import butcheredpig.commands.utils.ButcheredRow;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Bracket extends Term {
	
	@Expose
	@NotEmpty
	@NotNull
	private List<Term> terms = null;

	public Bracket() {
		
	}
	
	public Bracket(TrickyString toParse, TermRequirement termReq) throws IOException {
		setTerms(Query.parse(toParse, true));
		setRequirement(termReq);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
//		sb.append("[" + getRequirement() + "]");
		sb.append("(");
		int t = 0;
		for (Term term : getTerms()) {
			sb.append(term.toString(t++ > 0));
		}
		sb.append(")");
		return sb.toString();
	}

	public List<Term> getTerms() {
		if (terms == null)
			terms = new ArrayList<>();
		return terms;
	}

	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}
	
	@Override
	public boolean isFulfilled(ButcheredRow row) {
		Boolean oneShouldFulfilled = null;
		for (Term term : getTerms()) {
			if (term.getRequirement() == TermRequirement.MUST
					&& term.isFulfilled(row) == false)
					return false;
			else if (term.getRequirement() == TermRequirement.MUST_NOT 
					&& term.isFulfilled(row))
				return false;
			else if (term.getRequirement() == TermRequirement.SHOULD) {
				if (oneShouldFulfilled == null)
					oneShouldFulfilled = false;
				if (term.isFulfilled(row) == true)
					oneShouldFulfilled = true;
			} 
		}
		
		if (oneShouldFulfilled != null)
			return oneShouldFulfilled;
		
		return true;
	}
}
