package butcheredpig.query;

import static net.hypki.libs5.search.query.TermRequirement.MUST;
import static net.hypki.libs5.search.query.TermRequirement.SHOULD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import butcheredpig.commands.utils.ButcheredRow;
import kotlin.NotImplementedError;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Query {
	
	@Expose
	@NotEmpty
	@NotNull
	private List<Term> terms = null;

	public Query() {
		
	}
	
	public Query(String toParse) throws IOException {
		setTerms(parse(new TrickyString(toParse.trim()), false));
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int t = 0;
		for (Term term : getTerms()) {
			sb.append(term.toString(t++ > 0));
		}
		return sb.toString();
	}

	public static List<Term> parse(TrickyString ts, boolean exitOnParenthesis) throws IOException {		
		List<Term> terms = new ArrayList<>();
		String lastLine = ts.checkLine();
		TermRequirement termRequirement = MUST;
		
		while (!ts.isEmpty()) {
			String word = null;
			
			if (ts.checkNextPrintableCharacter().equals(")") && exitOnParenthesis)
				break;
			
			ts.trim();
			if (word == null
					&& !ts.checkNextPrintableCharacter().equals("("))
				word = ts.checkNextMatch(TrickyString.REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS);
			
			if (word != null) {
				if (word.equalsIgnoreCase("and")) {
					termRequirement = MUST;
					ts.getNextMatch(TrickyString.REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS);
					
					if (terms.size() == 1)
						terms.get(0).setRequirement(termRequirement);
					
					continue;
				} else if (word.equalsIgnoreCase("or")) {
					termRequirement = SHOULD;
					ts.getNextMatch(TrickyString.REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS);
					
					if (terms.size() == 1)
						terms.get(0).setRequirement(termRequirement);
					
					continue;
				} else if (word.equalsIgnoreCase("not")) {
					termRequirement = TermRequirement.MUST_NOT;
					ts.getNextMatch(TrickyString.REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS);
					continue;
				}
			}
				
			if (ts.checkNextPrintableCharacter().equals("(")) {
				// parsing bracket
				ts.expectedPrintableCharacter("(");
				terms.add(new Bracket(ts, termRequirement));
				if (ts.checkNextPrintableCharacter() != null && ts.checkNextPrintableCharacter().equals(")"))
					ts.expectedPrintableCharacter(")");
			} else if (ts.checkNextPrintableCharacter().equals(")")) {
				if (exitOnParenthesis) {
					break;
				} else {
					ts.expectedPrintableCharacter(")");
				}
			} else {
				// parsing term
				terms.add(new Term(ts).setRequirement(termRequirement));
			}
		
			if (lastLine.equals(ts.checkLine()))
				throw new IOException("Cannot parse in line: " + lastLine);
		}
		
		return terms;
	}

	public List<Term> getTerms() {
		if (terms == null)
			terms = new ArrayList<>();
		return terms;
	}

	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}

	public boolean isFullfilled(ButcheredRow r) {
		try {
			Boolean fulfilledAtLeastOne = null;
			for (Term term : getTerms()) {
				if (term.getRequirement() == TermRequirement.MUST) {
					if (!term.isFulfilled(r)) {
						return false;
					}
				} else if (term.getRequirement() == TermRequirement.MUST_NOT) {
					if (term.isFulfilled(r)) {
						return false;
					}
				} else {
					if (term.isFulfilled(r)) {
						fulfilledAtLeastOne = true;
					} else if (fulfilledAtLeastOne == null) {
						fulfilledAtLeastOne = false;
					}
				}
			}
			return fulfilledAtLeastOne != null ? fulfilledAtLeastOne : true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
