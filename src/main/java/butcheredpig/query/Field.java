package butcheredpig.query;

import static butcheredpig.Const.AS;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import butcheredpig.Const;
import butcheredpig.chunks.Chunk;
import jnr.ffi.provider.jffi.NumberUtil;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.utils.math.Expression;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Field {
	
	@Expose
//	@NotEmpty
//	@NotNull
	@AssertValid
	private String name = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	@AssertValid
	private Expression expression = null;
	
//	@Expose
//	@NotEmpty
//	@NotNull
//	private String newName = null;
//	
//	@Expose
//	@NotEmpty
//	@NotNull
//	private ColumnType type = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	@AssertValid
	private ColumnDefList columnList = null;
	
	@Expose
	@AssertValid
	private boolean asterisk = false;

	public Field() {
		
	}
	
	public Field(String toParse) throws IOException {
		try {
			if (toParse.matches("[\\s]*\\w[\\w\\d\\:\\.]*[\\s]*")) {
				// simple name
				setName(toParse);
			} else if (toParse.matches("[\\s]*\\w[\\w\\d\\:\\.]*[\\s]+" + AS.toLowerCase() + "[\\s]+\\w[\\w\\d\\:\\.]*[\\s]*")) {
				// name -> new name
				TrickyString ts = new TrickyString(toParse);
				setName(ts.getNextStringUntil(AS.toLowerCase(), false));
				ts.expectedAlphanumericWord(AS.toLowerCase());
				
				getColumnList().add(new ColumnDef(ts.getNextAlphanumericWord(), "", ColumnType.UNKNOWN));
			} else if (toParse.toLowerCase().trim().matches(".*" + AS.toLowerCase() + "[\\s]+[\\(\\)\\w\\d\\_\\:\\,\\s]+[\\s]*")) {
				// name AS newName:type
				int asIdx = toParse.toLowerCase().indexOf("as");
				
				setExpression(new Expression(toParse.substring(0, asIdx - 1)));
				
				TrickyString ts = new TrickyString(toParse.substring(asIdx + 2));
				
				// (
				if (ts.checkNextPrintableCharacter() != null
						&& ts.checkNextPrintableCharacter().equals("("))
					ts.expectedPrintableCharacter("(");
				
				while (!ts.isEmpty()) {
					String name = ts.getNextAlphanumericWordWithUnderscore();
					
					ColumnType type = ColumnType.UNKNOWN;
					if (ts.checkNextPrintableCharacter() != null && ts.checkNextPrintableCharacter().equals(":")) {
						ts.expectedPrintableCharacter(":");
						type = ColumnType.parse(ts.getNextAlphanumericWordWithUnderscore());
					}
					
					getColumnList().add(new ColumnDef(name.trim(), "", type));
					
					if (ts.checkNextPrintableCharacter() != null
							&& ts.checkNextPrintableCharacter().equals(","))
						ts.expectedPrintableCharacter(",");
					
					if (ts.checkNextPrintableCharacter() != null
							&& ts.checkNextPrintableCharacter().equals(")"))
						break;
				}

				// )
				if (ts.checkNextPrintableCharacter() != null
						&& ts.checkNextPrintableCharacter().equals(")"))
					ts.expectedPrintableCharacter(")");
			} else {
				// math expression
				setExpression(new Expression(toParse));
//				throw new NotImplementedException("not implemented: " + toParse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Cannot parse: " + toParse + " (" + e.getMessage() + ")");
		}
		
		// addiitonal properties
		if (getExpression() != null && getExpression().toString().equals("*"))
			setAsterisk(true);
	}
	
	@Override
	public String toString() {
		return isName() ? getName() : getExpression().toString();
	}
	
	public boolean isName() {
		return getName() != null;
	}
	
	public boolean isExpression() {
		return !isName();
	}
	
	public boolean isFunction() {
		if (!isExpression())
			return false;
		
		return toString().indexOf("(") > 0;
	}

	public Expression getExpression() {
		return expression;
	}

	public void setExpression(Expression expression) {
		this.expression = expression;
	}

//	public ColumnType getType() {
//		return type;
//	}
//
//	public void setType(ColumnType type) {
//		this.type = type;
//	}
//
//	public String getNewName() {
//		return newName;
//	}
//
//	public void setNewName(String newName) {
//		this.newName = newName != null ? newName.trim() : newName;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name != null ? name.trim() : name;
	}

	public ColumnDefList getColumnList() {
		if (columnList == null)
			columnList = new ColumnDefList();
		return columnList;
	}

	public void setColumnList(ColumnDefList columnList) {
		this.columnList = columnList;
	}
	
	public boolean isSUM() {
		if (isExpression()) {
			if (getExpression().isMathExpression()) {
				String lower = getExpression().getMathExpr().toLowerCase();
				if (lower.startsWith(Const.SUM.toLowerCase() + "("))
					return true;
			}
		}
		return false;
	}
	
	public boolean isMIN() {
		if (isExpression()) {
			if (getExpression().isMathExpression()) {
				String lower = getExpression().getMathExpr().toLowerCase();
				if (lower.startsWith(Const.MIN.toLowerCase() + "("))
					return true;
			}
		}
		return false;
	}
	
	public boolean isMAX() {
		if (isExpression()) {
			if (getExpression().isMathExpression()) {
				String lower = getExpression().getMathExpr().toLowerCase();
				if (lower.startsWith(Const.MAX.toLowerCase() + "("))
					return true;
			}
		}
		return false;
	}

	public boolean isAggregate() {
		if (isExpression()) {
			if (getExpression().isMathExpression()) {
				String lower = getExpression().getMathExpr().toLowerCase();
				if (lower.startsWith(Const.AVG.toLowerCase() + "("))
					return true;
				else if (lower.startsWith(Const.COUNT.toLowerCase() + "("))
					return true;
				else if (lower.startsWith(Const.MAX.toLowerCase() + "("))
					return true;
				else if (lower.startsWith(Const.MIN.toLowerCase() + "("))
					return true;
				else if (lower.startsWith(Const.SUM.toLowerCase() + "("))
					return true;
			}
		}
		return false;
	}

	public boolean isFlatten() {
		return nullOrEmpty(getName()) 
				&& getColumnList().size() > 0
				&& getExpression().toString().toLowerCase().contains(Const.FLATTEN.toLowerCase());
	}
	
	public FieldFlatten asFieldFlatten(Chunk parentChunk) {
		return new FieldFlatten(this, parentChunk);
	}

	public String getFunctionName() {
		try {
			String evalName = getExpression().getMathExpr();
			return evalName.substring(0, evalName.indexOf("("));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String getFunctionArguments() {
		String evalName = getExpression().getMathExpr();
		evalName = evalName.substring(evalName.indexOf("("));
		return evalName.substring(0, evalName.lastIndexOf(")"));
	}

	public String getNewName() {
		if (!isFlatten() && columnList != null && columnList.size() == 1)
			return columnList.get(0).getName();
		return getName();
	}

	public Class determineType() {
		// detrmining type based on the "name", which can be value
		if (getName() != null) {
			if (NumberUtils.isDouble(getName()))
				return Double.class;
			else if (NumberUtils.isInt(getName()))
				return Integer.class;
		}
		return null;
	}

	public boolean isAsterisk() {
		return asterisk;
	}

	public void setAsterisk(boolean asterisk) {
		this.asterisk = asterisk;
	}
}
