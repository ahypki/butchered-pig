package butcheredpig.query;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.math.BigDecimal;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import butcheredpig.commands.utils.ButcheredRow;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.search.query.TermComparator;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.math.Expression;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Term {

	@Expose
	@NotEmpty
	@NotNull
	private Expression left = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private Expression right = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private TermComparator comparator = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private TermRequirement requirement = null;
	
	public Term() {
		
	}
	
	public Term(TrickyString ts) {
		if (ts.checkNextAlphabeticalWord() != null
				&& ts.checkNextAlphabeticalWord().equals("or")) {
			setRequirement(TermRequirement.SHOULD);
			ts.expectedAlphanumericWord("or");
		} else if (ts.checkNextAlphabeticalWord() != null
				&& ts.checkNextAlphabeticalWord().equals("and")) {
			setRequirement(TermRequirement.MUST);
			ts.expectedAlphanumericWord("and");
		}
		
		setLeft(new Expression(ts.getNextStringUntil("[<>=]+")));
		
		setComparator(TermComparator.fromString(ts.getNextMatch("[<>=]+")));
		
		int m = Integer.MAX_VALUE;
		String line = ts.checkLine();
		m = line.indexOf("(") >= 0 ? min(line.indexOf("("), m) : m;
		m = line.indexOf(")") >= 0 ? min(line.indexOf(")"), m) : m; 
		m = line.indexOf(" and ") >= 0 ? min(line.indexOf(" and "), m) : m;
		m = line.indexOf(" or ") >= 0 ? min(line.indexOf(" or "), m) : m;
		
		if (m == Integer.MAX_VALUE) {
			setRight(new Expression(ts.getLine()));
		} else {
			String s =  line.substring(0, m).trim();
		
			ts.getNextStringUntil(s, true);
			
			setRight(new Expression(s));
		}
		
		if (getRight().toString().equals("a+7 OR"))
			System.out.println("kurwa");
	}
	
	@Override
	public String toString() {
		return getLeft() + " " 
				+ (getComparator() != null ? getComparator().toStringHuman() : "") + " " 
				+ getRight();
	}
	
	public String toString(boolean withRequirement) {
		return (withRequirement && getRequirement() != null ? getRequirement().toStringHuman() + " " : "") 
				+ toString();
	}

	public TermComparator getComparator() {
		return comparator;
	}

	public void setComparator(TermComparator comparator) {
		this.comparator = comparator;
	}

	public Expression getLeft() {
		return left;
	}

	public void setLeft(Expression left) {
		this.left = left;
	}

	public Expression getRight() {
		return right;
	}

	public void setRight(Expression right) {
		this.right = right;
	}

	public TermRequirement getRequirement() {
		return requirement;
	}

	public Term setRequirement(TermRequirement requirement) {
		this.requirement = requirement;
		return this;
	}
	
	public boolean isFulfilled(ButcheredRow r) {
		BigDecimal leftBC = r.compute(getLeft().toString(), null);
		BigDecimal rightBC = r.compute(getRight().toString(), null);
		if (getComparator() == TermComparator.L)
			return leftBC.compareTo(rightBC) < 0;
		else if (getComparator() == TermComparator.LE)
			return leftBC.compareTo(rightBC) <= 0;
		else if (getComparator() == TermComparator.E)
			return leftBC.compareTo(rightBC) == 0;
		else if (getComparator() == TermComparator.G)
			return leftBC.compareTo(rightBC) > 0;
		else if (getComparator() == TermComparator.GE)
			return leftBC.compareTo(rightBC) >= 0;
		else
			throw new NotImplementedException();
	}
}
