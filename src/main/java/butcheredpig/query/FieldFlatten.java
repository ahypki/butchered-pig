package butcheredpig.query;

import static net.hypki.libs5.utils.string.StringUtilities.substring;

import java.io.IOException;

import butcheredpig.chunks.Chunk;
import net.hypki.libs5.utils.string.StringUtilities;

public class FieldFlatten {

	private Chunk parentChunk = null;
	
	private Field field = null;
	
	public FieldFlatten() {
		
	}
	
	public FieldFlatten(Field field, Chunk parentChunk) {
		setField(field);
		setParentChunk(parentChunk);
	}
	
	public String getSourceName() {
		return substring(getField().getExpression().toString(), '(', ')');
	}
	
	public Chunk getSourceChunk() throws IOException {
		return getParentChunk().getButcheredPigScript().getChunkByAlias(getSourceName(), getParentChunk().getId());
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public Chunk getParentChunk() {
		return parentChunk;
	}

	public void setParentChunk(Chunk parentChunk) {
		this.parentChunk = parentChunk;
	}

//	public String getNewName() {
//		return getField().getColumnList().get
//		if (getField().getColumnList().size() > 0)
//		return null;
//	}
}
