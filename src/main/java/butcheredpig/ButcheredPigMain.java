package butcheredpig;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class ButcheredPigMain {
	public static void main(String[] args) throws ValidationException, IOException {
		LibsLogger.info(ButcheredPigMain.class, "Loading script adhoc.bpig...");
		String script = SystemUtils.readFileContent(ButcheredPigMain.class, "adhoc.bpig");
		
		assertTrue(notEmpty(script), "Read empty script");
		
		Init.init(args);
		
		// TODO DEBUG conf
//		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE_LOWERCASE, Job.COLUMN_FAMILY);
//		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE_LOWERCASE, Chunk.COLUMN_FAMILY);
		User user = UserFactory.getUser("arkadiusz@hypki.net"); // TODO conf
		
		ButcheredPigScript bp = new ButcheredPigScript()
				.setUserId(user.getUserId())
				.setNotebookId(UUID.random())
				.setToParse(script);
		
		bp
			.getContext()
			.setWorkingDir(new FileExt().getAbsolutePath());
		
		bp
				.save();
		
		while (!bp.isFinished()) {
			bp = ButcheredPigScriptFactory.getButcheredPigScript(bp.getId());
			
			bp.run();
			
			try {
				JobsManager.runAllJobs(Init.getAllChannelsNames());
				
				Thread.sleep(Settings.SCRIPT_BUTCHEREDPIG_SLEEP_MS);
			} catch (Exception e) {
				LibsLogger.error(ButcheredPigMain.class, "Cannot sleep", e);
				break;
			}
			
			LibsLogger.info(ButcheredPigMain.class, "ButcheredPigScript status " + bp.getProgress());
//			LibsLogger.info(ButcheredPigMain.class, "ButcheredPigScript status " + bp.toString(true));
		}
		
		Init.shutdown();
		
//		assertTrue(bp.getProgress().getStatus() == Status.DONE, "ButcheredPig status expected " + Status.DONE + 
//				" but is " + bp.getProgress());
		
		LibsLogger.info(ButcheredPigMain.class, "Butchered Pig finished!");
		System.exit(bp.getProgress().isDone() ? 0 : 1);
	}
}
