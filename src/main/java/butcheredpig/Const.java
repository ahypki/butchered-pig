package butcheredpig;

public class Const {
	public static final String REGEX_ALIAS = "\\w[\\w\\d\\_]*";
	public static final String REGEX_SPACE = "[\\s]*";
	
	public static final String DISTINCT = "DISTINCT";
	public static final String ASSERT = "ASSERT";
	public static final String DEFINE = "DEFINE";
	public static final String FOREACH = "FOREACH";
	public static final String ORDER = "ORDER";
	public static final String LOAD = "LOAD";
	public static final String STORE = "STORE";
	public static final String FILTER = "FILTER";
	public static final String GROUP = "GROUP";
	public static final String DUMP = "DUMP";
	public static final String UNION = "UNION";
	public static final String JOIN = "JOIN";
	public static final String SAMPLE = "SAMPLE";
	public static final String LIMIT = "LIMIT";
	public static final String CROSS = "CROSS";
	public static final String ILLUSTRATE = "ILLUSTRATE";
	
	public static final String INTO = "INTO";
	public static final String USING = "USING";
	public static final String BY = "BY";
	public static final String GENERATE = "GENERATE";
	public static final String AS = "AS";
	public static final String FLATTEN = "FLATTEN";
	public static final String OUTER = "OUTER";
	
	public static final String AVG = "AVG";
	public static final String COUNT = "COUNT";
	public static final String MAX = "MAX";
	public static final String MIN = "MIN";
	public static final String SUM = "SUM";
	
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";

	public static final int LOG_TRACE = 0;
	public static final int LOG_DEBUG = LOG_TRACE + 1;
	public static final int LOG_INFO = LOG_DEBUG + 1;
}
