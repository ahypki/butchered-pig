package butcheredpig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.hazelcast.core.Hazelcast;

import butcheredpig.commands.Foreach;
import butcheredpig.commands.ForeachDeser;
import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansConst;
import net.beanscode.model.connectors.CassandraConnector;
import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsConst;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class Init {

	private static boolean initiated = false;
	
	private static List<TableSchema> tableSchemas = null;
	
	private static List<String> channelNames = null;
	
	private static String[] args = null;
	
	public synchronized static void init(String [] args) throws IOException, ValidationException {
		Init.args = args;
		
		if (initiated == false) {
			initiated = true;
			
			// default logger
			if (args != null && ArgsUtils.exists(args, "log4j"))
				System.setProperty("log4j", ArgsUtils.getString(args, "log4j"));
			else if (args != null && ArgsUtils.exists(args, "verbose"))
				System.setProperty("log4j", "log4j-debug.properties");
			else
				System.setProperty("log4j", "log4j.properties");
			
//			System.setProperty("org.apache.commons.logging.Log","org.apache.commons.logging.impl.NoOpLog");
			
			final String settings = ArgsUtils.getString(args, "settings", "beans-dev.json");
			
			LibsLogger.debug(BeansConst.class, "Settings set to ", settings);
			LibsLogger.debug(BeansConst.class, "Initalizing Butchered Pig...");
				
			// locale
			Locale.setDefault(Locale.ENGLISH);
			
			LibsLogger.debug(BeansConst.class, "Libs4 code version " + LibsConst.VERSION);
			LibsLogger.debug(BeansConst.class, "BEANS code version " + BeansConst.VERSION);
			
			System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
			
			JsonUtils.registerTypeAdapter(Foreach.class, new ForeachDeser());
			
			initDb();
			
//			JobsManager.init(BeansConst.getAllChannelsNames());
			
			// INFO necessary because of problem with certificates at esa
			// https://dzone.com/articles/troubleshooting-javaxnetsslsslhandshakeexception-r
//			System.setProperty("https.protocols", "SSLv3,TLSv1,SSLv2Hello,TLSv1.2");
		}
	}
	
	public static void shutdown() throws IOException, ValidationException {
		Hazelcast.shutdownAll();
		DbObject.getDatabaseProvider().close();
		SearchManager.searchInstance().close();
		LibsLogger.info(Init.class, "Butchered Pig shutdowned");
	}
	
	public static void initDb() throws IOException, ValidationException {
		DbObject.init(new CassandraProvider());
		
		// create keyspace if not exist
		DbObject.getDatabaseProvider().createKeyspace(WeblibsConst.KEYSPACE);
							
		// creating table in DB
		for (TableSchema schema : getAllTableSchemas()) {
			DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE, schema);
			
		}
	}
	
	public static List<TableSchema> getAllTableSchemas() {
		if (tableSchemas == null) {
			tableSchemas = new ArrayList<TableSchema>();
			
			tableSchemas.add(new butcheredpig.chunks.ButcheredPigScript().getTableSchema());
			tableSchemas.add(new butcheredpig.commands.Distinct().getTableSchema());
			
			// columndefs table
			tableSchemas.add(new TableSchema(CassandraConnector.COLUMN_FAMILY_COLUMN_DEFS)
					.addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true)
					.addColumn(DbObject.COLUMN_DATA, ColumnType.STRING));
		}
		return tableSchemas;
	}

	public static List<String> getAllChannelsNames() {
		if (channelNames == null) {
			channelNames = new ArrayList<>();
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(WeblibsConst.CHANNEL_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_NORMAL);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_LONG_HIDDEN);
			channelNames.add(BeansChannels.CHANNEL_INDEX);
		}
		return channelNames;
	}
}
