package butcheredpig;

import java.io.File;

import net.hypki.libs5.utils.file.FileExt;

public class Settings {

	private static boolean tmpFolderCreated = false;
	
	public static final int 	LOG_LEVEL 						= Const.LOG_DEBUG;

	public static final int 	ALIAS_SPLIT_COUNT 				= 32;
	public static final long 	SORT_MAX_MEMORY_BYTES 			= 50_000_000; 	// 500 [MB]
	public static final int 	SCRIPT_BUTCHEREDPIG_SLEEP_MS 	= 5_000;		// 5 [s]
	public static final int 	SCRIPT_CHUNK_SLEEP_MS 			= 1_000;		// 1 [s]
	public static final long 	CHUNK_SPLIT_SIZE_BYTES 			= 100_000_000; 	// 100 [MB] by default for chunk size
	
	public static File getTempFolder() {
		if (!tmpFolderCreated)
			new FileExt("$HOME/.butcheredpig/tmp/").mkdirs();
		return new FileExt("$HOME/.butcheredpig/tmp/");
	}

	public static boolean isLogDebug() {
		return LOG_LEVEL >= Const.LOG_DEBUG;
	}
}
