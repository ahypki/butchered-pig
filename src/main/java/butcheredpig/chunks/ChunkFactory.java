package butcheredpig.chunks;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;

public class ChunkFactory {
	
	private static Map<UUID, Chunk> idToChunk = new HashMap<>();
	
	public static Iterable<Chunk> iterateChunks(List<UUID> chunks) {
		return new Iterable<Chunk>() {
			@Override
			public Iterator<Chunk> iterator() {
				return new Iterator<Chunk>() {
					private Iterator<UUID> chunkIdsIter = chunks.iterator();
					
					@Override
					public Chunk next() {
						return getChunk(chunkIdsIter.next());
					}
					
					@Override
					public boolean hasNext() {
						return chunkIdsIter.hasNext();
					}
				};
			}
		};
	}

	public static <T> T getChunk(UUID chunkId, Class<? extends Chunk> clazz) throws IOException {
		return (T) getChunk(chunkId);
//		Chunk t = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
//				Chunk.COLUMN_FAMILY, 
//				new C3Where()
//						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, chunkId.getId())), 
//				DbObject.COLUMN_DATA, 
//				clazz);
//		return t;
	}
	
	public static Chunk getChunk(UUID chunkId) {
		try {
			Chunk chTmp = idToChunk.get(chunkId);
			
			if (chTmp != null)
				return chTmp;
			
			String tObj = (String) DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
					Chunk.COLUMN_FAMILY, 
					new C3Where()
							.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, chunkId.getId())), 
					DbObject.COLUMN_DATA);
			
			if (tObj == null)
				return null;
			
			Class<? extends Chunk> clazz = (Class<? extends Chunk>) Class.forName(JsonUtils.parseJson(tObj).getAsJsonObject().get("type").getAsString());
			
			chTmp = JsonUtils.fromJson(tObj, clazz);
			
			idToChunk.put(chunkId, chTmp);
			
			return chTmp;
		} catch (Throwable t) {
			LibsLogger.error(ChunkFactory.class, "Cannot get chunk from DB", t);
			return null;
		}
	}

	public static void update(Chunk chunk) {
		idToChunk.put(chunk.getId(), chunk);
	}
	
	public static Progress getFinalProgress(List<UUID> chunkIDs) throws IOException {
		for (UUID uuid : chunkIDs) {
			Chunk ch = ChunkFactory.getChunk(uuid);
			
			if (ch.getProgress().isFailed())
				return ch.getProgress();
			else if (ch.getProgress().isRunning()
					|| ch.getProgress().isNew()) {
				// wait
				return ch.getProgress();
			} else if (ch.getProgress().isDone()) {
				// do nothing
			} else
				throw new IOException("Unimplemented case for chunk " + ch);
		}
		return new Progress(100, Status.DONE);
	}
}
