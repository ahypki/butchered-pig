package butcheredpig.chunks;

import java.io.IOException;
import java.util.Iterator;

import butcheredpig.commands.utils.GroupValue;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;

public class IterateAllGroups implements Iterable<Row> {
	private Chunk chunk = null;
	private Iterator<GroupValue> gvIter = null;
	private Iterator<Row> rowIter = null;
	private Row row;

	public IterateAllGroups() {
		
	}
	
	public IterateAllGroups(Chunk chunk) throws IOException, ValidationException {
		setChunk(chunk);
		setGvIter(chunk.iterateOutputGroupValue().iterator());
	}

	private Iterator<GroupValue> getGvIter() {
		return gvIter;
	}

	private void setGvIter(Iterator<GroupValue> gvIter) {
		this.gvIter = gvIter;
	}
	
	private Row next() {
		try {
			hasNext();
//			for (Object o : getChunk().iterateOutputGroupValue()) {
//				System.out.println(o);
//			}
			return row;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		} catch (ValidationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
		} finally {
			row = null;
		}
	}
	
	private boolean hasNext() {
		try {
			if (getGvIter() == null)
				return false;
			
			if (row != null)
				return true;
			
			while (true) {
				if (rowIter == null) {
					GroupValue gv = getGvIter().hasNext() ? getGvIter().next() : null;
					if (gv == null)
						// no more GroupValue
						return false;
					rowIter = getChunk().iterateOutputRows(gv, 0, 1).iterator();
				}
				
				if (rowIter.hasNext()) {
					row = rowIter.next();
					if (row == null)
						rowIter = null;
					else 
						return true;
				} else {
					// no more rows in this rows iterator, go to the next GroupValue
					rowIter = null;
				}
			}
		} catch (Throwable e) {
			LibsLogger.error(IterateAllGroups.class, "Iterator for group values failed", e);
			return false;
		}
	}

	@Override
	public Iterator<Row> iterator() {
		return new Iterator<Row>() {
			@Override
			public Row next() {
				return IterateAllGroups.this.next();
			}
			
			@Override
			public boolean hasNext() {
				return IterateAllGroups.this.hasNext();
			}
		};
	}

	private Chunk getChunk() {
		return chunk;
	}

	private void setChunk(Chunk chunk) {
		this.chunk = chunk;
	}
}
