package butcheredpig.chunks;

import static java.lang.String.format;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;

import butcheredpig.Context;
import butcheredpig.Settings;
import butcheredpig.commands.ForeachNested;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.SearchManager;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ButcheredPigScript extends ButcheredObject<ButcheredPigScript> implements Iterable<Chunk> {
	
	public static final String COLUMN_FAMILY = "butcheredpig";
		
	@Expose
	@NotNull
	@AssertValid
	private String butcheredPigScript = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private UUID notebookId = null;
	
	@Expose
	@NotNull
	@AssertValid
	private Progress progress = null;
	
	@Expose
	@NotNull
	@AssertValid
	private List<UUID> chunkIds = null;
	
	@Expose
	@NotNull
	@AssertValid
	private Context context = null;
	
	public ButcheredPigScript() {
		setId(UUID.random());
		getProgress().setStatus(Status.NEW);
	}
	
	public ButcheredPigScript(String toParse, UUID userId) throws IOException, ValidationException {
		setId(UUID.random());
		setUserId(userId);
		setToParse(toParse);
		getProgress().setStatus(Status.NEW);
	}
	
	@Override
	public String toString() {
		return format("ButcheredPigScript %s %s (%s, notebookId=%s)",
				getProgress(),
				getContext().getWorkingDir(),
				getId().getId(),
				getNotebookId().getId());
	}
		
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getId().getId();
	}
	
	@Override
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	@Override
	public void deindex() throws IOException {
		// removing table from search index
		SearchManager.removeObject(getCombinedKey().toLowerCase(), getColumnFamily());
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
//				.addInsertMutations(new ButcheredPigScriptJob(this))
				;
	}
	
	@Override
	public ButcheredPigScript save() throws ValidationException, IOException {
		super.save();
		
		ButcheredPigScriptFactory.updateCache(this);
		
		return this;
	}
	
	public boolean isFinished() {
		return getProgress().isDone() || getProgress().isFailed();
	}
	
	public boolean isRunning() {
		return getProgress().isRunning();
	}
	
	public void run() throws ValidationException, IOException {
		try {
			// check if the script is already done
			if (getProgress().getStatus() == Status.DONE
					|| getProgress().getStatus() == Status.FAILED) {
				LibsLogger.info(ButcheredPigScript.class, "Script " + this + " already " + getProgress().getStatus() + ", skipping...");
				return;
			}
			
			// check progress of all chunks
			int totalProgress = 0;
			Status totalStatus = Status.DONE;
			for (Chunk chunk : this) {
				
				if (chunk == null) {
					getProgress().fail("Cannot find chunk, failing the script");
					save();
					return;
				}
				
				LibsLogger.debug(ButcheredPigScript.class, "chunk ", chunk.getProgress());
				
				// determine final progress and status
				totalProgress += chunk.getProgress().getProgress();
				if (chunk.getProgress().getStatus() == Status.FAILED) {
					totalProgress = 0;
					totalStatus = Status.FAILED;
					break;
				} else if (chunk.getProgress().getStatus() == Status.NEW
						&& totalStatus != Status.FAILED) {
					totalStatus = Status.RUNNING;
//				break;
				} else if (chunk.getProgress().getStatus() == Status.RUNNING) {
					totalStatus = Status.RUNNING;
//				break;
				}
			}
				
			totalProgress = getChunkIds().size() > 0 ? 
					(int) (totalProgress / (float) (getChunkIds().size() * 100) * 100) : 100;
			LibsLogger.info(ButcheredPigScript.class, "Butchered Pig ", getId(), " total progress " + totalProgress 
					+ "%, final status " + totalStatus);
			
			if (totalStatus == Status.DONE) {
				// script is done
				getProgress().setProgress(100);
				getProgress().setStatus(Status.DONE);
				save();
				LibsLogger.info(ButcheredPigScript.class, "Butchered Pig script finished");
			} else if (totalStatus == Status.FAILED) {
				getProgress().fail("On of the Chunks in Butchered pig script failed");
				save();
				LibsLogger.error(ButcheredPigScript.class, "Butchered Pig script failed");
			} else {
				// save new progress and postpone
				getProgress().setProgress(totalProgress);
				getProgress().setStatus(totalStatus);
				save();
				
				if (totalStatus == Status.RUNNING) {
					try {
						LibsLogger.debug(ButcheredPigScript.class, "Butchered Pig script is not yet done, waiting " 
								+ (Settings.SCRIPT_BUTCHEREDPIG_SLEEP_MS/1000) + " [s]...");

						new ButcheredPigScriptJob(this)
							.setPostponeMs(Settings.SCRIPT_BUTCHEREDPIG_SLEEP_MS)
							.save();
					} catch (Throwable e) {
						LibsLogger.error(ButcheredPigScript.class, "Cannot put Butchered Pig script to sleep", e);
					}
				}
			}
		} catch (Throwable e) {
			LibsLogger.error(ButcheredPigScript.class, "Script failed", e);
			getProgress().fail(e.getMessage() != null ? e.getMessage() : "Butchered pig failed");
			save();
		}
	}

	public String getToParse() {
		return butcheredPigScript;
	}

	public ButcheredPigScript setToParse(String toParse) throws IOException, ValidationException {
		this.butcheredPigScript = toParse;
				
		ButcheredPigScriptFactory.parseToChunks(toParse, getUserId(), this);
		
		return this;
	}

	@Override
	public Iterator<Chunk> iterator() {
		//return getChunks().iterator();
		return new Iterator<Chunk>() {
			
			private Iterator<UUID> chunkIdIter = getChunkIds().iterator();
			
			@Override
			public Chunk next() {
				UUID nextChunkId = chunkIdIter.next();
				
				Chunk chunk = ChunkFactory.getChunk(nextChunkId);
				if (chunk != null)
					return chunk;
				
				try {
					chunk = ChunkFactory.getChunk(nextChunkId);
					
					if (chunk != null)
						return chunk;
				} catch (Exception e) {
					LibsLogger.error(ButcheredPigScript.class, "Cannot get chunk from DB", e);
				}
				
				return null;
			}
			
			@Override
			public boolean hasNext() {
				return chunkIdIter.hasNext();
			}
		};
	}
	

	public Progress getProgress() {
		if (progress == null)
			progress = new Progress();
		return progress;
	}

	public void setProgress(Progress progress) {
		this.progress = progress;
	}

	public Chunk getChunkByAlias(String alias, UUID chunkiIdStart) throws IOException {
		Chunk chunk = ChunkFactory
						.getChunk(chunkiIdStart);
		while (chunk != null) {
			// if the given chunk new alias is OK then return
			if (chunk.getAliasNew() != null && chunk.getAliasNew().equals(alias))
				return chunk;
			
			// the chunk can be FOREACH, and it can be grouped, if it is grouped one has to check the name of the group
//			if (chunk instanceof Foreach) {
//				Foreach foreach = (Foreach) chunk;
////				if (foreach.isSourceGrouped()
////						&& foreach.getSourceGroup().getAliasSource().equals(alias)
////						&& !foreach.getSourceGroup().getId().equals(chunkiIdStart))
//				if (foreach instanceof Foreach 
//						&& ((Foreach) foreach).getForeach() instanceof ForeachNotNestedGrouped) {
//					ForeachNotNestedGrouped f = ((Foreach) foreach).getForeachNotNestedGrouped();
//					if (f.getSouSourceGroup().getAliasSource().equals(alias)
//						&& !f.getId().equals(chunkiIdStart))
//						return f;
//				}
//			}
			
			// go to previous chunk
			chunk = chunk.getPrevChunk();
		}
		
		LibsLogger.error(ButcheredPigScript.class, "Cannot find chunk by alias " + alias + " starting from chunk " + chunkiIdStart);// TODO throw?
		return chunk;
		
//		if (alias.equals("sys2"))
//			System.out.println("brr");
//		boolean skip = true;
//		
//		if (chunkiIdStart == null)
//			skip = false;
//		
//		for (int i = getChunkIds().size() - 1; i >= 0; i--) {
//			UUID chunkId = getChunkIds().get(i);
//			Chunk chunk = ChunkFactory.getChunk(chunkId);
//	
//			if (skip) {
//				if (chunkId.equals(chunkiIdStart)) {
//					skip = false;
//				}
//				
//				// check if it is FOREACH with nested chunks
//				if (chunk instanceof Foreach) {
//					Foreach foreach = (Foreach) chunk;
//					
//					if (foreach.isSourceGrouped() 
//							&& foreach.getGroupSourceId().equals(chunkiIdStart))
//						skip = false;
//					
//					try {
//						if (foreach.isSourceGrouped() 
//								&& foreach.getSourceGroup().getAliasSource().equals(alias)
//								&& foreach.getSourceGroup().getId().equals(chunkiIdStart) == false)
//							return foreach.getSourceGroup();
//					} catch (IOException e) {
//						LibsLogger.error(ButcheredPigScript.class, "Cannot create group chunk from DB", e);
//					}
//					
//					if (foreach.isNestedForeach()) {
//						Chunk chunkForeach = foreach.getChunkByAlias(alias, chunkiIdStart);
//						if (chunkForeach != null)
//							return chunkForeach;
//					}
//					
//					if (foreach.getNestedChunkIds().contains(chunkiIdStart)) {
//						skip = false;
//					}
//				}
//				
//				
//				continue;
//			}
//		
//			if (chunk.getAliasNew() != null && chunk.getAliasNew().equals(alias))
//				return chunk;
//		}
//		return null;
	}

	public List<UUID> getChunkIds() {
		if (chunkIds == null)
			chunkIds = new ArrayList<>();
		return chunkIds;
	}

	public void setChunkIds(List<UUID> chunkIds) {
		this.chunkIds = chunkIds;
	}
	
	public Chunk getChunk(UUID chunkId) throws IOException {
		return ChunkFactory.getChunk(chunkId);
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public ButcheredPigScript setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
		return this;
	}
	
	public UUID getLastChunkId() {
		return getChunkIds().size() > 0 ? getChunkIds().get(getChunkIds().size() - 1) : null;
	}
	
	public int getChunkIndex(UUID chunkId) {
		for (int i = 0; i < getChunkIds().size(); i++) {
			if (getChunkIds().get(i).equals(chunkId))
				return i;
		}
		return -1;
	}

	public Chunk getChunkPrev(UUID startChunkId) throws IOException {
		int chIdx = getChunkIndex(startChunkId);
		if (chIdx > 0)
			return getChunk(getChunkIds().get(chIdx - 1));
		
//		// searching for Chunk among nested instructions
//		throw new NotImplementedError("Chunk " + chunkId + "was not found in BP, it has to be in nested instruction");
		
		UUID chunkId = null;
		UUID prevChunkId = null;
		for (int i = getChunkIds().size() - 1; i >= 0; i--) {
			prevChunkId = chunkId;
			
			chunkId = getChunkIds().get(i);
			Chunk chunk = ChunkFactory.getChunk(chunkId);
			
			if (chunkId.equals(startChunkId)) {
				return ChunkFactory.getChunk(prevChunkId);
			}
				
			// check if it is FOREACH with nested chunks
			if (chunk instanceof ForeachNested) {
				ForeachNested foreach = (ForeachNested) chunk;
				
				if (foreach.getGroupSourceId().equals(startChunkId))
					return ChunkFactory.getChunk(prevChunkId);
				
//				if (foreach.isNestedForeach()) {
					chIdx = foreach.getNestedChunkIds().indexOf(startChunkId);
					if (chIdx > 0)
						return ChunkFactory.getChunk(foreach.getNestedChunkIds().get(chIdx - 1));
//				}
			}
		}
		return null;
	}

	public Context getContext() {
		if (context == null)
			context = new Context();
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}
