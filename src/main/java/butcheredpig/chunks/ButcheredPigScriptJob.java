package butcheredpig.chunks;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ButcheredPigScriptJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID butcheredPigId = null;
	
	private ButcheredPigScript butcheredPigScript = null;

	public ButcheredPigScriptJob() {
		
	}
	
	public ButcheredPigScriptJob(ButcheredPigScript s) {
		setButcheredPigScript(s);
		setButcheredPigId(s.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		// check if the script was not removed in the mean time
		if (getButcheredPigScript() == null)
			return;
		
		// run script, do not wait for the results
		getButcheredPigScript().run();
		
//		LibsLogger.info(JoinLargeUnitTest.class, "ButcheredPigScript status " + getButcheredPigScript().toString(true));
	}

	public UUID getButcheredPigId() {
		return butcheredPigId;
	}

	public void setButcheredPigId(UUID butcheredPigId) {
		this.butcheredPigId = butcheredPigId;
	}

	private ButcheredPigScript getButcheredPigScript() throws IOException {
		if (butcheredPigScript == null) {
			butcheredPigScript = ButcheredPigScriptFactory.getButcheredPigScript(getButcheredPigId());
		}
		return butcheredPigScript;
	}

	private void setButcheredPigScript(ButcheredPigScript butcheredPigScript) {
		this.butcheredPigScript = butcheredPigScript;
	}
}
