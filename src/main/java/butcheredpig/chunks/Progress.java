package butcheredpig.chunks;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Progress {
	
	@Expose
	@NotNull
	@AssertValid
	private int progress = 0;

	@Expose
	@NotNull
	@AssertValid
	private Status status = Status.NEW;
	
	@Expose
	@AssertValid
	private String message = null;
	
	public Progress() {
		
	}
	
	public Progress(int progress, Status status) {
		setProgress(progress);
		setStatus(status);
	}
	
	@Override
	public String toString() {
		return String.format("%s %2d%%%s", getStatus(), getProgress(), isMessage() ? " [" + getMessage() + "]" : "");
	}
	
	public boolean isDone() {
		return getStatus() == Status.DONE;
	}
	
	public boolean isRunning() {
		return getStatus() == Status.RUNNING;
	}
	
	public boolean isNew() {
		return getStatus() == Status.NEW;
	}
	
	public boolean isFailed() {
		return getStatus() == Status.FAILED;
	}

	public int getProgress() {
		return progress;
	}
	
	public boolean isMessage() {
		return getMessage() != null && getMessage().trim().length() > 0;
	}

	public void setProgress(int progress) {
		this.progress = progress;
		this.progress = Math.max(this.progress, 0);
		this.progress = Math.min(this.progress, 100);
		
		if (this.progress == 100)
			setStatus(Status.DONE);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
		if (status == Status.DONE)
			this.progress = 100;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void fail(String message) {
		setStatus(Status.FAILED);
		setMessage(message);
	}
	
	public void done(String message) {
		setStatus(Status.DONE);
		setMessage(message);
	}
}
