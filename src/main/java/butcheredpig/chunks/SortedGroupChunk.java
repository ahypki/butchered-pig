package butcheredpig.chunks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import butcheredpig.Settings;
import butcheredpig.commands.SortJob;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.ConnectorUtils;
import net.beanscode.model.plugins.MemoryConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.file.FileExt;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class SortedGroupChunk extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID sourceChunkId = null;
		
	@Expose
//	@NotEmpty
//	@NotNull
	private GroupValue groupValue = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private List<String> sortColumns = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	private ConnectorList result = null;
	
	private Chunk sourceChunk = null;

	public SortedGroupChunk() {
		
	}
	
	public SortedGroupChunk(Chunk chunk, GroupValue groupValue, List<String> sortColumns) {
		setButcheredPigScriptId(chunk.getButcheredPigScriptId());
		setUserId(chunk.getUserId());
		setSourceChunkId(chunk.getId());
		setGroupValue(groupValue);
		setSortColumns(sortColumns);
	}
	
	@Override
	public String toStringScript() {
		return String.format("SORTED GROUP CHUNK %s", 
				getGroupValue() != null ? getGroupValue().toString() : "");
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		return this;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		return false;
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.add(new SortJob(getId()).getSaveMutations());
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getSourceChunk().getSourceApproxBytesSize();
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getSourceChunk().iterateSourceRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new H5Connector(UUID.random(), new FileExt("sorted-" + getId() + ".h5")).iterateRows(null, splitNr, splitCount);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (isFinished())
			return;

		if (getButcheredPigScript().isFinished())
			return;
		
		if (!getSourceChunk().isDone())
			return;
		
		// read source and sort up to some MAX memory size, then open new result connector and do the same. 
		// At the end read sorted values from connectors
		MemoryConnector memConn = new MemoryConnector(getSourceChunk().getOutputColumnDefList());
		for (Row r : getSourceChunk().iterateOutputRows(getGroupValue(), 0, 1)) {
			memConn.write(r);
			
			if (memConn.getSize() % 1000 == 0) {
				if (memConn.getAproxBytesSize() > Settings.SORT_MAX_MEMORY_BYTES) {
					sortAndAdd(memConn);
				}
			}
		}

		if (memConn.getSize() > 0)
			sortAndAdd(memConn);
		
		H5Connector dest = new H5Connector(UUID.random(), new FileExt("sorted-" + getId() + ".h5"));
		dest.setColumnDefs(memConn.getColumnDefs());
		
		ConnectorUtils.combineSortedConnectors(getResult(), dest, getSortColumns());
		
		getProgress().done("Done sorting for GroupValue " + getGroupValue());
		save();
	}
	
	private void sortAndAdd(MemoryConnector memConn) throws IOException {
		// sort in memory
		memConn.sort(getSortColumns());
		
		// save to disk
		H5Connector dest = new H5Connector(UUID.random(), new FileExt("sorted-" + UUID.random() + ".h5"));
		dest.setColumnDefs(memConn.getColumnDefs());
		ConnectorUtils.pipe(memConn, dest);
		dest.close();
		
		// add to the results
		getResult().addConnector(dest);
		
		memConn.clear();
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	public Chunk getSourceChunk() throws IOException {
		if (sourceChunk == null) {
			sourceChunk = ChunkFactory.getChunk(getSourceChunkId());
		}
		return sourceChunk;
	}

	private ConnectorList getResult() {
		if (result == null)
			result = new ConnectorList();
		return result;
	}

	private void setResult(ConnectorList result) {
		this.result = result;
	}

	public List<String> getSortColumns() {
		if (sortColumns == null)
			sortColumns = new ArrayList<String>();
		return sortColumns;
	}

	public void setSortColumns(List<String> sortColumns) {
		this.sortColumns = sortColumns;
	}

	public GroupValue getGroupValue() {
		return groupValue;
	}

	public void setGroupValue(GroupValue groupValue) {
		this.groupValue = groupValue;
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		// it is already working on groupev value
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		List<GroupValue> gr = new ArrayList<>();
		gr.add(getGroupValue());
		return gr;
	}

	public UUID getSourceChunkId() {
		return sourceChunkId;
	}

	public void setSourceChunkId(UUID sourceChunkId) {
		this.sourceChunkId = sourceChunkId;
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		new FileExt("sorted-" + getId() + ".h5").deleteIfExists();
	}
}
