package butcheredpig.chunks;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.buildStringFromChar;
import static net.hypki.libs5.utils.string.StringUtilities.padLeft;
import static net.hypki.libs5.utils.string.StringUtilities.padRight;

import java.io.IOException;
import java.util.Map;

import net.beanscode.model.BeansObject;
import net.beanscode.model.connectors.ConnectorLink;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

import butcheredpig.Const;
import butcheredpig.commands.Dump;
import butcheredpig.commands.utils.GroupValue;

public abstract class Chunk extends ButcheredObject<Chunk> {

	public static final String COLUMN_FAMILY = "bp";
	
	@Expose
	@NotNull
	@AssertValid
	private String type = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private UUID prevChunkId = null;
		
	@Expose
	@NotNull
	@AssertValid
	private Progress progress = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID butcheredPigScriptId = null;

	@Expose
//	@NotNull
	@AssertValid
	private String aliasNew = null;

	public abstract boolean 		whoWantsToParse(String toParse) throws IOException;
	public abstract Chunk 			parse(ButcheredPigScript butcheredPigScript, String toParse) 			throws IOException, ValidationException;
	
	public abstract void 			run() 							throws IOException, ValidationException;
	
	/**
	 * 
	 * @return Returns bytes size, 0 if there is no data, -1 if the Chunk is not ready yet
	 * @throws IOException
	 * @throws ValidationException
	 */
	public abstract long 			getSourceApproxBytesSize() 		throws IOException, ValidationException;
	
	public abstract ColumnDefList 	getSourceColumnDefList() 		throws IOException, ValidationException;
	
	public abstract Iterable<Row> 	iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException;
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 * @throws ValidationException
	 */
	public abstract long 			getOutputApproxBytesSize() 		throws IOException, ValidationException;
	
	public abstract ColumnDefList 	getOutputColumnDefList() 		throws IOException, ValidationException;
	
	public abstract Iterable<Row> 	iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException;
	
	public abstract boolean 		isOutputGrouped() 				throws IOException, ValidationException;
	public abstract Iterable<GroupValue> 	iterateOutputGroupValue() 		throws IOException, ValidationException;
	
	public abstract String 			toStringScript();
	
	public abstract void 			clear() 						throws IOException, ValidationException;
	
	// cache
	private ButcheredPigScript butcheredPigScriptCache = null;
	
	public Chunk() {
		setType(getClass().getName());
		setId(UUID.random());
		getProgress().setStatus(Status.NEW);
	}
	
	public Chunk(ButcheredPigScript butcheredPigScript, Chunk previousChunk) {
		setButcheredPigScriptId(butcheredPigScript.getId());
		setButcheredPigScript(butcheredPigScript);
		setType(getClass().getName());
		setId(UUID.random());
		getProgress().setStatus(Status.NEW);
		setPrevChunkId(previousChunk.getId());
	}
	
	@Override
	public String toString() {
		return format("%s [%s %s]",
				toStringScript(),
				getProgress(),
				getClass().getSimpleName().toUpperCase());
	}
	
	public void illustrate(int tabs) {
		try {
			int w1 = 4;
			int w2 = w1 * 2;
			tabs = tabs * w1;
			LibsLogger.info(getClass(), buildStringFromChar(' ', tabs), toString());
			LibsLogger.info(getClass(), buildStringFromChar(' ', tabs + w1), "Progress: ", getProgress().toString());
			LibsLogger.info(getClass(), buildStringFromChar(' ', tabs + w1), String.format("%s %.12e %s", "Source, size: ", (getSourceApproxBytesSize() / 1_000_000.0), " [MB]"));
			LibsLogger.info(getClass(), buildStringFromChar(' ', tabs + w1), "Source, first <= 10 rows: ");
			int c = 0;
			for (Row r : iterateSourceRows(null, 0, 1)) {
				LibsLogger.info(getClass(), buildStringFromChar(' ', tabs + w2), r);
				if (c++ == 10)
					break;
			}
			LibsLogger.info(getClass(), buildStringFromChar(' ', tabs + w1), "Output, size: ", (getOutputApproxBytesSize() / 1_000_000.0), " [MB]");
			c = 0;
			for (Row r : iterateOutputRows(null, 0, 1)) {
				LibsLogger.info(getClass(), buildStringFromChar(' ', tabs + w2), r);
				if (c++ == 10)
					break;
			}
		} catch (Exception e) {
			LibsLogger.error(Chunk.class, "Cannot illustrate " + toString(), e);
		}
	}
	
	public Chunk cloneThis() {
		return (Chunk) JsonUtils.fromJson(getData(), getClass());
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getId().getId();
	}
	
	@Override
	public void index() throws IOException {
		// do nothing
//		SearchManager.index(this);
	}
	
	@Override
	public void deindex() throws IOException {
		// do nothing
//		SearchManager.removeObject(getCombinedKey(), COLUMN_FAMILY);
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addInsertMutations(!isFinished(), new ChunkJob(this));
	}
	
	@Override
	public Chunk save() throws ValidationException, IOException {
		super.save();
		
		ChunkFactory.update(this);
		
		return this;
	}
	
	protected void fail(String message) {
		getProgress().fail(message);
	}

	protected void done(String message) {
		getProgress().done(message);
	}
	
	public boolean isDone() {
		return getProgress().isDone();
	}
	
	public boolean isFinished() {
		return getProgress().isDone() || getProgress().isFailed();
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getAliasNew() {
		return aliasNew;
	}

	public void setAliasNew(String aliasNew) {
		this.aliasNew = aliasNew;
	}

	public Progress getProgress() {
		if (progress == null)
			progress = new Progress();
		return progress;
	}
	
	public void setProgress(Progress progress) {
		this.progress = progress;
	}
	
	public UUID getButcheredPigScriptId() {
		return butcheredPigScriptId;
	}
	
	public void setButcheredPigScriptId(UUID butcheredPigScriptId) {
		this.butcheredPigScriptId = butcheredPigScriptId;
	}
	
	public ButcheredPigScript getButcheredPigScript() throws IOException {
		if (butcheredPigScriptCache == null)
			butcheredPigScriptCache = ButcheredPigScriptFactory.getButcheredPigScript(getButcheredPigScriptId());
		return butcheredPigScriptCache;
	}
	
	public void setButcheredPigScript(ButcheredPigScript butcheredPigScript) {
		this.butcheredPigScriptId = butcheredPigScript.getId();
		this.butcheredPigScriptCache = butcheredPigScript;
	}
	
	public UUID getPrevChunkId() {
		return prevChunkId;
	}
	
	public void setPrevChunkId(UUID prevChunId) {
		this.prevChunkId = prevChunId;
	}
	
	public Chunk getPrevChunk() {
		return getPrevChunkId() != null && !getPrevChunkId().equals(UUID.ZERO) ? ChunkFactory.getChunk(getPrevChunkId()) : null; 
	}
}
