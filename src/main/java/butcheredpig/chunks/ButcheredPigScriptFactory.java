package butcheredpig.chunks;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butcheredpig.commands.Assert;
import butcheredpig.commands.Comment;
import butcheredpig.commands.Cross;
import butcheredpig.commands.Define;
import butcheredpig.commands.Distinct;
import butcheredpig.commands.Dump;
import butcheredpig.commands.Filter;
import butcheredpig.commands.Foreach;
import butcheredpig.commands.Generate;
import butcheredpig.commands.Group;
import butcheredpig.commands.Illustrate;
import butcheredpig.commands.Join;
import butcheredpig.commands.Limit;
import butcheredpig.commands.Load;
import butcheredpig.commands.Order;
import butcheredpig.commands.Sample;
import butcheredpig.commands.Store;
import butcheredpig.commands.Union;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;

public class ButcheredPigScriptFactory {

	private static List<Chunk> allPossibleChunks = null;
	private static Map<UUID, ButcheredPigScript> idToButcheredPigScript = new HashMap<>();

	public static List<Chunk> getAllPossibleChunksInstances() {
		if (allPossibleChunks == null) {
			allPossibleChunks = new ArrayList<>();
			allPossibleChunks.add(new Distinct());
			allPossibleChunks.add(new Assert());
			allPossibleChunks.add(new Comment());
			allPossibleChunks.add(new Load());
			allPossibleChunks.add(new Dump());
			allPossibleChunks.add(new Foreach());
			allPossibleChunks.add(new Store());
			allPossibleChunks.add(new Group());
			allPossibleChunks.add(new Filter());
			allPossibleChunks.add(new Union());
			allPossibleChunks.add(new Join());
			allPossibleChunks.add(new Order());
			allPossibleChunks.add(new Generate());
			allPossibleChunks.add(new Sample());
			allPossibleChunks.add(new Limit());
			allPossibleChunks.add(new Define());
			allPossibleChunks.add(new Cross());
			allPossibleChunks.add(new Illustrate());
		}
		return allPossibleChunks;
	}
	
	private static Chunk findChunkToParse(String lineToParse) throws IOException {
		Chunk meatSelected = null;
		
		for (Chunk meat : ButcheredPigScriptFactory.getAllPossibleChunksInstances()) {
			if (meat.whoWantsToParse(lineToParse)) {
				if (meatSelected != null)
					throw new IOException("There is more chunks of meat ready to parse: " + lineToParse 
							+ " (" + meatSelected.getClass().getSimpleName() + " and " + meat.getClass().getSimpleName());
				
				meatSelected = meat;
			}
		} 
		
		return meatSelected;
	}

	public static List<Chunk> parseToChunks(String script, UUID userId, ButcheredPigScript butcheredPig) throws IOException {
		if (nullOrEmpty(script))
			return new ArrayList<>();
		
		List<Chunk> chunks = new ArrayList<>();
		EscapeString es = new EscapeString(script);
		es.escapeBetween('{', '}');
		
		UUID prevChunk = UUID.ZERO;
		for (String lineSemi : StringUtilities.split(es.getEscapedText(), ';')) {
			lineSemi = es.deescapeString(lineSemi);
			lineSemi = lineSemi.trim();
			
			if (nullOrEmpty(lineSemi))
				continue;
			
			try {
				// parsing comments (they can not be split by semicolons)
				while (new Comment().whoWantsToParse(lineSemi)) {
					int newLineIdx = lineSemi.indexOf('\n');
					String cmt = newLineIdx >= 0 ? lineSemi.substring(0, newLineIdx) : lineSemi;
					
					Comment comment = new Comment(cmt);
					comment.setPrevChunkId(prevChunk);
					comment.setUserId(userId);
					comment.setButcheredPigScript(butcheredPig);
					comment.save();
					
					butcheredPig.getChunkIds().add(comment.getId());
					chunks.add(comment);
					
					prevChunk = comment.getId();
					if (newLineIdx >= 0)
						lineSemi = lineSemi.substring(newLineIdx + 1).trim();
					else 
						break;
				}
				
				// removing new lines
				lineSemi = lineSemi.replace('\n', ' ').trim();
				
				if (nullOrEmpty(lineSemi))
					continue;
				
				// find a proper chunk
				Chunk chunkToRun = findChunkToParse(lineSemi);
							
				if (chunkToRun == null)
					throw new IOException("Don't know how to parse: " + lineSemi);
				
				// parse the line into Chunk class
				Chunk toAdd = (Chunk) chunkToRun.copy();
				toAdd.setPrevChunkId(prevChunk);
				toAdd.setId(UUID.random());
				toAdd.setUserId(userId);
				toAdd.setButcheredPigScript(butcheredPig);
				toAdd.parse(butcheredPig, lineSemi);
				toAdd.save();
				
				butcheredPig.getChunkIds().add(toAdd.getId());
				chunks.add(toAdd);
				
				prevChunk = toAdd.getId();
			} catch (Exception e) {
				throw new IOException("Cannot parse line: " + lineSemi + " with error: " + e.getMessage(), e);
			}
		}
		
		LibsLogger.debug(ButcheredPigScript.class, "Script parse complete");
		return chunks;
	}
	
	public static ButcheredPigScript getButcheredPigScript(UUID butcheredPigScriptId) throws IOException {
		ButcheredPigScript chTmp = idToButcheredPigScript.get(butcheredPigScriptId);
		
		if (chTmp != null)
			return chTmp;
		
		ButcheredPigScript bps = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				ButcheredPigScript.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, butcheredPigScriptId.getId())), 
				DbObject.COLUMN_DATA, 
				ButcheredPigScript.class);
		
		idToButcheredPigScript.put(butcheredPigScriptId, bps);
		return bps;
	}

	public static void updateCache(ButcheredPigScript butcheredPigScript) {
		idToButcheredPigScript.put(butcheredPigScript.getId(), butcheredPigScript);
	}
}
