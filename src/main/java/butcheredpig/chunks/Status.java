package butcheredpig.chunks;

public enum Status {
	NEW,
	RUNNING,
	FAILED,
	DONE
}
