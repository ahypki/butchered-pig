package butcheredpig.chunks;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.Settings;
import net.beanscode.model.BeansSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ChunkJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID chunkId = null;
	
	private Chunk chunk = null;
	
	public ChunkJob() {
		
	}
	
	public ChunkJob(Chunk chunk) {
		setChunkId(chunk.getId());
	}

	// TODO move to ButcheredPigScriptJob
//	@Override
//	protected String getLogSeparately() {
//		try {
//			if (getChunk() == null || getChunk().getButcheredPigScript() == null)
//				return null;
//			
//			SettingValue pigPath = SettingFactory.getSettingValue(BeansSettings.SETTING_PIG_FOLDER, "path");
//			FileExt f = new FileExt(pigPath.getValueAsString() + "/beans-" + getChunk().getButcheredPigScript().getNotebookId() + ".log");
//			return f.getAbsolutePath();
//		} catch (Throwable e) {
//			LibsLogger.error(ChunkJob.class, "Cannot log separatelly Job", e);
//			return super.getLogSeparately();
//		}
//	}
	
	@Override
	public void run() throws IOException, ValidationException {
		Chunk chunk = getChunk();
		
		if (chunk == null) {
			// do nothing, remove this job
			LibsLogger.info(ChunkJob.class, "Chunk " + getCombinedKey() + " does not exist anymore, consuming chunk job");
			return;
		}
		
		if (chunk.isFinished()) {
			// chunk succeeded or failed, remove this job
			LibsLogger.info(ChunkJob.class, "Chunk " + chunkId + " already finished, consuming chunk job");
			return;
		}
		
		if (chunk.getButcheredPigScript() == null) {
			LibsLogger.info(ChunkJob.class, "ButcheredPigScript for Chunk " + chunkId + " does not exist, consuming chunk job");
			return;
		}
		
		if (chunk.getButcheredPigScript().isFinished()) {
			// ButcheredPig script succeeded or failed, remove this job
			LibsLogger.info(ChunkJob.class, "ButcheredPigScript for Chunk " + chunkId + " already finished, consuming chunk job");
			return;
		}
		
		if (!chunk.getButcheredPigScript().isRunning()) {
			// ButcheredPig script did not start yet, postponing
			LibsLogger.debug(ChunkJob.class, "ButcheredPigScript not started yet, postponing chunk " + chunk.toString());
			setPostponeMs(Settings.SCRIPT_CHUNK_SLEEP_MS);
			return;
		}
		
		try {
			chunk.run();
		} catch (Throwable e) {
			LibsLogger.error(ChunkJob.class, "Chunk failed", e);
			chunk.getProgress().fail(e.getMessage());
			chunk.save();
		}
		
		if (chunk.getProgress().getStatus() == Status.DONE) {
			// do nothing, remove this job
		} else if (chunk.getProgress().getStatus() == Status.FAILED) {
			// do nothing, remove this job
		} else if (chunk.getProgress().getStatus() == Status.NEW
				|| chunk.getProgress().getStatus() == Status.RUNNING) {
			setPostponeMs(Settings.SCRIPT_CHUNK_SLEEP_MS);
		} else {
			LibsLogger.error(ChunkJob.class, "Unimplemented case for Chunkjob " + chunk.getProgress() + 
					", setting chunk to " + Status.FAILED +", and consuming job");
			chunk.getProgress().setStatus(Status.FAILED);
			chunk.save();
		}
	}
	
	private Chunk getChunk() throws IOException {
		if (chunk == null)
			chunk = ChunkFactory.getChunk(getChunkId());
		return chunk;
	}

	public UUID getChunkId() {
		return chunkId;
	}

	public void setChunkId(UUID chunkId) {
		this.chunkId = chunkId;
	}

}
