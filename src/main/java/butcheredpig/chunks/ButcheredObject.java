package butcheredpig.chunks;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public abstract class ButcheredObject<T> extends WeblibsObject<T> implements Indexable {

	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID userId = null;

	
	public UUID getId() {
		return id;
	}

	public T setId(UUID id) {
		this.id = id;
		return (T) this;
	}
	
	public UUID getUserId() {
		return userId;
	}
	
	public User getUser() throws IOException {
		return UserFactory.getUser(getUserId());
	}

	public T setUserId(UUID userId) {
		this.userId = userId;
		return (T) this;
	}

}
