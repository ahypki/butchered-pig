package butcheredpig.chunks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import net.beanscode.model.plugins.ColumnDef;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;

public class IterateCross implements Iterable<Row>, Iterator<Row> {
	
	private List<Chunk> chunks = null;
	
	private int splitNr = 0;
	
	private int splitCount = 1;
	
	private List<Iterator<Row>> chunksIter = null;
	
	private List<Row> chunkRows = null;
	
	public IterateCross() {
		
	}
	
	public IterateCross(int splitNr, int splitCount, List<Chunk> chunks) {
		this.chunks = chunks;
		setSplitNr(splitNr);
		setSplitCount(splitCount);
	}

	@Override
	public Iterator<Row> iterator() {
		return this;
	}

	public List<Chunk> getChunks() {
		if (chunks == null)
			chunks = new ArrayList<>();
		return chunks;
	}

	public void setChunks(List<Chunk> chunks) {
		this.chunks = chunks;
	}

	public int getSplitNr() {
		return splitNr;
	}

	public void setSplitNr(int splitNr) {
		this.splitNr = splitNr;
	}

	public int getSplitCount() {
		return splitCount;
	}

	public void setSplitCount(int splitCount) {
		this.splitCount = splitCount;
	}
	
	private boolean isRowReady() {
		boolean hasNext = getChunksIter().size() > 0;
		for (int i = 1; i < chunkRows.size(); i++) {
			if (chunkRows.get(i) == null) {
				hasNext = false;
				break;
			}
		}
		return hasNext;
	}

	@Override
	public boolean hasNext() {
		try {
			if (chunkRows == null)
				getChunksIter();
			
			while (true) {
				// check if all rows are ready
				if (isRowReady())
					// new row is already waiting
					return true;
				
				if (getChunksIter().get(0) == null
						|| getChunksIter().get(0).hasNext() == false)
					// no more rows from the first iterator
					return false;
				
				// reopen (if needed)
				for (int i = 1; i < getChunksIter().size(); i++)
					reopen(i);
				
				// advancing
				for (int i = 1; i < chunkRows.size(); i++)
					if (chunkRows.get(i) == null)
						if (getChunksIter().get(i).hasNext())
							advance(i);
			}
		} catch (IOException | ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	private void reopen(int i) throws IOException, ValidationException {
		if (i == 0) {
			chunkRows.set(0, null);
			return; // do not reopen the first iterator, it cannot be reopened/reverted
		}
		
		if (getChunksIter().get(i) != null
				&& getChunksIter().get(i).hasNext())
			return; // do nothing, the iterator is already opened and still has elements
		
		if (getChunksIter().get(i) == null
				|| getChunksIter().get(i).hasNext() == false) {
			getChunksIter().set(i, 
					getChunks().get(i).iterateOutputRows(null, getSplitNr(), getSplitCount()).iterator());
			advance(i);
			advance(i - 1);
//			chunkRows.set(i, null);
//			chunkRows.set(i - 1, null);
		}
	}
	
	private void advance(int i) throws IOException, ValidationException {
//		if (chunkRows.get(i) != null)
//			return; // do nothing
		
		if (i < 0)
			return;
		
		if (getChunksIter().get(i) != null
				&& getChunksIter().get(i).hasNext()) {
			chunkRows.set(i, getChunksIter().get(i).next());
			return;
		}
		
		if (getChunksIter().get(i) != null
				&& getChunksIter().get(i).hasNext() == false) {
			reopen(i);
//			advance(i - 1);
//			chunkRows.set(i, null);
//			if (i == 0) {
//				// do nothing for the first iterator
//			} else {
////				chunkRows.set(i, null);
////				advance(i - 1);
//			}
		}
	}
	

	@Override
	public Row next() {
		try {
			if (chunkRows == null)
				hasNext();
			
			// building row
			Row row = new Row();
			for (int i = 0; i < getChunks().size(); i++) {
				Chunk ch = getChunks().get(i);
				Row r = chunkRows.get(i);
				
				if (r == null)
					return null;
				
				for (ColumnDef col : ch.getOutputColumnDefList()) {
					row.addColumn(ch.getAliasNew() + "::" + col.getName(), r.get(col.getName()));
				}
			}
			
			// clearing the row
			advance(chunkRows.size() - 1);
//			chunkRows.set(chunkRows.size() - 1, null);
			
			return row;
		} catch (IOException | ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Returns true if there are some elements left, false otherwise. 
	 * 
	 * @return
	 */
	private boolean reopenChunks() {
		try {
			while (true) {
				if (getChunksIter().get(0) == null
						|| getChunksIter().get(0).hasNext() == false) {
					return false;
				}
				
				boolean reopened = false;
				for (int i = 0; i < getChunks().size(); i++) {
					if (getChunksIter().get(i) == null
							|| getChunksIter().get(i).hasNext() == false) {
						Iterator<Row> it = getChunks().get(i).iterateOutputRows(null, getSplitNr(), getSplitCount()).iterator();
						getChunksIter().set(i, it);
						chunkRows.set(i, it.hasNext() ? it.next() : null);
						reopened = true;
					}
				}
				
				if (!reopened)
					// nothing was reopened
					return true;
			}
		} catch (IOException | ValidationException e) {
			LibsLogger.error(IterateCross.class, "Cannot reopen an iterator for a chunk", e);
			return false;
		}
	}

	private List<Iterator<Row>> getChunksIter() {
		if (chunksIter == null) {
			chunksIter = new ArrayList<>();
			chunkRows = new ArrayList<>();
			try {
				chunksIter.add(getChunks().get(0).iterateOutputRows(null, splitNr, splitCount).iterator());
				chunkRows.add(chunksIter.get(0).hasNext() ? chunksIter.get(0).next() : null);
				for (Chunk chunk : getChunks().stream().skip(1).collect(Collectors.toList())) {
					Iterator<Row> it = chunk.iterateOutputRows(null, splitNr, splitCount).iterator();
					chunksIter.add(it);
					chunkRows.add(it.hasNext() ? it.next() : null);
				}
			} catch (IOException | ValidationException e) {
				LibsLogger.error(IterateCross.class, "Cannot create iterators for CROSS", e);
			}
		}
		return chunksIter;
	}
}
