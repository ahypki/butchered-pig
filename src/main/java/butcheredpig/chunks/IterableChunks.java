package butcheredpig.chunks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;

import butcheredpig.commands.utils.GroupValue;
import kotlin.NotImplementedError;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

// TODO this class is not yet implemented I see
public class IterableChunks implements Iterable<Row> {
	
	@Expose
	@NotNull
	@AssertValid
	private List<UUID> chunkIds = null;
	
	@Expose
	@NotNull
	@AssertValid
	private GroupValue groupValue = null; 
	
//	private Map<UUID, Chunk> idToChunkCache = new HashMap<>();

	public IterableChunks() {
		
	}
	
	public IterableChunks(List chunks, GroupValue gv) {
		for (Object chunkObj : chunks) {
			if (chunkObj instanceof Chunk) {
				Chunk chunk = (Chunk) chunkObj;
				getChunkIds().add(chunk.getId());
//				idToChunkCache.put(chunk.getId(), chunk);
			} else if (chunkObj instanceof UUID) {
				UUID chunkId = (UUID) chunkObj;
				getChunkIds().add(chunkId);
			} else {
				throw new NotImplementedError("Unimplemented case in IterableChunks constructor for: " + chunkObj);
			}
		}
		setGroupValue(gv);
	}

	public List<UUID> getChunkIds() {
		if (chunkIds == null)
			chunkIds = new ArrayList<>();
		return chunkIds;
	}

	public void setChunkIds(List<UUID> chunkIds) {
		this.chunkIds = chunkIds;
	}

	@Override
	public Iterator<Row> iterator() {
		return new Iterator<Row>() {
			private Iterator<UUID> chunksIter = null;
			private Chunk currentChunk = null;
			private Iterator<Row> currentChunkIter = null;
			private Row row = null;
			
			@Override
			public Row next() {
				try {
					if (chunksIter == null)
						hasNext();
					return row;
				} finally {
					row = null;
				}
			}
			
			@Override
			public boolean hasNext() {
				if (row != null)
					return true;
				
				if (chunksIter == null) {
					chunksIter = getChunkIds().iterator();
				}
				
				while (true) {
					if (currentChunk == null) {
						if (chunksIter.hasNext()) {
							currentChunk = ChunkFactory.getChunk(chunksIter.next());
							try {
								currentChunkIter = currentChunk.iterateOutputRows(getGroupValue(), 0, 1).iterator();
							} catch (IOException | ValidationException e) {
								LibsLogger.error(IterableChunks.class, "Cannot open iterator", e);
								return false;
							}
						} else
							break;
					}
					
					if (row == null) {
						if (currentChunk != null && currentChunkIter.hasNext())
							row = currentChunkIter.next();
						else {
							currentChunk = null;
							currentChunkIter = null;
							continue;
						}
					}
					
					if (row != null)
						return true;
				}
				
				return row != null;
			}
		};
	}

	public GroupValue getGroupValue() {
		return groupValue;
	}

	public void setGroupValue(GroupValue groupValue) {
		this.groupValue = groupValue;
	}
}
