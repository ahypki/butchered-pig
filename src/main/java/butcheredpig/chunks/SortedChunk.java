package butcheredpig.chunks;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.google.gson.annotations.Expose;

import butcheredpig.Settings;
import butcheredpig.commands.SortJob;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.ConnectorUtils;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.HazelcastManager;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class SortedChunk extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID sourceChunkId = null;
		
	@Expose
	@NotEmpty
	@NotNull
	private int sourcePart = -1;
	
	@Expose
	@NotEmpty
	@NotNull
	private int sourcePartsCount = -1;
	
	@Expose
	@NotEmpty
	@NotNull
	private List<String> sortColumns = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	private ConnectorList result = null;
	
	@Expose
	private boolean statusSplitted = false;
	
	@Expose
	private boolean statusSorted = false;
	
	@Expose
	private List<UUID> sortSplits = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	private GroupValue groupValue = null;
	
	private long approxBytes = -1;
	
	private Chunk sourceChunkCache = null;

	public SortedChunk() {
		
	}
	
	public SortedChunk(Chunk chunk, GroupValue gv) {
		setButcheredPigScriptId(chunk.getButcheredPigScriptId());
		setUserId(chunk.getUserId());
		setSourceChunkId(chunk.getId());
		setPrevChunkId(chunk.getId());
		setGroupValue(gv);
	}
	
	@Override
	public String toStringScript() {
		return toString();
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		return this;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		return false;
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.add(new SortJob(getId()).getSaveMutations());
	}
		
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return approxBytes;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getSourceChunk().iterateSourceRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getResult().iterateRows(null, splitNr, splitCount);
	}
	
	private boolean isMoreSplittable() {
		return sourcePartsCount == -1;
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (isFinished())
			return;

		if (getButcheredPigScript().isFinished())
			return;
		
//		if (!getSourceChunk().getso.isDone())
//			return;
		
		try {
			if (statusSplitted == false) {
				// 1 step
				statusSplitted = true;
				
				if (isMoreSplittable()) {
					int partsCount = Settings.ALIAS_SPLIT_COUNT;
					for (int i = 0; i < partsCount; i++) {
						try { 
							SortedChunk sortPart = (SortedChunk) cloneThis();
							sortPart.setId(UUID.random());
							sortPart.setButcheredPigScriptId(getButcheredPigScriptId());
							sortPart.setUserId(getUserId());
							sortPart.setSortColumns(getSortColumns());
							sortPart.setSourceChunkId(getSourceChunkId());
							sortPart.setSourcePart(i);
							sortPart.setSourcePartsCount(partsCount);
							sortPart.setPrevChunkId(getPrevChunkId());
							sortPart.save();
							
							getSortSplits().add(sortPart.getId());
						} catch (ValidationException | IOException e) {
							throw new IOException("Cannot split tables for sorting");
						}
					}
					
					// waiting for the sorted files to join them into one 
				} else {
					// file is not splittable, go to the next step
				}
				
				save();
				return;
			}
			
			if (statusSorted == false) {
				if (isMoreSplittable()) {
					Lock lockJobId = null;
					boolean lockedHere = false;
					try {
						lockJobId = HazelcastManager.getInstance().getCPSubsystem().getLock(getKey());
						if (lockJobId.tryLock(1000, MILLISECONDS) == true) {
							lockedHere = true;
							
							// double check if it was not sorted yet
							SortedChunk sortedCheck = ChunkFactory.getChunk(getId(), SortedChunk.class);
							if (sortedCheck.getProgress().getStatus() == Status.DONE)
								return;
							
							List<Chunk> sortedSplits = new ArrayList<Chunk>();
							
							// check if all sub-jobs are done
							approxBytes = 0;
							for (UUID sortId : getSortSplits()) {
								SortedChunk bp = ChunkFactory.getChunk(sortId, SortedChunk.class);
								if (bp.getProgress().isFailed()) {
									fail(bp.getProgress().getMessage());
									save();
									throw new IOException("SortedChunk failed: " + bp.getProgress().getMessage());
								} else if (bp.getProgress().getStatus() != Status.DONE) {
									LibsLogger.info(SortedChunk.class, "Split sorts ", sortedSplits.size(), " done out of ", Settings.ALIAS_SPLIT_COUNT);
									save();
									return;
								}
								
								sortedSplits.add(bp);
								approxBytes += bp.getSourceApproxBytesSize();
							}
							
							// sorting parts into one combined sorted connector
							// TODO make this connector general
							H5Connector dest = new H5Connector(UUID.random(), new FileExt("sorted-" + getId() + ".h5"));
							
							dest.setColumnDefs(getSourceChunk().getOutputColumnDefList());
							
							getResult().addConnector(dest);
							
							ConnectorList connListSortedSplits = new ConnectorList();
							for (Chunk bp : sortedSplits) {
								connListSortedSplits.addConnector(((SortedChunk) bp).getResult());
							}
							
							Watch combining = new Watch();
							ConnectorUtils.sort(connListSortedSplits,
									dest, 
									getSortColumns().get(0));
							LibsLogger.info(SortedChunk.class, "Sort parts combined in " + combining);
							
							this.statusSorted = true;
							getProgress().setStatus(Status.DONE);
							save();
						}
	
					} catch (Throwable e) {
						LibsLogger.error(SortedChunk.class, "Cannot combine sort", e);
					} finally {
						if (lockJobId != null && lockedHere == true)
							lockJobId.unlock();
					}
				} else {
					// sorting this one connector
					H5Connector dest = new H5Connector(UUID.random(), new FileExt("sorted-" + getId() + ".h5"));
					
					dest.setColumnDefs(getSourceChunk().getOutputColumnDefList());
					
					getResult().addConnector(dest);
					
					ConnectorUtils.sort(getSourceChunk().iterateOutputRows(getGroupValue(), getSourcePart(), getSourcePartsCount()).iterator(),
							getSourceChunk().getOutputColumnDefList(),
							dest, 
							getSortColumns().get(0));
					
					this.statusSorted = true;
					getProgress().setStatus(Status.DONE);
					save();
					
					LibsLogger.info(SortedChunk.class, "part=", getSourcePart(), " sorted");
				}
			}
		} catch (Throwable t) {
			LibsLogger.error(SortedChunk.class, "Cannot run " + getClass() + ": " + (t != null ? t.getMessage() : ""), t);
			
			getProgress().fail("Cannot run " + getClass() + ": " + (t != null ? t.getMessage() : ""));
			save();
		}
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	public Chunk getSourceChunk() throws IOException {
		if (sourceChunkCache == null) {
			sourceChunkCache = ChunkFactory.getChunk(getSourceChunkId());
		}
		return sourceChunkCache;
	}

	public int getSourcePart() {
		return sourcePart;
	}

	public void setSourcePart(int sourcePart) {
		this.sourcePart = sourcePart;
	}

	public int getSourcePartsCount() {
		return sourcePartsCount;
	}

	public void setSourcePartsCount(int sourcePartsCount) {
		this.sourcePartsCount = sourcePartsCount;
	}

	public ConnectorList getResult() {
		if (result == null)
			result = new ConnectorList();
		return result;
	}

	private void setResult(ConnectorList result) {
		this.result = result;
	}

	public List<String> getSortColumns() {
		if (sortColumns == null)
			sortColumns = new ArrayList<String>();
		return sortColumns;
	}

	public void setSortColumns(List<String> sortColumns) {
		this.sortColumns = sortColumns;
	}

	private List<UUID> getSortSplits() {
		if (sortSplits == null)
			sortSplits = new ArrayList<UUID>();
		return sortSplits;
	}

	private void setSortSplits(List<UUID> sortSplits) {
		this.sortSplits = sortSplits;
	}

	public UUID getSourceChunkId() {
		return sourceChunkId;
	}

	public void setSourceChunkId(UUID sourceChunkId) {
		this.sourceChunkId = sourceChunkId;
	}

	public GroupValue getGroupValue() {
		return groupValue;
	}

	public void setGroupValue(GroupValue groupValue) {
		this.groupValue = groupValue;
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {	
		for (UUID uuid : getSortSplits()) {
			Chunk ch = ChunkFactory.getChunk(uuid);
			if (ch != null)
				ch.clear();
		}
	}
}
