package butcheredpig.commands;

public enum JoinType {
	LEFT,
	RIGHT,
	FULL
}
