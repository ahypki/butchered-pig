package butcheredpig.commands;

import static butcheredpig.Const.INTO;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static butcheredpig.Const.STORE;
import static butcheredpig.Const.USING;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.regex.Pattern;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.connectors.BeansConnector;
import net.beanscode.model.connectors.BeansConnectorStatus;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Store extends Chunk {
	
	@Expose
	@NotEmpty
	@NotNull
	private String aliasToStore = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private String destName = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private String using = null;
	
	private Dataset datasetCache = null;
	private Notebook notebookCache = null;
	private Chunk sourceChunkCache = null;
		
	public Store() {
		
	}
	
	public Store(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("STORE %s INTO %s USING %s", 
				getAliasToStore(), 
				getDestName(),
				"");
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// STORE
		ts.expectedAlphanumericWord(STORE.toLowerCase());
		
		// alias
		setAliasToStore(ts.getNextAlphanumericWordWithUnderscore());
		
		// INTO
		ts.expectedAlphanumericWord(INTO.toLowerCase());
		
		// name		
		setDestName(RegexUtils.firstGroup("name[\\s]*=[\\s]*\"([^\\\"]*)\"", ts.checkLine(), Pattern.CASE_INSENSITIVE));
		if (nullOrEmpty(getDestName()))
			setDestName(RegexUtils.firstGroup("name[\\s]*\"([^\\\"]*)\"", ts.checkLine(), Pattern.CASE_INSENSITIVE));
		ts.getNextStringUntil(USING.toLowerCase(), false);
		
		// USING
		ts.expectedAlphanumericWord(USING.toLowerCase());
		
		// using
		setUsing(ts.getNextStringUntil("()", false));
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(STORE.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + INTO.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	private Notebook getNotebook() {
		if (notebookCache == null) {
			try {
				notebookCache = Notebook.getNotebook(getButcheredPigScript().getNotebookId());
			} catch (IOException e) {
				LibsLogger.error(Store.class, "Cannot get Notebook from DB", e);
			}
		}
		return notebookCache;
	}
	
	public boolean isCsv() {
		return getUsing() != null && getUsing().toLowerCase().equals("csv");
	}
	
	public boolean isH5() {
		return getUsing() != null && getUsing().toLowerCase().equals("h5");
	}
	
	public boolean isBeansTable() {
		return getUsing() != null && getUsing().toLowerCase().equals("beanstable");
	}
	
	private Dataset getDataset() throws IOException {
		if (this.datasetCache == null) {
			this.datasetCache = DatasetFactory.getDataset(getButcheredPigScript().getNotebookId());
			
			if (this.datasetCache == null) {
				try {
					final String notbookName = getNotebook() != null ? getNotebook().getName() : "ButcheredPig notebook";
					datasetCache = new Dataset(getButcheredPigScript().getUserId(), notbookName);
					datasetCache.setId(getButcheredPigScript().getNotebookId());
					datasetCache.setDescription("This is automatically generated Dataset from within a Pig script. "
//							+ "It is a read-only Dataset. "
							);
					datasetCache.save();
					
					datasetCache.makeReadOnly();
				} catch (ValidationException e) {
					throw new IOException("Cannot create Dataset for writing", e);
				}
			}
		}
		return this.datasetCache;
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (getSourceChunk() == null) {
			fail("Source alias for " + toString() + " not found");
			save();
			return;
		}
		
		if (!getSourceChunk().isDone())
			return;
		
		if (isBeansTable()) {
			// TODO OPTY
			Table tableCache = new Table(getDataset(), getDestName());
			tableCache.setId(UUID.random());
			tableCache.getMeta().add("queryId", getButcheredPigScriptId().getId());
			tableCache.save();
			
			long rowsSaved = 0;
			Connector conn = BeansSettings.getDefaultConnector(tableCache.getId(), UUID.random().getId());//new H5Connector(tableCache.getId(), null)
			conn.setColumnDefs(getSourceColumnDefList());
			for (Row row : iterateOutputRows(null, 0, 1)) {
//				LibsLogger.info(Store.class, row); // TODO DEBUG
				row.set(Table.COLUMN_TBID, tableCache.getId().getId());
				conn.write(row);
				rowsSaved++;
			}
			conn.close();
			
			LibsLogger.info(Store.class, getAliasToStore() + " saved " + rowsSaved + " rows");
			
			tableCache.addConnector(conn);
			tableCache.save();
			
			new BeansConnector(conn, UUID.random().getId())
				.setStatus(BeansConnectorStatus.COMMITTED)
				.save();
		} else if (isCsv()) {
			FileExt csvFilename = new FileExt(getButcheredPigScript().getContext().getWorkingDir(), getDestName());
			
			if (csvFilename.exists())
				csvFilename.delete();
			
			Connector conn = new PlainConnector(csvFilename, getSourceColumnDefList());
			((PlainConnector) conn).setSimpleHeader(true);
			for (Row row : iterateOutputRows(null, 0, 1)) {
				if (row == null)
					break;
				conn.write(row);
			}
			conn.close();
		} else {
			throw new IOException("Unimplemented STORE " + getUsing());
		}
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	public Chunk getSourceChunk() throws IOException {
		if (sourceChunkCache == null) {
			sourceChunkCache = getButcheredPigScript().getChunkByAlias(getAliasToStore(), this.getPrevChunkId());
		}
		return sourceChunkCache;
	}

	public String getAliasToStore() {
		return aliasToStore;
	}

	public void setAliasToStore(String aliasToStore) {
		this.aliasToStore = aliasToStore;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		Chunk sourceChunk = getSourceChunk();
		if (sourceChunk != null)
			return sourceChunk.getSourceApproxBytesSize();
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return iterateOutputRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	public String getDestName() {
		return destName;
	}

	public void setDestName(String destName) {
		this.destName = destName;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}

	public String getUsing() {
		return using;
	}

	public void setUsing(String using) {
		this.using = using != null ? using.trim() : using;
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
