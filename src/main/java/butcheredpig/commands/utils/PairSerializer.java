package butcheredpig.commands.utils;

import java.lang.reflect.Type;

import org.javatuples.Pair;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.math.ExpressionList;
import net.hypki.libs5.utils.string.StringUtilities;

public class PairSerializer implements JsonSerializer<Pair<String, ExpressionList>>, JsonDeserializer<Pair<String, ExpressionList>> {
	
	@Override
	public JsonElement serialize(Pair<String, ExpressionList> map, Type arg1, JsonSerializationContext arg2) {
		try {
//			return JsonUtils.toJson(map);
			JsonObject jo = new JsonObject();
			jo.addProperty("key", map.getValue0());
			jo.addProperty("value", StringUtilities.serialize(map.getValue1()));
			return jo;
		} catch (Exception e) {
			LibsLogger.error(PairSerializer.class, "Cannot serialize InfMap", e);
			return null;
		}
	}

	@Override
	public Pair<String, ExpressionList> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		try {
			return new Pair<String, ExpressionList>(json.getAsJsonObject().get("key").getAsString(), 
					(ExpressionList) StringUtilities.deserialize(json.getAsJsonObject().get("value").getAsString()));
		} catch (Throwable e) {
			LibsLogger.error(PairSerializer.class, "Cannot deserialize " + json, e);
			return null;
		}
	}
}
