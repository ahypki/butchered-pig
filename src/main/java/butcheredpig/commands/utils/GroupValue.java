package butcheredpig.commands.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.hazelcast.shaded.nonapi.io.github.classgraph.json.JSONUtils;

import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.constraint.AssertValid;

public class GroupValue {
	
	@Expose
	@AssertValid
	private List<Object> values = null;

	public GroupValue() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		int i = 0;
		for (Object o : values) {
			if (i++ > 0)
				sb.append(",");
			sb.append(o);
		}
		sb.append(")");
		return sb.toString();
//		return JsonUtils.objectToString(this);
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GroupValue) {
			GroupValue input = (GroupValue) obj;
			for (int i = 0; i < getValues().size(); i++) {
				if (!getValues().get(i).equals(input.values.get(i)))
					return false;
			}
			return true;
		} if (obj instanceof List) {
			List input = (List) obj;
			for (int i = 0; i < getValues().size(); i++) {
				if (!getValues().get(i).equals(input.get(i)))
					return false;
			}
			return true;
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	public GroupValue add(Object obj) {
		getValues().add(obj);
		return this;
	}

	public List<Object> getValues() {
		if (values == null)
			values = new ArrayList<>();
		return values;
	}

	public void setValues(List<Object> values) {
		this.values = values;
	}
}
