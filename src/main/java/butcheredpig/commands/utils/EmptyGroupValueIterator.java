package butcheredpig.commands.utils;

import java.util.Iterator;

import net.hypki.libs5.db.db.Row;

public class EmptyGroupValueIterator implements Iterable<GroupValue> {

	@Override
	public Iterator<GroupValue> iterator() {
		return new Iterator<GroupValue>() {
			
			@Override
			public GroupValue next() {
				return null;
			}
			
			@Override
			public boolean hasNext() {
				return false;
			}
		};
	}

}
