package butcheredpig.commands.utils;

import com.google.gson.annotations.Expose;

import net.beanscode.model.connectors.ConnectorLink;
import net.sf.oval.constraint.AssertValid;

public class Bag {

	@Expose
	@AssertValid
	private ConnectorLink connectorLink = null;
	
	public Bag() {
		
	}
	
	@Override
	public String toString() {
		return "{}";
	}

	public ConnectorLink getConnectorLink() {
		return connectorLink;
	}

	public void setConnectorLink(ConnectorLink connectorLink) {
		this.connectorLink = connectorLink;
	}
}
