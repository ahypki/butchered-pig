package butcheredpig.commands.utils;

import java.io.IOException;

import org.apache.commons.lang.NotImplementedException;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.RewindableConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;

public class RewindableChunk extends Chunk {
	
	private RewindableConnector conn = null;

	public RewindableChunk() {
		
	}
	
	public RewindableChunk(RewindableConnector conn) {
		this.conn = conn;
	}

	public RewindableConnector getConn() {
		return conn;
	}

	public void setConn(RewindableConnector conn) {
		this.conn = conn;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		return false;
	}

	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		throw new NotImplementedException("Parsing not implemented");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// do nothing
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getConn().getAproxBytesSize();
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getConn().getColumnDefs();
	}

	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount)
			throws IOException, ValidationException {
		if (splitNr == 0)
			return getConn().iterateRows(null);
		return new EmptyRowIterator();
	}

	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getConn().getAproxBytesSize();
	}

	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getConn().getColumnDefs();
	}

	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount)
			throws IOException, ValidationException {
		if (splitNr == 0)
			return getConn().iterateRows(null);
		return new EmptyRowIterator();
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}

	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}

	@Override
	public String toStringScript() {
		return "Not implemented toStringScript";
	}

	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
