package butcheredpig.commands.utils;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

import butcheredpig.UDFs;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.string.StringUtilities;

public class ButcheredRow extends Row implements Serializable {

	public ButcheredRow() {
		
	}
	
	public ButcheredRow(Row r) {
		super(r.getPk(), r.getColumns());
	}
	
	public ButcheredRow(Object pk, Map<String, Object> columns) {
		setPk(pk);
		setColumns(columns);
	}
	
	
	@Override
	public BigDecimal compute(String expr, String varPrefix) {
		// TODO OPTY
		for (String shortname : UDFs.iterateShortNames()) {
			int shornameStart = expr.indexOf(shortname + "(");
			if (shornameStart >= 0) {
				try {
					EvalFunc eval = UDFs.getEvalFunc(shortname);
					String args = expr.substring(shornameStart + shortname.length() + 1, 
							StringUtilities.findClosingBracket(expr, shornameStart + shortname.length()));
					String[] argsArr = StringUtilities.split(args, ',');
					Tuple t = TupleFactory.getInstance().newTuple(argsArr.length);
					int tidx = 0;
					for (String arg : argsArr)
						t.set(tidx++, compute(arg, null));
					Object v = eval.exec(t);
					expr = expr.replace(shortname + "(" + args + ")", String.valueOf(v));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		expr = expr.toLowerCase();
		expr = expr.replace(" and ", " && ");
		expr = expr.replace(" or ", " || ");
		expr = expr.replace(" not ", " ! ");
		
//		Object o = get(expr);
//		if (o != null)
//			return o;
		
		return super.compute(expr, varPrefix);
	}
	
	public Object computeObject(String expr, String varPrefix) {
		if (expr.startsWith("'") && expr.endsWith("'"))
			return expr;
		
		// TODO OPTY
		for (String shortname : UDFs.iterateShortNames()) {
			int shornameStart = expr.indexOf(shortname + "(");
			if (shornameStart >= 0) {
				try {
					EvalFunc eval = UDFs.getEvalFunc(shortname);
					String args = expr.substring(shornameStart + shortname.length() + 1, 
							StringUtilities.findClosingBracket(expr, shornameStart + shortname.length()));
					String[] argsArr = StringUtilities.split(args, ',');
					Tuple t = TupleFactory.getInstance().newTuple(argsArr.length);
					int tidx = 0;
					for (String arg : argsArr) {
						Object o = get(arg);
						if (o != null)
							t.set(tidx++, o);
						else
							t.set(tidx++, compute(arg, null));
					}
					Object v = eval.exec(t);//TableFactory.getTable(new net.hypki.libs5.db.db.weblibs.utils.UUID("3552ee80305882cec6d97fd5cc20dc0f"))
					expr = expr.replace(shortname + "(" + args + ")", String.valueOf(v));
				} catch (Exception e) {
					// TODO remove this try catch
					System.out.println("");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
//		expr = expr.toLowerCase();
		expr = expr.replace(" and ", " && ");
		expr = expr.replace(" or ", " || ");
		expr = expr.replace(" not ", " ! ");
		expr = expr.replace(" AND ", " && ");
		expr = expr.replace(" OR ", " || ");
		expr = expr.replace(" NOT ", " ! ");
		
		// check if the field returns just some value, because then there is no need to compute anything
		Object o = get(expr);
		if (o != null)
			return o;
		
		BigDecimal bd = super.compute(expr, varPrefix);
		if (bd != null)
			return bd;
		
		// check if the expr is already computed
		if (StringUtilities.isAlphanumerical(expr))
			return expr;
		
		if (expr.trim().startsWith("(")
				&& expr.trim().endsWith(")"))
			expr = expr.substring(1, expr.length() - 1);
		
		int q = expr.indexOf(" ? ");
		int twodot = expr.indexOf(" : ");
		if (q > 0 && twodot > 0 && twodot > q) {
			String gr1 = substringGr1(expr, q);
			String gr2 = substringGr2(expr, q + 2, twodot);
			String gr3 = substringGr3(expr, twodot + 2);
			expr = "IF(" + gr1 + ", " + gr2 + ", " + gr3 + ")";
		}
		
		bd = super.compute(expr, varPrefix);
		return bd;
	}

	private String substringGr3(String expr, int twodot) {
		return expr.substring(twodot);
	}

	private String substringGr2(String expr, int q, int twodot) {
		return expr.substring(q, twodot);
	}

	private String substringGr1(String expr, int q) {
		return expr.substring(0, q);
	}
}
