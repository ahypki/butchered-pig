package butcheredpig.commands.utils;

import java.util.Iterator;

import net.hypki.libs5.db.db.Row;

public class EmptyRowIterator implements Iterable<Row> {

	@Override
	public Iterator<Row> iterator() {
		return new Iterator<Row>() {
			
			@Override
			public Row next() {
				return null;
			}
			
			@Override
			public boolean hasNext() {
				return false;
			}
		};
	}

}
