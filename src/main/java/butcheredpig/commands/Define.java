package butcheredpig.commands;

import static butcheredpig.Const.DEFINE;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.UDFs;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Define extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String shortname = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private String fullname = null;
		
	public Define() {
		
	}
	
	public Define(String toParse, ButcheredPigScript butcheredPigScript, Chunk prevChunk) throws IOException, ValidationException {
		super(butcheredPigScript, prevChunk);
		parse(null, toParse);
	}
	
	@Override
	public String toString() {
		return toStringScript();
	}
	
	@Override
	public String toStringScript() {
		return String.format(DEFINE.toString() + " %s %s();", 
				getShortname(), 
				getFullname());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// DEFINE
		ts.expectedAlphanumericWord(DEFINE.toLowerCase());
		
		// short name
		setShortname(ts.getNextAlphanumericWord());
		
		// fullname
		setFullname(ts.toString());
		
		UDFs.putEvalFunc(getShortname(), getFullname());
		
		getProgress().setStatus(Status.DONE);
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().trim();
		return toParse.matches(DEFINE.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// do nothing
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}
		
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return -1;
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return null;
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return null;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		fullname = fullname.trim();
		if (fullname.endsWith(";"));
			fullname = fullname.substring(0, fullname.length());
		if (fullname.endsWith("()"));
			fullname = fullname.substring(0, fullname.length() - 2);
		this.fullname = fullname;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
