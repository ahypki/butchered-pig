package butcheredpig.commands;

import static butcheredpig.Const.DUMP;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Dump extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String toDump = null;
	
	private Chunk toDumpChunk = null;
	
	public Dump() {
		
	}
	
	public Dump(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toString() {
		return String.format("DUMP %s", 
				getToDump());
	}
	
	@Override
	public String toStringScript() {
		return DUMP.toString() + " " + getToDump();
	}
		
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		ts.expectedAlphanumericWord(DUMP.toLowerCase());
		
		setToDump(ts.getNextAlphanumericWord());
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_SPACE + DUMP.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + ".*");
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		if (getToDumpChunk() != null
				&& getToDumpChunk().getProgress().isDone())
			return getToDumpChunk().getSourceApproxBytesSize();
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return iterateOutputRows(gv, splitNr, splitCount);
	}
		
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getToDumpChunk() != null ? getToDumpChunk().iterateOutputRows(gv, splitNr, splitCount) : null;
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (getToDumpChunk() == null || getToDumpChunk().getProgress().isFailed()) {
			LibsLogger.info(Dump.class, "Dump source chunk failed, consuming job");
			fail(getToDumpChunk().getProgress().getMessage());
			save();
			return;
		}
		
		if (getToDumpChunk() == null || !getToDumpChunk().getProgress().isDone()) {
			LibsLogger.info(Dump.class, "Dump is not ready yet");
			return;
		}
		
		if (isOutputGrouped()) {
			// printing grouped output
			for (GroupValue gc : iterateOutputGroupValue()) {
				LibsLogger.info(Dump.class, getToDump(), " (" + gc + ", {");
				for (Row row : iterateOutputRows(gc, 0, 1))
					LibsLogger.info(Dump.class, "     ", row);
				LibsLogger.info(Dump.class, "   })");
			}
			
			LibsLogger.info(Dump.class, getToDump().toString());
			for (Row row : iterateOutputRows(null, 0, 1))
				System.out.println(row.toString());
		} else {
			// printing non grouped output
			for (Row row : iterateOutputRows(null, 0, 1)) {
				LibsLogger.info(Dump.class, getToDump(), ": ", row);
			}
		}
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	private Chunk getToDumpChunk() throws IOException {
		if (toDumpChunk == null)
			toDumpChunk = getButcheredPigScript().getChunkByAlias(getToDump(), this.getPrevChunkId());
		return toDumpChunk;
	}

	public String getToDump() {
		return toDump;
	}

	public void setToDump(String toDump) {
		this.toDump = toDump;
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getToDumpChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}
	

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getToDumpChunk().isOutputGrouped();
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getToDumpChunk().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
