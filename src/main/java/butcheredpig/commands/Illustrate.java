package butcheredpig.commands;

import static butcheredpig.Const.ILLUSTRATE;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Illustrate extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String toIllustrate = null;
	
	private Chunk toIllustrateChunk = null;
	
	public Illustrate() {
		
	}
	
	public Illustrate(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toString() {
		return String.format(ILLUSTRATE.toString() + " " + getToIllustrate());
	}
	
	@Override
	public String toStringScript() {
		return ILLUSTRATE.toString() + " " + getToIllustrate();
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		ts.expectedAlphanumericWord(ILLUSTRATE.toLowerCase());
		
		setToIllustrate(ts.getNextAlphanumericWord());
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_SPACE + ILLUSTRATE.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + ".*");
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return 1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return 1;
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}
		
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (getToIllustrateChunk() == null || getToIllustrateChunk().getProgress().isFailed()) {
			LibsLogger.info(Illustrate.class, "Illustrate source chunk failed, consuming job");
			fail(getToIllustrateChunk().getProgress().getMessage());
			save();
			return;
		}
		
		if (getToIllustrateChunk() == null || !getToIllustrateChunk().getProgress().isDone()) {
			LibsLogger.info(Illustrate.class, "Illustrate is not ready yet");
			return;
		}
		
		LibsLogger.info(Illustrate.class, toString());
		LibsLogger.info(Illustrate.class, getButcheredPigScript().toString());
		for (Chunk ch : getButcheredPigScript())
			ch.illustrate(1);
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	private Chunk getToIllustrateChunk() throws IOException {
		if (toIllustrateChunk == null)
			toIllustrateChunk = getButcheredPigScript().getChunkByAlias(getToIllustrate(), this.getPrevChunkId());
		return toIllustrateChunk;
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return new ColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return new ColumnDefList();
	}
	

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}

	public String getToIllustrate() {
		return toIllustrate;
	}

	public void setToIllustrate(String toIllustrate) {
		this.toIllustrate = toIllustrate;
	}
}
