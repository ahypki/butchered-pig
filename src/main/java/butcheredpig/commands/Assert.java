package butcheredpig.commands;

import static butcheredpig.Const.ASSERT;
import static butcheredpig.Const.BY;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.math.BigDecimal;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.ButcheredRow;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Assert extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String aliasSource = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private String query = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	private String message = null;
	
	/**
	 * If this source chunk is splitted, the list of sub-chunks is stored in this list. All of them 
	 * have to be ready for proceed with this Assert. 
	 */
//	@Expose
//	private List<Chunk> chunks = null;
	
	private Chunk aliasSourceChunk = null;
	
	public Assert() {
		
	}
	
	public Assert(String toParse, ButcheredPigScript butcheredPigScript, Chunk prevChunk) throws IOException, ValidationException {
		super(butcheredPigScript, prevChunk);
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("ASSERT %s %s", 
				getAliasSource(), 
				getQuery().toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// ASSERT
		ts.expectedAlphanumericWord(ASSERT.toLowerCase());
		
		// source alias
		setAliasSource(ts.getNextAlphanumericWord());
		
		// BY
		ts.expectedAlphanumericWord(BY.toLowerCase());

		// query
		String s = ts.getNextStringUntil(",");
		if (s == null)
			s = ts.checkLine(); // read the whole line
		String query = s.endsWith(";") ? s.substring(0, s.length() - 1) : s;
		setQuery(query);
		
		// message (if exists)
		if (!ts.isEmpty()
				&& ts.checkNextPrintableCharacter() != null
				&& ts.checkNextPrintableCharacter().equals(",")) {
			ts.expectedPrintableCharacter(",");
			setMessage(ts.checkLine());
		}
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().trim();
		return toParse.matches(ASSERT.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + BY.toLowerCase() + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// check if source alias is ready
		if (getAliasSourceChunk() == null) {
			LibsLogger.error(Assert.class, "Cannot find source chunk, something is wrong, failing " + getClass().getSimpleName());
			getProgress().setStatus(Status.FAILED);
			save();
			return;
		} else if (getAliasSourceChunk().getProgress().getStatus() == Status.FAILED) {
			LibsLogger.error(Assert.class, "Source alias failed, failing " + getClass().getSimpleName());
			getProgress().setStatus(Status.FAILED);
			save();
			return;
		} else if (getAliasSourceChunk().getProgress().getStatus() == Status.NEW
				|| getAliasSourceChunk().getProgress().getStatus() == Status.RUNNING) {
			LibsLogger.info(Assert.class, "Source alias not ready yet, waiting...");
			return;
		} else if (getAliasSourceChunk().getProgress().getStatus() == Status.DONE) {
			// done, go further
		} else {
			LibsLogger.error(Assert.class, "Source alias has not implemented status: " + getAliasSourceChunk().getProgress());
			getProgress().setStatus(Status.FAILED);
			save();
			return;
		}
		
		LibsLogger.info(Assert.class, "approx size bytes " + getAliasSourceChunk().getSourceApproxBytesSize());
		
		// TODO OPTY split into sub-chunks
		boolean failed = false;
		for (Row row : getAliasSourceChunk().iterateOutputRows(null, 0, 1)) {
			if (new ButcheredRow(row).compute(getQuery(), null).compareTo(new BigDecimal(1)) != 0) {
				getProgress().setStatus(Status.FAILED);
				getProgress().setMessage(isMessageDefined() ? getMessage() : "Row " + row + " failed for the condition " + getQuery());
				save();
				
				failed = true;
				break;
			}
		}
		
		// if the source alias is DONE then this distinct is DONE too
		if (!failed) {
			getProgress().setStatus(Status.DONE);
			save();
		}
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		if (getAliasSourceChunk() != null
				&& getAliasSourceChunk().getProgress().isDone())
			return getAliasSourceChunk().getSourceApproxBytesSize();
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}

	public String getAliasSource() {
		return aliasSource;
	}

	public void setAliasSource(String aliasSource) {
		this.aliasSource = aliasSource;
	}

	private Chunk getAliasSourceChunk() throws IOException {
		if (aliasSourceChunk == null)
			aliasSourceChunk = getButcheredPigScript().getChunkByAlias(getAliasSource(), this.getPrevChunkId());
		return aliasSourceChunk;
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getAliasSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	private void setAliasSourceChunk(Chunk aliasSourceChunk) {
		this.aliasSourceChunk = aliasSourceChunk;
	}

//	private List<Chunk> getChunks() {
//		if (chunks == null)
//			chunks = new ArrayList<>();
//		return chunks;
//	}
//
//	private void setChunks(List<Chunk> chunks) {
//		this.chunks = chunks;
//	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	public boolean isMessageDefined() {
		return notEmpty(getMessage());
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message != null ? message.trim() : message;
	}


	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getAliasSourceChunk().isOutputGrouped();
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
