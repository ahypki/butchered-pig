package butcheredpig.commands;

import static butcheredpig.Const.ASC;
import static butcheredpig.Const.BY;
import static butcheredpig.Const.DESC;
import static butcheredpig.Const.ORDER;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.ChunkFactory;
import butcheredpig.chunks.Progress;
import butcheredpig.chunks.SortedGroupChunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.CollectionUtils;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Order extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String sourceAlias = null;
	
	@Expose
	@NotNull
	@AssertValid
	private List<String> sortBy = null;
		
	@Expose
	private Map<GroupValue, UUID> gv2ChunkId = null;
		
	@Expose
	private List<UUID> sortSplits = null;
	
	@Expose
	private boolean asc = true;
	
	public Order() {
		
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = ORDER %s BY %s", 
				getAliasNew(), 
				getSourceAlias(),
				CollectionUtils.toString(getSortBy(), ", "));
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// ORDER
		ts.expectedAlphanumericWord(ORDER.toLowerCase());
		
		// source alias
		setSourceAlias(ts.getNextAlphanumericWordWithUnderscore());
		
		// BY
		ts.expectedAlphanumericWord(BY.toLowerCase());
		
		// parsing aliases to join
		while (ts.length() > 0) {
			String aliasToSort = ts.getNextAlphanumericWordWithUnderscore();
			
			getSortBy().add(aliasToSort);
			
			if (ts.checkNextAlphabeticalWord().equalsIgnoreCase(ASC.toLowerCase())) {
				ts.expectedAlphanumericWord(ASC.toLowerCase());
			} else if (ts.checkNextAlphabeticalWord().equalsIgnoreCase(DESC.toLowerCase())) {
				ts.expectedAlphanumericWord(DESC.toLowerCase());
				setAsc(false);
			}  
			
			if (ts.checkNextPrintableCharacter() != null && ts.checkNextPrintableCharacter().equals(","))
				ts.expectedPrintableCharacter(",");
		}
		
		return this;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + ORDER.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.add(new SortJob(getId()).getSaveMutations());
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getSourceChunk().getSourceApproxBytesSize();
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getSourceChunk().iterateSourceRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		if (!isDone())
			return new EmptyRowIterator();
		
		if (gv == null) {
			return ChunkFactory.getChunk(getSortSplits().get(0)).iterateOutputRows(null, splitNr, splitCount);
		} else {
			return ChunkFactory.getChunk(getGv2ChunkId().get(gv)).iterateOutputRows(gv, splitNr, splitCount);
		}
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (!getSourceChunk().isDone())
			return;
		
		if (isFinished())
			return;
				
		createAndCheckSortedChunks();
		
		Progress finalProgress = ChunkFactory.getFinalProgress(getSortSplits());
		
		if (!(finalProgress.isDone() || finalProgress.isFailed()))
			return;
		
//		// all sort splits are sorted, now combining them into one result
//		LibsLogger.info(Order.class, "Combining the results");
//		
//		// check if the source is grouped or not
//		if (getSourceChunk().isOutputGrouped()) {
//			// the source is NOT grouped
//			H5Connector dest = new H5Connector(UUID.random(), new FileExt("sorted-" + getId() + ".h5"));
//			GroupValue gv = new GroupValue();
//			
//			dest.setColumnDefs(getSourceChunk().getOutputColumnDefList());
//			
//			List<Iterable<Row>> connListSortedSplits = new ArrayList<>();
//			for (UUID bpid : getSortSplits()) {
//				connListSortedSplits.add(ChunkFactory.getChunk(bpid).iterateOutputRows(null, 0, 1));
//			}
//			
//			ConnectorUtils.sort(connListSortedSplits,
//					dest, 
//					getSortBy().get(0));
//			
//			ConnectorList connList = new ConnectorList();
//			connList.addConnector(dest);
//			getResult().put(gv, connList);
//		} else {
//			// the source is grouped			
//			for (GroupValue gv : getSourceChunk().iterateOutputGroupValue()) {
//				H5Connector dest = new H5Connector(UUID.random(), new FileExt("sorted-" + getId() + "-" + UUID.random() + ".h5"));
//				
//				dest.setColumnDefs(getSourceChunk().getOutputColumnDefList());
//				
//				List<Iterable<Row>> connListSortedSplits = new ArrayList<>();
//				for (UUID bpid : getSortSplits()) {
//					connListSortedSplits.add(ChunkFactory.getChunk(bpid).iterateOutputRows(gv, 0, 1));
//				}
//				
//				ConnectorUtils.sort(connListSortedSplits,
//						dest, 
//						getSortBy().get(0));
//				
//				ConnectorList connList = new ConnectorList();
//				connList.addConnector(dest);
//				getResult().put(gv, connList);
//			}
//		}
		
//		getProgress().setStatus(Status.DONE);
		setProgress(finalProgress);
		save();
	}
	
	private void createAndCheckSortedChunks() throws IOException, ValidationException {
		// first sorting all input sources
		if (getSortSplits().size() == 0) {
			Chunk sourceChunk = getSourceChunk();
			if (sourceChunk.isOutputGrouped()) {
				// source is grouped
				for (GroupValue gv : sourceChunk.iterateOutputGroupValue()) {
					if (gv == null)
						LibsLogger.error(Order.class, "Something is wrong, GroupValue from iterator is null");
					
					SortedGroupChunk sc = new SortedGroupChunk(sourceChunk, gv, getSortBy());
					sc.setPrevChunkId(getPrevChunkId());
					sc.save();
					 
					getSortSplits().add(sc.getId());
					getGv2ChunkId().put(gv, sc.getId());
				}
			} else {
				SortedGroupChunk sc = new SortedGroupChunk(sourceChunk, null, getSortBy());
				sc.setPrevChunkId(getPrevChunkId());
				sc.save();
				 
				getSortSplits().add(sc.getId());
			}
			
			if (getSortSplits().size() == 0)
				getProgress().setStatus(Status.DONE);
			else
				getProgress().setStatus(Status.RUNNING);
			
			save();
		}
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	public Chunk getSourceChunk() throws IOException {
		return getButcheredPigScript().getChunkByAlias(getSourceAlias(), this.getPrevChunkId());
	}

	private List<UUID> getSortSplits() {
		if (sortSplits == null)
			sortSplits = new ArrayList<UUID>();
		return sortSplits;
	}

	private void setSortSplits(List<UUID> sortSplits) {
		this.sortSplits = sortSplits;
	}

	public String getSourceAlias() {
		return sourceAlias;
	}

	public void setSourceAlias(String sourceAlias) {
		this.sourceAlias = sourceAlias;
	}

	public List<String> getSortBy() {
		if (sortBy == null)
			sortBy = new ArrayList<>();
		return sortBy;
	}

	public void setSortBy(List<String> sortBy) {
		this.sortBy = sortBy;
	}

	private boolean isAsc() {
		return asc;
	}

	private void setAsc(boolean asc) {
		this.asc = asc;
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getSourceChunk().isOutputGrouped();
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getSourceChunk().iterateOutputGroupValue();
	}

	public Map<GroupValue, UUID> getGv2ChunkId() {
		if (gv2ChunkId == null)
			gv2ChunkId = new HashMap<GroupValue, UUID>();
		return gv2ChunkId;
	}

	public void setGv2ChunkId(Map<GroupValue, UUID> gv2ChunkId) {
		this.gv2ChunkId = gv2ChunkId;
	}
	
	@Override
	public void clear() throws IOException, ValidationException {	
		for (UUID uuid : getSortSplits()) {
			Chunk ch = ChunkFactory.getChunk(uuid);
			if (ch != null)
				ch.clear();
		}
	}
}
