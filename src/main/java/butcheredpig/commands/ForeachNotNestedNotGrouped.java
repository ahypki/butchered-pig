package butcheredpig.commands;

import static butcheredpig.Const.FOREACH;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

import com.google.gson.annotations.Expose;

import butcheredpig.UDFs;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.ButcheredRow;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import butcheredpig.query.Field;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.plugin.pig.udfs.DsId;
import net.beanscode.plugin.pig.udfs.Pow;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ForeachNotNestedNotGrouped extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String aliasSource = null;
	
	@Expose
	@NotNull
	@AssertValid
	private List<Field> fields = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupSum = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupMin = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupMax = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupCount = null;
	
	@Expose
	@AssertValid
	private Boolean aggregate = null;
	
	// cache
	private Chunk aliasSourceChunk = null;
	private int[] groupSumIdx = null;
	private int[] groupMinIdx = null;
	private int[] groupMaxIdx = null;
	private int[] groupCountIdx = null;
	private Group groupSourceCache = null;
	private ColumnDefList outputColumnDefList = null;
	
	public ForeachNotNestedNotGrouped() {
		
	}
	
	@Override
	public String toStringScript() {
		StringBuilder sb = new StringBuilder();
		for (Field f : getFields())
			sb.append(f + ", ");
		return String.format("%s = FOREACH %s GENERATE %s", 
				getAliasNew(), 
				getAliasSource(),
				sb.toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		throw new NotImplementedException("Direct usage of this class is not implemented, use Foreach class");
	}
	
	public boolean isAggregate() {
		if (aggregate == null) {
			aggregate = false;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					aggregate = true;
					break;
				}
			}
		}
		return aggregate;
	}
	
	public boolean isFieldsList() {
		return this.fields != null && this.fields.size() > 0;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + FOREACH.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	private int[] getGroupSumIdx() {
		if (groupSumIdx == null) {
			List<Integer> idx = new ArrayList<>();
			int fieldIdx = 0;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					if (field.isSUM()) {
						idx.add(fieldIdx);
					}
				}
				fieldIdx++;
			}
			
			groupSumIdx = idx.stream().mapToInt(i->i).toArray();
		}
		return groupSumIdx;
	}
	
	private int[] getGroupMinIdx() {
		if (groupMinIdx == null) {
			List<Integer> idx = new ArrayList<>();
			int fieldIdx = 0;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					if (field.isMIN()) {
						idx.add(fieldIdx);
					}
				}
				fieldIdx++;
			}
			
			groupMinIdx = idx.stream().mapToInt(i->i).toArray();
		}
		return groupMinIdx;
	}
	
	private int[] getGroupMaxIdx() {
		if (groupMaxIdx == null) {
			List<Integer> idx = new ArrayList<>();
			int fieldIdx = 0;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					if (field.isMAX()) {
						idx.add(fieldIdx);
					}
				}
				fieldIdx++;
			}
			
			groupMaxIdx = idx.stream().mapToInt(i->i).toArray();
		}
		return groupMaxIdx;
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getAliasSourceChunk() == null) {
			fail("Source alias for " + toString() + " not found");
			save();
		}
		
		if (!getAliasSourceChunk().isFinished())
			return;
		
		if (!getAliasSourceChunk().isDone()) {
			setProgress(getAliasSourceChunk().getProgress());
			throw new IOException("Source alias failed");
		}
		
		if (isFinished())
			return;
		
		// if foreach is done on grouped data it is not so easy
		if (isAggregate()) {
			int[] sumIdx = getGroupSumIdx();
			int[] minIdx = getGroupMinIdx();
			int[] maxIdx = getGroupMaxIdx();
			
			for (Row row : getAliasSourceChunk().iterateOutputRows(null, 0, 1)) {
				// compute SUMs (if any)
				if (sumIdx.length > 0) {
					for (int i = 0; i < sumIdx.length; i++) {
						Double sum = (Double) getGroupSum().get(sumIdx[i]);
						if (sum == null)
							sum = 0.0;
						String noSUM = getFields().get(sumIdx[i]).getExpression().toString().trim();
						noSUM = noSUM.substring("SUM(".length()); // TODO do it once only
						noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
						getGroupSum().put(sumIdx[i], row.compute(noSUM, null).doubleValue() + sum);
					}
				}
				
				// compute MIN (if any)
				if (minIdx.length > 0) {
					for (int i = 0; i < minIdx.length; i++) {
						Double min = (Double) getGroupMin().get(minIdx[i]);
						if (min == null)
							min = 0.0;
						String noSUM = getFields().get(minIdx[i]).getExpression().toString().trim();
						noSUM = noSUM.substring("MIN(".length()); // TODO do it once only
						noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
						getGroupMin().put(minIdx[i], Math.min(min, row.compute(noSUM, null).doubleValue()));
					}
				}
				
				// compute MAX (if any)
				if (maxIdx.length > 0) {
					for (int i = 0; i < maxIdx.length; i++) {
						Double max = (Double) getGroupMax().get(maxIdx[i]);
						if (max == null)
							max = 0.0;
						String noSUM = getFields().get(maxIdx[i]).getExpression().toString().trim();
						noSUM = noSUM.substring("MAX(".length()); // TODO do it once only
						noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
						getGroupMax().put(maxIdx[i], Math.max(max, row.compute(noSUM, null).doubleValue()));
					}
				}
			}
		}
		
		getProgress().setStatus(Status.DONE);
		save();	
	}
	
	public Iterator<Row> iteratorNonAggregate(GroupValue gv) {
		try {
//			try {
//				LibsLogger.info(Foreach.class, "" + getAliasSourceChunk().isOutputGrouped());
//				if (getAliasSourceChunk().isOutputGrouped()) {
//					return new Iterator<Row>() {
//						Iterator<GroupValue> gvIter = getAliasSourceChunk().iterateOutputGroupValue().iterator();
//						private Iterator<Row> allRows = null;
//						
//						@Override
//						public Row next() {
//							if ()
//							return null;
//						}
//						
//						@Override
//						public boolean hasNext() {
//							// TODO Auto-generated method stub
//							return false;
//						}
//					};
////					for (GroupValue gv : getAliasSourceChunk().iterateOutputGroupValue())
////						LibsLogger.info(Foreach.class, "" + gv);
//				}
//			} catch (Throwable t) {
//				System.out.println(t);
//			}
			
			return new Iterator<Row>() {
				// TODO implement isNestedForeach() ? getNestedScript().iter
				private Iterator<Row> allRows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
				
				@Override
				public Row next() {
					Row rowOriginal = allRows.next();
					if (rowOriginal == null)
						return null;
					
					Row newNew = new Row(rowOriginal.getPk());
					ButcheredRow row = new ButcheredRow(rowOriginal);
					
					for (Field field : getFields()) {
						try {
							if (field.isAsterisk()) {
								
								// rewritting all source fields
								newNew.addColumns(row.getColumns(), null);
								
							} else if (field.isName()) {
								
								Object v = row.get(field.getName());
								if (v == null)
									// looking for a column which ends with this new name
									v = row.getColumnWhichEndsWith(field.getName());
								newNew.addColumn(field.getNewName(), v);
								
							} else if (field.isExpression()) {
								
								newNew.addColumn(field.getColumnList().get(0).getName(), 
										row.computeObject(field.getExpression().toString(), null));
								
								// TODO DEBUG
//								if (field.getExpression().getMathExpr().toLowerCase().startsWith("dsid(")) {
//									try {
//										Tuple t = TupleFactory.getInstance().newTuple(1);
//										t.set(0, row.get("tbid"));
//										newNew.addColumn(field.getColumnList().get(0).getName(),
//												new DsId().exec(t));
//									} catch (IOException e) {
//										// TODO Auto-generated catch block
//										e.printStackTrace();
//									}
//								} else
//									newNew.addColumn(field.getColumnList().get(0).getName(), 
//											row.compute(field.getExpression().toString(), null));
								
							} else {
								throw new NotImplementedException("not implemented");
							}
						} catch (Throwable t) {
							LibsLogger.error(ForeachNotNestedNotGrouped.class, "Cannot compute field, skipping row...", t);
							throw t;
						}
					}
					return newNew;
				}
				
				@Override
				public boolean hasNext() {
					return allRows.hasNext();
				}
			};
		} catch (IOException | ValidationException e) {
			LibsLogger.error(ForeachNotNestedNotGrouped.class, "Cannot  open iterator for " + getAliasSource(), e);
			return null;
		}
	}

	public Iterator<Row> iterator() {
		if (isAggregate()) {
			return new Iterator<Row>() {
				private Row 					currentRow = null;
				
				private Iterator<GroupValue> 	iterGroupValue = null;
				private GroupValue 				currentGroupValue = null;
				
				private Iterator<Row> 			groupValueRows = null;
				
				@Override
				public Row next() {
					if (iterGroupValue == null)
						hasNext();
					return currentRow;
				}
				
				@Override
				public boolean hasNext() {
					if (groupValueRows != null) {
						if (groupValueRows.hasNext()) {
							currentRow.addColumns(groupValueRows.next().getColumns());
							return true;
						} else {
							groupValueRows = null; // no more values for flatten
						}
					}
					
					if (iterGroupValue == null) {
						try {
							// TODO parallel
							iterGroupValue = ((Group) ForeachNotNestedNotGrouped.this.getAliasSourceChunk()).iterateOutputGroupValue().iterator();
						} catch (IOException | ValidationException e) {
							throw new RuntimeException("Canot iterate over group values");
						}
					}
					
					currentGroupValue = iterGroupValue.hasNext() ? iterGroupValue.next() : null;
					
					if (currentGroupValue == null)
						return false;
					
					// TODO implement FLATTEN
					
					currentRow = new ButcheredRow(UUID.random(), null);
					int fieldIdx = 0;
					for (Field field : getFields()) {
						if (field.isName()
								&& field.getName().equals("group"))
							currentRow.addColumn(field.getColumnList().get(0).getName(), 
									currentGroupValue.getValues().get(0)); // TODO implement multi values
						else if (field.isExpression()
								&& field.getExpression().toString().equals("group"))
							currentRow.addColumn(field.getColumnList().get(0).getName(), 
									currentGroupValue.getValues().get(0)); // TODO implement multi values
						else if (field.isSUM()) {
//							String noSUM = field.getExpression().toString().trim();
//							noSUM = noSUM.substring("SUM(".length()); // TODO do it once only
//							noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
							currentRow.addColumn(field.getColumnList().get(0).getName(), 
									getGroupSum().get(fieldIdx)); // TODO implement multi values
						} else if (field.isMIN()) {
							currentRow.addColumn(field.getColumnList().get(0).getName(), 
									getGroupMin().get(fieldIdx)); // TODO implement multi values
						} else if (field.isMAX()) {
							currentRow.addColumn(field.getColumnList().get(0).getName(), 
									getGroupMax().get(fieldIdx)); // TODO implement multi values
						} else if (field.isFlatten()) {
							try {
								groupValueRows = ((Group) ForeachNotNestedNotGrouped.this.getAliasSourceChunk()).iterateRows(currentGroupValue).iterator();
								
								if (groupValueRows.hasNext()) {
									// TODO it will get only one results from this groupValueRows? What about the rest of the rows?
									currentRow.addColumns(groupValueRows.next().getColumns());
									return true;
								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
//						} else if (field.isName()) {
//							currentRow.addColumn(field.getName(), 
//									oldRow.get(field.getName()));
						} else
							throw new NotImplementedException("Unimplemented case for field " + field);
						fieldIdx++;
					}
					
					return true;
				}
			};
		} else {
			try {
				if (getAliasSourceChunk().isOutputGrouped()) {
					return new Iterator<Row>() {
						private Iterator<GroupValue> gvIter = null;
						private GroupValue gv = null;
						private Iterator<Row> rows = null;
						
						@Override
						public Row next() {
							if (!hasNext())
								return null;
							
							if (rows.hasNext())
								return rows.next();
							
							rows = null;
							if (!hasNext())
								return null;
							
							return null;
						}
						
						@Override
						public boolean hasNext() {
							if (gvIter == null) {
								try {
									gvIter = getAliasSourceChunk().iterateOutputGroupValue().iterator();
								} catch (IOException | ValidationException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							if (rows == null) {
								gv = gvIter.next();
								if (gv == null)
									return false;
								
								try {
									rows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
								} catch (IOException | ValidationException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
							return rows.hasNext();
						}
					};
				}
			} catch (IOException | ValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return iteratorNonAggregate(null);
		}
	}

	public String getAliasSource() {
		return aliasSource;
	}

	public void setAliasSource(String aliasSource) {
		this.aliasSource = aliasSource;
	}
	
	public void setAliasSource(Chunk aliasSource) {
		this.aliasSource = aliasSource.getAliasNew();
		this.aliasSourceChunk = aliasSource;
	}

	public List<Field> getFields() {
		if (fields == null)
			fields = new ArrayList<>();
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getAliasSourceChunk() != null ? getAliasSourceChunk().getSourceApproxBytesSize() : -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		if (splitNr == 0)
			return new Iterable<Row>() {
				@Override
				public Iterator<Row> iterator() {
					return ForeachNotNestedNotGrouped.this.iterator();
				}
			};
		else 
			return new EmptyRowIterator();
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getAliasSourceChunk().getSourceColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		if (outputColumnDefList == null) {
			
//			outputColumnDefList = null;// TODO remove it later

			ColumnDefList cdf = new ColumnDefList();
			for (Field f : getFields()) {
				try {
					if (f.isAsterisk()) {
						
						for (ColumnDef c : getAliasSourceChunk().getOutputColumnDefList())
							cdf.add(c);
						
					} else if (f.isFlatten()) {
						
						for (ColumnDef col : f.getColumnList()) {
							if (col.getType() == ColumnType.UNKNOWN) {
								ColumnDef colDefFromSource = getAliasSourceChunk().getOutputColumnDefList()
										.getColumn(col.getName());
								if (colDefFromSource != null) {
									// getting type from the columns list from the source alias
									col.setType(colDefFromSource.getType());
									continue;
								}
	
								String fName = f.getName() != null ? f.getName() : f.getExpression().toString();
								colDefFromSource = getAliasSourceChunk().getOutputColumnDefList()
										.getColumn(fName.replace("::", "."));
								if (colDefFromSource != null) {
									// getting type from the columns list from the source alias
									col.setType(colDefFromSource.getType());
									continue;
								} else if (f.isFunction()) {
									try {
										String funcName = f.getFunctionName();
										if (funcName.equals("MAX"))
											col.setType(ColumnType.DOUBLE);
										else if (funcName.equals("MIN"))
											col.setType(ColumnType.DOUBLE);
										else if (funcName.equals("SUBSTRING"))
											col.setType(ColumnType.STRING);
										else
											col.setType(ColumnType.fromClass(UDFs.getEvalFunc(funcName).getReturnType()));
									} catch (Throwable e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
	
								if (col.getType() == ColumnType.UNKNOWN)
									col.setType(ColumnType.DOUBLE);
								// TODO implement it better
	//									throw new NotImplementedException("Cannot determine type");
							}
	
							cdf.add(col);
						}
					} else {
						ColumnDef col = new ColumnDef(f.getNewName(), null, ColumnType.UNKNOWN);
						ColumnDef colDefFromSource = getAliasSourceChunk().getOutputColumnDefList().getColumn(f.getName());
						
						if (colDefFromSource == null) {
							// last chance
							// TODO ugly
							colDefFromSource = getAliasSourceChunk().getOutputColumnDefList().getColumnWhichEndsWith(f.getName());
						}
						
						if (colDefFromSource != null)
							// getting type from the columns list from the source alias
							col.setType(colDefFromSource.getType());
						else if (f.isFunction()) {
							EvalFunc eval = UDFs.getEvalFunc(f.getFunctionName());
							if (eval != null)
								col.setType(ColumnType.fromClass(eval.getReturnType()));
						} else {
							col.setType(ColumnType.fromClass(f.determineType()));
						}
	
						if (col.getType() == ColumnType.UNKNOWN) {
							// super last chance -> determine type based on the value
							try {
								if (getAliasSourceChunk().isDone()) {
									Row r = getAliasSourceChunk().iterateOutputRows(null, 0, 1).iterator().next();
									Object o = new ButcheredRow(r).computeObject(f.toString(), null);
									col.setType(ColumnType.parse(o));
								}
							} catch (Exception e) {
								throw new IOException("Cannot determine type based on the value", e);
							}
							
							if (col.getType() == ColumnType.UNKNOWN)
								throw new NotImplementedException("Cannot determine type");
						}
	
						cdf.addColumnDef(col);
					}
				} catch (Throwable t) { // TODO remove later
					LibsLogger.error(ForeachNotNestedNotGrouped.class, "Column definition failed XXYYXXX", t);
					throw t;
				}

				outputColumnDefList = cdf;
				// return getAliasSourceChunk().getSourceColumnDefList();
			}
		}
		return outputColumnDefList;
	}

	private Chunk getAliasSourceChunk() throws IOException {
		if (aliasSourceChunk == null)
			aliasSourceChunk = getButcheredPigScript().getChunkByAlias(getAliasSource(), this.getPrevChunkId());
		return aliasSourceChunk;
	}

	private void setAliasSourceChunk(Chunk aliasSourceChunk) {
		this.aliasSourceChunk = aliasSourceChunk;
	}

	private Map<Integer, Object> getGroupSum() {
		if (groupSum == null)
			groupSum = new HashMap<>();
		return groupSum;
	}

	private void setGroupSum(Map<Integer, Object> groupSum) {
		this.groupSum = groupSum;
	}

	private Map<Integer, Object> getGroupCount() {
		if (groupCount == null)
			groupCount = new HashMap<>();
		return groupCount;
	}

	private void setGroupCount(Map<Integer, Object> groupCount) {
		this.groupCount = groupCount;
	}
	
//	@Override
	public void setButcheredPigScriptId(UUID butcheredPigScriptId) {
		super.setButcheredPigScriptId(butcheredPigScriptId);
	}

	private Map<Integer, Object> getGroupMin() {
		if (groupMin == null)
			groupMin = new HashMap<>();
		return groupMin;
	}

	private void setGroupMin(Map<Integer, Object> groupMin) {
		this.groupMin = groupMin;
	}

	private Map<Integer, Object> getGroupMax() {
		if (groupMax == null)
			groupMax = new HashMap<>();
		return groupMax;
	}

	private void setGroupMax(Map<Integer, Object> groupMax) {
		this.groupMax = groupMax;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
