package butcheredpig.commands;

import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static butcheredpig.Const.UNION;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.ChunkFactory;
import butcheredpig.chunks.IterableChunks;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Union extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private List<String> aliasesToUnion = null;
	
	public Union() {
		
	}
	
	public Union(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		StringBuilder sb = new StringBuilder();
		for (String f : getAliasesToUnion())
			sb.append(f + ", ");
		return String.format("%s = UNION %s", 
				getAliasNew(), 
				sb.toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// DISTINCT
		ts.expectedAlphanumericWord(UNION.toLowerCase());
		
		// source alias
		while (ts.length() > 0) {
			getAliasesToUnion().add(ts.getNextAlphanumericWordWithUnderscore());
			
			if (ts.checkNextPrintableCharacter() != null
					&& ts.checkNextPrintableCharacter().equals(","))
				ts.expectedPrintableCharacter(",");
		}
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().trim();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + UNION.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// do nothing actually, only check if the source aliases are ready
		for (String oneToUnion : getAliasesToUnion()) {
			Chunk ch = getButcheredPigScript().getChunkByAlias(oneToUnion, getId());
			if (!ch.isDone())
				return;
		}
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return iterateOutputRows(gv, splitNr, splitCount);
	}
	
//	@Override
//	public ConnectorList getSourceConnector() throws IOException, ValidationException {
//		ConnectorList connList = new ConnectorList();
//		
//		for (String oneAliasToUnion : getAliasesToUnion()) {
//			Chunk ch = getButcheredPigScript().getChunkByAlias(oneAliasToUnion, this.getId());
//			connList.addConnector(ch.getSourceConnector());
//		}
//		
//		return connList;
//	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		List<UUID> chunkIds = new ArrayList<>();
		
		for (String oneAliasToUnion : getAliasesToUnion()) {
			Chunk ch = getButcheredPigScript().getChunkByAlias(oneAliasToUnion, this.getPrevChunkId());
			chunkIds.add(ch.getId());
		}
		
		return new IterableChunks(chunkIds, gv);
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		long bytesAllChunks = 0;
		
		for (String oneToUnion : getAliasesToUnion()) {
			Chunk ch = getButcheredPigScript().getChunkByAlias(oneToUnion, getId());
			
			if (ch == null) {
				LibsLogger.error(Union.class, "Something is wrong, there is no chunk " + oneToUnion);
				return -1;				
			} else { 
				long bytesOneChunk = ch.getSourceApproxBytesSize();
				if (bytesOneChunk == -1)
					return -1;
				else
					bytesAllChunks += bytesOneChunk;
			}
		}
		
		return bytesAllChunks;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		for (String oneToUnion : getAliasesToUnion()) {
			Chunk ch = getButcheredPigScript().getChunkByAlias(oneToUnion, getId());
			return ch.getOutputColumnDefList();
		}
		return null;
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	public List<String> getAliasesToUnion() {
		if (aliasesToUnion == null)
			aliasesToUnion = new ArrayList<>();
		return aliasesToUnion;
	}

	public void setAliasesToUnion(List<String> toUnion) {
		this.aliasesToUnion = toUnion;
	}


	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
