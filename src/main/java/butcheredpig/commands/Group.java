package butcheredpig.commands;

import static butcheredpig.Const.BY;
import static butcheredpig.Const.GROUP;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import butcheredpig.Settings;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.collections.InfMap;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.math.Expression;
import net.hypki.libs5.utils.math.ExpressionList;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Group extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String aliasSource = null;
	
	@Expose
	@NotNull
	@AssertValid
	private boolean byAll = false;
	
	@Expose
	@AssertValid
	private String byField = null;
	
	@Expose
	@AssertValid
	private Expression byExpression = null;
	
	@Expose
	@AssertValid
	private ExpressionList byTuple = null;
	
	@Expose
	@AssertValid
	private InfMap valToConnector = null;
	
	private Map<GroupValue, Connector> valToConnectorCache = null;
	
	private Chunk aliasSourceChunk = null;
	private ColumnDefList outputColumnDefList = null;
	
	public Group() {
		
	}
	
	public Group(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = GROUP %s BY %s", 
				getAliasNew(), 
				getAliasSource(),
				isByAll() ? "ALL" 
						: (isByExpression() ? getByExpression().toString() : 
							(isByField() ? getByField() : getByTuple().toString())));
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		ts.expectedPrintableCharacter("=");
		
		// GROUP
		ts.expectedAlphanumericWord(GROUP.toLowerCase());
		
		// source alias
		setAliasSource(ts.getNextAlphanumericWord());
		
		String nextWord = ts.checkNextAlphabeticalWord();
		if (notEmpty(nextWord) && nextWord.toLowerCase().equals(BY.toLowerCase())) {
			// BY
			ts.expectedAlphanumericWord(nextWord.toLowerCase());
			
			// parsing field, or a list of fields
			if (ts.checkNextPrintableCharacter().equals("(")) {
				EscapeString es = new EscapeString(ts.checkLine());
				es.escapeAll("(\\([^\\)\\(]*\\))");
				
				int semiIdx = es.getEscapedText().indexOf(',');
				String fields = semiIdx > 0 ? es.getEscapedText().substring(0, semiIdx) : es.getEscapedText();
				fields = es.deescapeString(fields);
				
				String newTs = semiIdx > 0 ? es.getEscapedText().substring(semiIdx) : "";
				newTs = es.deescapeString(newTs);
				ts = new TrickyString(newTs);
				
				ExpressionList el = new ExpressionList(fields);
				if (el.getExpressions().size() == 1) {
					Expression e = el.getExpressions().get(0);
					if (e.isField())
						setByField(e.getField());
					else
						setByExpression(e);
				} else
					setByTuple(el);
			} else {
				// one field
				String fieldOrExpr = ts.getNextStringUntil(";", false);
				if (StringUtilities.nullOrEmpty(fieldOrExpr))
					fieldOrExpr = ts.getLine();
				
				if (fieldOrExpr.trim().matches("\\w[\\w\\d\\_]*"))
					setByField(fieldOrExpr.trim());
				else
					setByExpression(new Expression(fieldOrExpr));
			}
			
			if (ts.checkLine().trim().length() > 0)
				ts.expectedPrintableCharacter(";");
		} else {
			// group by all
			setByAll(true);
		}
				
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + GROUP.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (isFinished()) 
			return;
		
		if (!getAliasSourceChunk().isFinished())
			return;
		
		// going over source rows and putting rows to different connectors, depending on their values
		for (Row row : iterateSourceRows(null, 0, 1)) {
			GroupValue gv = new GroupValue();
			if (isByField())
				gv.add(row.get(getByField()));
			else
				throw new NotImplementedException("Not implemented group by ");
			
			Connector out = getConnector(gv);
			if (out == null) {
				UUID id = UUID.random();
				out = new H5Connector(id, new FileExt(Settings.getTempFolder().getAbsolutePath(), id.getId() + ".h5"));
				
				out.setColumnDefs(getSourceColumnDefList());
				
				getValToConnector().put(out.getInitParams(), gv.getValues().toArray());
				getValToConnectorCache().put(gv, out);
				// TODO if it is more than xx memory save this map and create a new one
			}
			
			out.write(row);
		}
		
		for (Connector out : getValToConnectorCache().values())
			out.close();
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	private Connector getConnector(GroupValue groupValue) {
		try {
			Connector out = getValToConnectorCache().get(groupValue);
			if (out != null)
				return out;
			
			ConnectorInitParams iniParams = (ConnectorInitParams) getValToConnector().get(groupValue.getValues().toArray());
			if (iniParams != null) {
				out = new H5Connector();
				out.setInitParams(iniParams);

				getValToConnectorCache().put(groupValue, out);
			}
			
			return out;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean isByField() {
		return this.byField != null;
	}
	
	public boolean isByExpression() {
		return this.byExpression != null;
	}
	
	public boolean isByTuple() {
		return this.byTuple != null;
	}

	public String getAliasSource() {
		return aliasSource;
	}

	public void setAliasSource(String aliasSource) {
		this.aliasSource = aliasSource;
	}

	public boolean isByAll() {
		return byAll;
	}

	public void setByAll(boolean byAll) {
		this.byAll = byAll;
	}

	private Chunk getAliasSourceChunk() throws IOException {
		if (aliasSourceChunk == null)
			aliasSourceChunk = getButcheredPigScript().getChunkByAlias(getAliasSource(), this.getPrevChunkId());
		return aliasSourceChunk;
	}

	private void setAliasSourceChunk(Chunk aliasSourceChunk) {
		this.aliasSourceChunk = aliasSourceChunk;
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getAliasSourceChunk().getSourceColumnDefList();
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		if (outputColumnDefList == null)
			outputColumnDefList = getAliasSourceChunk()
									.getOutputColumnDefList()
									.cloneThis()
									.addPrefix(getAliasSource() + ".");
		return outputColumnDefList;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getAliasSourceChunk().getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
//		return getSourceConnector().iterateRows(null, splitNr, splitCount);
		assertTrue(gv == null, "Source alias for group should no be used with groups, right?");
		return getAliasSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
		
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					private Iterator<Row> it = splitNr == 0 
							? iterateRows(gv).iterator() 
							: new EmptyRowIterator().iterator();//s getAliasSourceChunk().iterateOutputRows(splitNr, splitCount).iterator();
					
					@Override
					public Row next() {
						// TODO OPTY
						Row r = it.next();
						if (r == null)
							return null;
						Row newR = new Row(r.getPk());
						for (Map.Entry<String, Object> e : r.getColumns().entrySet())
							newR.addColumn(getAliasSource() + "." + e.getKey(), e.getValue());
						return newR;
					}
					
					@Override
					public boolean hasNext() {
						return it.hasNext();
					}
				};
			}
		};
	}

	public String getByField() {
		return byField;
	}

	public void setByField(String byField) {
		this.byField = byField;
	}

	public Expression getByExpression() {
		return byExpression;
	}

	public void setByExpression(Expression byExpression) {
		this.byExpression = byExpression;
	}

	public ExpressionList getByTuple() {
		if (byTuple == null)
			byTuple = new ExpressionList();
		return byTuple;
	}

	public void setByTuple(ExpressionList byTuple) {
		this.byTuple = byTuple;
	}

	public InfMap getValToConnector() {
		if (valToConnector == null)
			valToConnector = new InfMap();
		return valToConnector;
	}

	public void setValToConnector(InfMap valToConnector) {
		this.valToConnector = valToConnector;
	}

	private Map<GroupValue, Connector> getValToConnectorCache() {
		if (valToConnectorCache == null)
			valToConnectorCache = new HashMap<>();
		return valToConnectorCache;
	}

	private void setValToConnectorCache(Map<GroupValue, Connector> valToConnectorCache) {
		this.valToConnectorCache = valToConnectorCache;
	}
	
	public Iterable<Row> iterateRows(GroupValue groupValue) {
		if (groupValue == null) {
			// iterating over all GroupValue values
			ConnectorList connList = new ConnectorList();
			for (List gvList : getValToConnector().iterateKeys()) {
				assertTrue(gvList.size() == 1, "Expected only 1 element in the list of GroupValue list. TODO");
				ConnectorInitParams connInitParams = (ConnectorInitParams) getValToConnector().get(gvList.toArray());
				H5Connector out = new H5Connector();
				out.setInitParams(connInitParams);
				connList.addConnector(out);
			}
			return connList;
		}
		return getConnector(groupValue).iterateRows(null);
	}

	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
//		for(List l : getValToConnector().iterateKeys())
//			System.out.println(l);
		return new Iterable<GroupValue>() {
			@Override
			public Iterator<GroupValue> iterator() {
				return new Iterator<GroupValue>() {
					private Iterator<List> iterGroupValueString = getValToConnector().iterateKeys().iterator();
					
					@Override
					public GroupValue next() {
						List tmp = iterGroupValueString.hasNext() ? iterGroupValueString.next() : null;
						if (tmp != null) {
							GroupValue gv = new GroupValue();
							for (Object o : tmp)
								gv.add(o);
							return gv;
						} else
							return null;
					}
					
					@Override
					public boolean hasNext() {
						return iterGroupValueString.hasNext();
					}
				};
			}
		};
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return true;
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
