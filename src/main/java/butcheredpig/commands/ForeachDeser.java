package butcheredpig.commands;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Progress;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;

public class ForeachDeser implements JsonDeserializer<Foreach> {

	@Override
	public Foreach deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		try {
			JsonObject jo = json.getAsJsonObject();
			Foreach foreach = new Foreach();

			if (jo.get("type") != null)
				foreach.setType(jo.get("type").getAsString());
			if (jo.get("aliasNew") != null)
				foreach.setAliasNew(jo.get("aliasNew").getAsString());
			if (jo.get("progress") != null)
				foreach.setProgress(JsonUtils.fromJson(jo.get("progress").getAsJsonObject(), Progress.class));
			if (jo.get("butcheredPigScriptId") != null)
				foreach.setButcheredPigScriptId(JsonUtils.fromJson(jo.get("butcheredPigScriptId").getAsJsonObject(), UUID.class));
			if (jo.get("prevChunkId") != null)
				foreach.setPrevChunkId(JsonUtils.fromJson(jo.get("prevChunkId").getAsJsonObject(), UUID.class));
			if (jo.get("foreach") != null) {
				JsonObject f = jo.get("foreach").getAsJsonObject();
				foreach.setForeach((Chunk) JsonUtils.fromJson(f, Class.forName(f.get("type").getAsString())));
			}
			
			return foreach;
		} catch (Throwable e) {
			LibsLogger.error(ForeachDeser.class, "Cannot deserialize Foreach", e);
			return null;
		}
	}
}
