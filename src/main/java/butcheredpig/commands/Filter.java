package butcheredpig.commands;

import static butcheredpig.Const.BY;
import static butcheredpig.Const.FILTER;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;

import org.python.modules.errno;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.ButcheredRow;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Filter extends Chunk {
	
	private static int CHECK_BUTCHERED_PIG = 10_000; // TODO OPTY CONF
	
	@Expose
	@NotEmpty
	@NotNull
	private String source = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private String query = null;
	
	private Chunk sourceChunk = null;
		
	public Filter() {
		
	}
	
	public Filter(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = FILTER %s BY %s", 
				getAliasNew(), 
				getSource(),
				getQuery().toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");
		
		// FILTER
		ts.expectedAlphanumericWord(FILTER.toLowerCase());
		
		// source alias
		setSource(ts.getNextAlphanumericWord());
		
		// BY
		ts.expectedAlphanumericWord(BY.toLowerCase());
		
		// query
		String s = ts.checkLine();
		String query = s.endsWith(";") ? s.substring(0, s.length() - 1) : s;
		setQuery(query);
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + FILTER.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + BY.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getSourceChunk().getSourceApproxBytesSize(); //getButcheredPigScript().getChunkByAlias(getSource(), this.getPrevChunkId()).getSourceApproxBytesSize();
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return -1;
	}
	
	public Chunk getSourceChunk() throws IOException {
		if (sourceChunk == null)
			sourceChunk = getButcheredPigScript().getChunkByAlias(getSource(), this.getPrevChunkId());
		return sourceChunk;
	}
		
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new H5Connector(getId(), new File("chunk-" + getId())).iterateRows(null, splitNr, splitCount);
//		return new Iterable<Row>() {
//			
//			@Override
//			public Iterator<Row> iterator() {
//				return new Iterator<Row>() {
//					private int rowCounter = 0;
//					
//					private Iterator<Row> notFiltered = null;
//					
//					private Row nextRow = null;
//					
//					@Override
//					public Row next() {
//						if (notFiltered == null)
//							hasNext();
//						return nextRow;
//					}
//					
//					@Override
//					public boolean hasNext() {
//						if ((rowCounter++ % CHECK_BUTCHERED_PIG) == 0) {
//							try {
//								ButcheredPigScript bp = ButcheredPigScriptFactory.getButcheredPigScript(getButcheredPigScriptId());
//								if (bp != null && bp.isFinished()) {
//									LibsLogger.info(Filter.class, "ButcheredPig is finished alread, closing this chunk");
//									return false;
//								}
//							} catch (IOException e) {
//								LibsLogger.error(Filter.class, "Cannot check if ButcheredPig is still running", e);
//							}
//						}
//						
//						if (notFiltered == null) {
//							try {
//								notFiltered = getSourceChunk().iterateOutputRows(gv, splitNr, splitCount).iterator();
//							} catch (IOException | ValidationException e) {
//								LibsLogger.error(Filter.class, "Cannot create iterator for source for filter: " + this.toString(), e);
//								return false;
//							}
//						}
//						
//						while (notFiltered.hasNext()) {
//							Row r = notFiltered.next();
//							try {
//								if (r != null 
//										&& new ButcheredRow(r).compute(getQuery(), null).compareTo(new BigDecimal(1)) == 0) {
//									nextRow = r;
//									return true;
//								}
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						}
//						
//						return false;
//					}
//				};
//			}
//		};
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (getSourceChunk() == null) {
			fail("Source alias for " + toString() + " not found");
			save();
			return;
		}
		
		if (!getSourceChunk().isDone())
			return;
		
		// creating the connector for joined rows
		String tbid = null;//
		for (Row r : getSourceChunk().iterateOutputRows(null, 0, 1)) {
			if (r.contains("tbid"))
				tbid = r.get("tbid").toString();
			break;
		}
		H5Connector h5 = new H5Connector(tbid != null ? new UUID(tbid) : getId(), new File("chunk-" + getId()));
			
		// setting output columns defs
		h5.setColumnDefs(getOutputColumnDefList());
		
		// filtering the input rows
		final BigDecimal one = new BigDecimal(1);
		ButcheredRow bpRow = new ButcheredRow();
		int readRows = 0;
		int writtenRows = 0;
		for (Row r : getSourceChunk().iterateOutputRows(null, 0, 1)) {
			if (r != null) {
				bpRow.setColumns(r.getColumns());
				if (((Comparable) bpRow.computeObject(getQuery(), null)).compareTo(one) == 0) {
					LibsLogger.info(Filter.class, bpRow.get("tphys"), bpRow.get("idd1"), bpRow.get("idd2"), bpRow.get("ik1"), bpRow.get("ik2"));
					h5.write(r);
					writtenRows++;
				}
			}
			
			if ((readRows++ % CHECK_BUTCHERED_PIG) == 0) {
				LibsLogger.info(Filter.class, "Read " + readRows + " rows in filter " + toString());
				try {
					ButcheredPigScript bp = ButcheredPigScriptFactory.getButcheredPigScript(getButcheredPigScriptId());
					if (bp != null && bp.isFinished()) {
						fail("ButcheredPig is finished already, closing this chunk");
						save();
						return;
					}
				} catch (IOException e) {
					LibsLogger.error(Filter.class, "Cannot check if ButcheredPig is still running", e);
				}
			}
		}
		
		LibsLogger.info(Filter.class, "Read ", readRows, " rows, written ", writtenRows, " rows");
		
		// closing the output
		h5.close();
		
		getProgress().setStatus(Status.DONE);
		save();
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getSourceChunk().isOutputGrouped();
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getSourceChunk().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
