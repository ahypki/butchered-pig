package butcheredpig.commands;

import static butcheredpig.Const.LIMIT;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;
import java.util.Iterator;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.TrickyString;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Limit extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String toLimit = null;
	
	@Expose
	@NotNull
	@AssertValid
	private long limit = 0;
	
	// cache
	private Chunk toLimitChunk = null;
	
	public Limit() {
		
	}
	
	public Limit(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = LIMIT %s %s", 
				getAliasNew(), 
				getToDump(),
				"" + getLimit());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		// e.g. limited1 = limit ordered 1;
		TrickyString ts = new TrickyString(toParse);

		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// DUMP keyword
		ts.expectedAlphanumericWord(LIMIT.toLowerCase());
		
		// source alias
		setToDump(ts.getNextAlphanumericWord());
		
		// sample rate
		setLimit(ts.getNextNumber());
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + LIMIT.toLowerCase() 
			+ REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + RegexUtils.INTEGER + REGEX_SPACE + ".*");
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		if (getToLimitChunk() != null
				&& getToLimitChunk().getProgress().isDone())
			return getToLimitChunk().getSourceApproxBytesSize();
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		// TODO read some lines and then compute, using limit, how much data it will take
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return iterateOutputRows(gv, splitNr, splitCount);
	}
	
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		if (getToLimitChunk() == null)
			return null;
		
		try {
			Iterator<Row> all = getToLimitChunk().iterateOutputRows(gv, splitNr, splitCount).iterator();
			
			return new Iterable<Row>() {
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						private long l = 0;
						
						@Override
						public Row next() {
							Row r = null;
							
							while (hasNext()) {
								r = all.next();
								if (l++ < getLimit())
									return r;
							}
							
							return r;
						}
						
						@Override
						public boolean hasNext() {
							return l < getLimit();
						}
					};
				}
			};
		} catch (Exception e) {
			LibsLogger.error(Limit.class, "Cannot iterate sample", e);
			return null;
		}
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (getToLimitChunk() == null || !getToLimitChunk().getProgress().isDone()) {
			LibsLogger.info(Limit.class, "Limit is not ready yet");
			return;
		}
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	private Chunk getToLimitChunk() throws IOException {
		if (toLimitChunk == null)
			toLimitChunk = getButcheredPigScript().getChunkByAlias(getToDump(), this.getPrevChunkId());
		return toLimitChunk;
	}

	// TODO name
	public String getToDump() {
		return toLimit;
	}

	public void setToDump(String toDump) {
		this.toLimit = toDump;
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getToLimitChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	public double getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}
	
	public void setLimit(String limit) {
		this.limit = NumberUtils.toLong(limit);
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return true;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getToLimitChunk().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
