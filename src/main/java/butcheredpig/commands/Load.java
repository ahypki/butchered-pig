package butcheredpig.commands;

import static butcheredpig.Const.LOAD;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import butcheredpig.Const;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.plugin.pig.BeansTable;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;

public class Load extends Chunk {
	
	@Expose
//	@NotEmpty
//	@NotNull
	private String datasetsQuery = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	private String tablesQuery = null;
	
	@Expose
	private String h5 = null;
	
	@Expose
	private String csv = null;
	
	public Load() {
		
	}
	
	public Load(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = LOAD datasets=%s tables=%s USING %s", 
				getAliasNew(), 
				getDatasetsQuery(),
				getTablesQuery(),
				isH5() ? "H5" : (isCSV() ? "CSV" : "??"));
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		ts.expectedPrintableCharacter("=");
		
		// LOAD
		ts.expectedAlphanumericWord(Const.LOAD.toLowerCase());
		
		// datasets
		ts.expectedPrintableCharacter("'");
		
		String loadQuery = ts.getNextStringUntil("'", false);
		ts.expectedPrintableCharacter("'");
		
		if (notEmpty(loadQuery)
				&& loadQuery.contains("datasets")) {
			setDatasetsQuery(RegexUtils.firstGroup("datasets[\\s]*=[\\s]*\"([^\\\"]*)\"", loadQuery, Pattern.CASE_INSENSITIVE));
			
			// tables
			setTablesQuery(RegexUtils.firstGroup("tables[\\s]*=[\\s]*\"([^\\\"]*)\"", loadQuery, Pattern.CASE_INSENSITIVE));
		}
		
		// TODO implement FILTER!
		
		// using
		ts.expectedAlphanumericWord("using");
		
		// which class use to read data
		String clazz = ts.getNextAlphanumericWord();
		if (notEmpty(clazz) && clazz.equalsIgnoreCase(BeansTable.class.getSimpleName())) {
			// do nothing
		} else if (notEmpty(clazz) && clazz.equalsIgnoreCase("h5")) {
			// reading h5 file
			setH5(loadQuery);
		} else if (notEmpty(clazz) && clazz.equalsIgnoreCase("csv")) {
			// reading h5 file
			setCsv(loadQuery);
		} else {
			throw new IOException("Cannot parse " + clazz);
		}
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + LOAD.toLowerCase() + REGEX_SPACE + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// load is done on-the-fly
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	public boolean isBeansTables() {
		return notEmpty(getDatasetsQuery()) || notEmpty(getTablesQuery());
	}
	
	public boolean isH5() {
		return StringUtilities.notEmpty(getH5());
	}
	
	public boolean isCSV() {
		return StringUtilities.notEmpty(getCsv());
	}

	public String getDatasetsQuery() {
		return datasetsQuery;
	}

	public void setDatasetsQuery(String datasetsQuery) {
		this.datasetsQuery = datasetsQuery;
	}

	public String getTablesQuery() {
		return tablesQuery;
	}

	public void setTablesQuery(String tablesQuery) {
		this.tablesQuery = tablesQuery;
	}

	public String getH5() {
		return h5;
	}

	public void setH5(String h5) {
		this.h5 = h5;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		if (isBeansTables()) {
			long bytes = 0;
			for (Table tab : TableFactory.iterateTables(getButcheredPigScript().getUserId(), getDatasetsQuery(), getTablesQuery()))
				for (Connector conn : tab.iterateConnectors())
					bytes += conn.getAproxBytesSize();
			return bytes;
		} else if (isH5() || isCSV()) {
			return getSourceConnector().getAproxBytesSize();
		} else {
			throw new NotImplementedException("LOAD getAproxBytesSize() not implemented");
		}
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	private ConnectorList getSourceConnector() throws IOException, ValidationException {
		if (isBeansTables()) {
			ConnectorList connList = new ConnectorList();
			for (Table tab : TableFactory.iterateTables(getButcheredPigScript().getUserId(), getDatasetsQuery(), getTablesQuery()))
				for (Connector conn : tab.iterateConnectors())
					connList.addConnector(conn);
			return connList;
		} else if (isH5())
			return new ConnectorList().addConnector(new H5Connector(UUID.random(), new File(getH5())));
		else if (isCSV())
			return new ConnectorList().addConnector(new PlainConnector(new File(getCsv())));
		else
			throw new NotImplementedException("LOAD class not implemented");
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return iterateOutputRows(gv, splitNr, splitCount);
	}

	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getSourceConnector().iterateRows(null, splitNr, splitCount);
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getSourceConnector().getColumnDefs();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	public String getCsv() {
		return csv;
	}

	public void setCsv(String csv) {
		this.csv = csv;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
