package butcheredpig.commands;

import static butcheredpig.Const.FOREACH;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

import com.google.gson.annotations.Expose;

import butcheredpig.UDFs;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.ChunkFactory;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.ButcheredRow;
import butcheredpig.commands.utils.GroupValue;
import butcheredpig.query.Field;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.plugin.pig.udfs.DsId;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ForeachNotNestedGrouped extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID sourceGroupId = null;
		
	@Expose
	@NotNull
	@AssertValid
	private List<Field> fields = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupSum = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupMin = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupMax = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupCount = null;
	
	@Expose
	@AssertValid
	private Boolean aggregate = null;
	
	// cache
	private Chunk sourceGroup = null;
	private int[] groupSumIdx = null;
	private int[] groupMinIdx = null;
	private int[] groupMaxIdx = null;
	private int[] groupCountIdx = null;
	private ColumnDefList outputColumnDefList = null;
	
	public ForeachNotNestedGrouped() {
		
	}
	
	@Override
	public String toStringScript() {
		StringBuilder sb = new StringBuilder();
		for (Field f : getFields())
			sb.append(f + ", ");
		return String.format("%s = FOREACH %s GENERATE %s", 
				getAliasNew(), 
				getSourceGroupId(),
				sb.toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		throw new NotImplementedException("Direct usage of this class is not implemented, use Foreach class");
	}
	
	public boolean isSourceGrouped() {
		return true;
	}
	
	public boolean isAggregate() {
		if (aggregate == null) {
			aggregate = false;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					aggregate = true;
					break;
				}
			}
		}
		return aggregate;
	}
	
	public boolean isFieldsList() {
		return this.fields != null && this.fields.size() > 0;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + FOREACH.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	private int[] getGroupSumIdx() {
		if (groupSumIdx == null) {
			List<Integer> idx = new ArrayList<>();
			int fieldIdx = 0;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					if (field.isSUM()) {
						idx.add(fieldIdx);
					}
				}
				fieldIdx++;
			}
			
			groupSumIdx = idx.stream().mapToInt(i->i).toArray();
		}
		return groupSumIdx;
	}
	
	private int[] getGroupMinIdx() {
		if (groupMinIdx == null) {
			List<Integer> idx = new ArrayList<>();
			int fieldIdx = 0;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					if (field.isMIN()) {
						idx.add(fieldIdx);
					}
				}
				fieldIdx++;
			}
			
			groupMinIdx = idx.stream().mapToInt(i->i).toArray();
		}
		return groupMinIdx;
	}
	
	private int[] getGroupMaxIdx() {
		if (groupMaxIdx == null) {
			List<Integer> idx = new ArrayList<>();
			int fieldIdx = 0;
			for (Field field : getFields()) {
				if (field.isAggregate()) {
					if (field.isMAX()) {
						idx.add(fieldIdx);
					}
				}
				fieldIdx++;
			}
			
			groupMaxIdx = idx.stream().mapToInt(i->i).toArray();
		}
		return groupMaxIdx;
	}
	
	@Override
	public void run() throws IOException, ValidationException {	
		if (!getAliasSourceChunk().isFinished())
			return;
		
		if (!getAliasSourceChunk().isDone()) {
			setProgress(getAliasSourceChunk().getProgress());
			throw new IOException("Source alias failed");
		}
		
		if (isFinished())
			return;
		
		// if foreach is done on grouped data it is not so easy
		if (isAggregate()) {
			int[] sumIdx = getGroupSumIdx();
			int[] minIdx = getGroupMinIdx();
			int[] maxIdx = getGroupMaxIdx();
			
			for (Row row : getAliasSourceChunk().iterateOutputRows(null, 0, 1)) {
				// compute SUMs (if any)
				if (sumIdx.length > 0) {
					for (int i = 0; i < sumIdx.length; i++) {
						Double sum = (Double) getGroupSum().get(sumIdx[i]);
						if (sum == null)
							sum = 0.0;
						String noSUM = getFields().get(sumIdx[i]).getExpression().toString().trim();
						noSUM = noSUM.substring("SUM(".length()); // TODO do it once only
						noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
						getGroupSum().put(sumIdx[i], row.compute(noSUM, null).doubleValue() + sum);
					}
				}
				
				// compute MIN (if any)
				if (minIdx.length > 0) {
					for (int i = 0; i < minIdx.length; i++) {
						Double min = (Double) getGroupMin().get(minIdx[i]);
						if (min == null)
							min = 0.0;
						String noSUM = getFields().get(minIdx[i]).getExpression().toString().trim();
						noSUM = noSUM.substring("MIN(".length()); // TODO do it once only
						noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
						getGroupMin().put(minIdx[i], Math.min(min, row.compute(noSUM, null).doubleValue()));
					}
				}
				
				// compute MAX (if any)
				if (maxIdx.length > 0) {
					for (int i = 0; i < maxIdx.length; i++) {
						Double max = (Double) getGroupMax().get(maxIdx[i]);
						if (max == null)
							max = 0.0;
						String noSUM = getFields().get(maxIdx[i]).getExpression().toString().trim();
						noSUM = noSUM.substring("MAX(".length()); // TODO do it once only
						noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
						getGroupMax().put(maxIdx[i], Math.max(max, row.compute(noSUM, null).doubleValue()));
					}
				}
			}
		}
		
		getProgress().setStatus(Status.DONE);
		save();	
	}
	
	public Iterator<Row> iteratorNonAggregate(GroupValue gv) {
		try {
//			try {
//				LibsLogger.info(Foreach.class, "" + getAliasSourceChunk().isOutputGrouped());
//				if (getAliasSourceChunk().isOutputGrouped()) {
//					return new Iterator<Row>() {
//						Iterator<GroupValue> gvIter = getAliasSourceChunk().iterateOutputGroupValue().iterator();
//						private Iterator<Row> allRows = null;
//						
//						@Override
//						public Row next() {
//							if ()
//							return null;
//						}
//						
//						@Override
//						public boolean hasNext() {
//							// TODO Auto-generated method stub
//							return false;
//						}
//					};
////					for (GroupValue gv : getAliasSourceChunk().iterateOutputGroupValue())
////						LibsLogger.info(Foreach.class, "" + gv);
//				}
//			} catch (Throwable t) {
//				System.out.println(t);
//			}
			
			return new Iterator<Row>() {
				// TODO implement isNestedForeach() ? getNestedScript().iter
				private Iterator<Row> allRows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
				
				@Override
				public Row next() {
					Row row = allRows.next();
					if (row == null)
						return null;
					
					Row newNew = new Row(row.getPk());
					
					for (Field field : getFields()) {
						if (field.isName()) {
							newNew.addColumn(field.getNewName(), 
									row.get(field.getName()));
						} else if (field.isExpression()) {
							// TODO DEBUG
							if (field.getExpression().getMathExpr().toLowerCase().startsWith("dsid(")) {
								try {
									Tuple t = TupleFactory.getInstance().newTuple(1);
									t.set(0, row.get("tbid"));
									newNew.addColumn(field.getColumnList().get(0).getName(),
											new DsId().exec(t));
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else
								newNew.addColumn(field.getColumnList().get(0).getName(), 
										row.compute(field.getExpression().toString(), null));
						} else {
							throw new NotImplementedException("not implemented");
						}
					}
					return newNew;
				}
				
				@Override
				public boolean hasNext() {
					return allRows.hasNext();
				}
			};
		} catch (IOException | ValidationException e) {
			LibsLogger.error(ForeachNotNestedGrouped.class, "Cannot open iterator for " + getSourceGroupId(), e);
			return null;
		}
	}

	public Iterable<Row> iterator(GroupValue gv) throws IOException, ValidationException {
//		if (isAggregate()) {
		final ColumnDefList cdf = getOutputColumnDefList();
		
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						private Row currentRow = null;
						private Iterator<Row> groupValueRows = null;
						
						private void init() {
							try {
								if (groupValueRows == null)
									groupValueRows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
							} catch (IOException | ValidationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
						@Override
						public Row next() {
							if (groupValueRows == null)
								init();
							return currentRow;
						}
						
						@Override
						public boolean hasNext() {
							if (groupValueRows == null)
								init();
							
							Row oldRow = groupValueRows.hasNext() ? groupValueRows.next() : null;
							
							if (oldRow == null)
								return false;
							
							currentRow = new ButcheredRow(UUID.random(), null);
							int fieldIdx = 0;
							for (Field field : getFields()) {
								if (field.isName()
										&& field.getName().equals("group"))
									currentRow.addColumn(field.getColumnList().get(0).getName(), 
											gv.getValues().get(0)); // TODO implement multi values
								else if (field.isExpression()
										&& field.getExpression().toString().equals("group"))
									currentRow.addColumn(field.getColumnList().get(0).getName(), 
											gv.getValues().get(0)); // TODO implement multi values
								else if (field.isExpression()
										&& field.getExpression().getMathExpr().toLowerCase().startsWith("dsid(")) {
									try {
										Tuple t = TupleFactory.getInstance().newTuple(1);
										t.set(0, currentRow.get("tbid"));
										currentRow.addColumn(field.getColumnList().get(0).getName(),
												new DsId().exec(t));
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else if (field.isSUM()) {
//								String noSUM = field.getExpression().toString().trim();
//								noSUM = noSUM.substring("SUM(".length()); // TODO do it once only
//								noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
									currentRow.addColumn(field.getColumnList().get(0).getName(), 
											getGroupSum().get(fieldIdx)); // TODO implement multi values
								} else if (field.isMIN()) {
									currentRow.addColumn(field.getColumnList().get(0).getName(), 
											getGroupMin().get(fieldIdx)); // TODO implement multi values
								} else if (field.isMAX()) {
									currentRow.addColumn(field.getColumnList().get(0).getName(), 
											getGroupMax().get(fieldIdx)); // TODO implement multi values
								} else if (field.isFlatten()) {	
									if (field.getColumnList().size() == 0)
										// no need to renaming the fields
										currentRow.addColumns(oldRow.getColumns());
									else {
										// renaming
										try {
											ColumnDefList oldRowColDefs = ForeachNotNestedGrouped.this.getAliasSourceChunk().getOutputColumnDefList();
//											FieldFlatten ff = field.asFieldFlatten(ForeachNotNestedGrouped.this.getAliasSourceChunk());
											for (int i = 0; i < oldRowColDefs.size(); i++) {
												currentRow.addColumn(field.getColumnList().get(i).getName(), 
														oldRow.get(oldRowColDefs.get(i).getName()));
											}
										} catch (IOException | ValidationException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								} else if (field.isName()) {
									currentRow.addColumn(field.getName(), 
											oldRow.get(field.getName()));
								} else
									throw new NotImplementedException("Unimplemented case for field " + field);
								fieldIdx++;
							}
							
							return true;
						}
					};
			}
		};
		
//			return ;
//		} else {
//			try {
//				if (getAliasSourceChunk().isOutputGrouped()) {
//					return new Iterator<Row>() {
//						private Iterator<GroupValue> gvIter = null;
//						private GroupValue gv = null;
//						private Iterator<Row> rows = null;
//						
//						@Override
//						public Row next() {
//							if (!hasNext())
//								return null;
//							
//							if (rows.hasNext())
//								return rows.next();
//							
//							rows = null;
//							if (!hasNext())
//								return null;
//							
//							return null;
//						}
//						
//						@Override
//						public boolean hasNext() {
//							if (gvIter == null) {
//								try {
//									gvIter = getAliasSourceChunk().iterateOutputGroupValue().iterator();
//								} catch (IOException | ValidationException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}
//							
//							if (rows == null) {
//								gv = null;
//								if (gvIter.hasNext())
//									gv = gvIter.next();
//								if (gv == null)
//									return false;
//								
//								try {
//									rows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
//								} catch (IOException | ValidationException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}
//							
//							return rows.hasNext();
//						}
//					};
//				}
//			} catch (IOException | ValidationException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			return iteratorNonAggregate(null);
//		}
	}

	public List<Field> getFields() {
		if (fields == null)
			fields = new ArrayList<>();
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getAliasSourceChunk() != null ? getAliasSourceChunk().getSourceApproxBytesSize() : -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
//		if (splitNr == 0)
//			return new Iterable<Row>() {
//				@Override
//				public Iterator<Row> iterator() {
					return ForeachNotNestedGrouped.this.iterator(gv);
//				}
//			};
//		else 
//			return new EmptyRowIterator();
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getAliasSourceChunk().getSourceColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		if (outputColumnDefList == null) {
			
			ColumnDefList cdf = new ColumnDefList();
			for (Field f : getFields()) {
				if (f.isFlatten()) {
					for (ColumnDef col : f.getColumnList()) {
						if (col.getType() == ColumnType.UNKNOWN) {
							ColumnDef colDefFromSource = getAliasSourceChunk()
															.getOutputColumnDefList()
															.getColumn(col.getName());
							if (colDefFromSource != null) {
								// getting type from the columns list from the source alias
								col.setType(colDefFromSource.getType());
								continue;
							}
							
							String fName = f.getName() != null ? f.getName() : f.getExpression().toString();
							colDefFromSource = getAliasSourceChunk()
									.getOutputColumnDefList()
									.getColumn(fName.replace("::", "."));
							if (colDefFromSource != null) {
								// getting type from the columns list from the source alias
								col.setType(colDefFromSource.getType());
								continue;
							} else if (f.isFunction()) {
								try {
									String funcName = f.getFunctionName();
									if (funcName.equals("MAX"))
										col.setType(ColumnType.DOUBLE);
									else if (funcName.equals("MIN"))
										col.setType(ColumnType.DOUBLE);
									else if (funcName.equals("SUBSTRING"))
										col.setType(ColumnType.STRING);
									else
										col.setType(ColumnType.fromClass(UDFs.getEvalFunc(funcName).getReturnType()));
								} catch (Throwable e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} 
							
							if (col.getType() == ColumnType.UNKNOWN)
								col.setType(ColumnType.DOUBLE);
								// TODO implement it better
//									throw new NotImplementedException("Cannot determine type");
						}
						
						cdf.add(col);
					}
				} else {
					ColumnDef col = new ColumnDef(f.getNewName(), null, ColumnType.UNKNOWN);
					ColumnDef colDefFromSource = getAliasSourceChunk().getOutputColumnDefList().getColumn(f.getName());
					if (colDefFromSource != null)
						// getting type from the columns list from the source alias
						col.setType(colDefFromSource.getType());
					else if (f.isFunction()) {
						col.setType(ColumnType.fromClass(UDFs.getEvalFunc(f.getFunctionName()).getReturnType()));//UDFs.pigTypeToColumnType());
					} else {
						col.setType(ColumnType.fromClass(f.determineType()));
					}
					
					if (col.getType() == ColumnType.UNKNOWN)
						throw new NotImplementedException("Cannot determine type");
					
					cdf.addColumnDef(col);
				}
			}
			outputColumnDefList = cdf;
//			return getAliasSourceChunk().getSourceColumnDefList();
			
		}
		return outputColumnDefList;
	}

	private Chunk getAliasSourceChunk() throws IOException {
		if (sourceGroup == null)
			sourceGroup = ChunkFactory.getChunk(getSourceGroupId());
		return sourceGroup;
	}

	public void setSourceGroup(Chunk aliasSourceChunk) {
		this.sourceGroupId = aliasSourceChunk != null ? aliasSourceChunk.getId() : null;
		this.sourceGroup = aliasSourceChunk;
	}

	private Map<Integer, Object> getGroupSum() {
		if (groupSum == null)
			groupSum = new HashMap<>();
		return groupSum;
	}

	private void setGroupSum(Map<Integer, Object> groupSum) {
		this.groupSum = groupSum;
	}

	private Map<Integer, Object> getGroupCount() {
		if (groupCount == null)
			groupCount = new HashMap<>();
		return groupCount;
	}

	private void setGroupCount(Map<Integer, Object> groupCount) {
		this.groupCount = groupCount;
	}

	private Map<Integer, Object> getGroupMin() {
		if (groupMin == null)
			groupMin = new HashMap<>();
		return groupMin;
	}

	private void setGroupMin(Map<Integer, Object> groupMin) {
		this.groupMin = groupMin;
	}

	private Map<Integer, Object> getGroupMax() {
		if (groupMax == null)
			groupMax = new HashMap<>();
		return groupMax;
	}

	private void setGroupMax(Map<Integer, Object> groupMax) {
		this.groupMax = groupMax;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return true;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputGroupValue();
	}

	public UUID getSourceGroupId() {
		return sourceGroupId;
	}

	public void setSourceGroupId(UUID sourceGroupId) {
		this.sourceGroupId = sourceGroupId;
	}

	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
