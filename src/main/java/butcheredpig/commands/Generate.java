package butcheredpig.commands;

import static butcheredpig.Const.GENERATE;
import static butcheredpig.Const.REGEX_SPACE;
import static butcheredpig.chunks.Status.DONE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.IterateAllGroups;
import butcheredpig.commands.utils.GroupValue;
import butcheredpig.query.Field;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.plugin.pig.udfs.DsId;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Generate extends Chunk implements Iterable<Row> {
	
//	@Expose
//	@NotNull
//	@AssertValid
//	private UUID foreachId = null;
		
	@Expose
	@NotNull
	@AssertValid
	private List<Field> fields = null; // TODO where is it used?
		
	public Generate() {
		
	}
	
	@Override
	public String toStringScript() {
		StringBuilder sb = new StringBuilder();
		for (Field f : getFields())
			sb.append(f + ", ");
		return String.format("GENERATE %s",
				sb.toString());
	}
	
//	@Override
//	public void additionalValidation() throws ValidationException {
//		super.additionalValidation();
//		
//		if (getAliasNew() == null) {
//			// if new alias is not defined then foreachId has to be set
//			assertTrue(FOREACH + " is not defined in " + GENERATE + " which is a nested one", 
//					getForeachId() != null);
//		}
//	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		try {
			TrickyString ts = new TrickyString(toParse);
			
			// GENERATE
			ts.expectedAlphanumericWord(GENERATE.toLowerCase());
			
			// fields
			EscapeString es = new EscapeString(ts.checkLine());
			es.escapeAll("(\\([^\\)\\(]*\\))");
			
			// list of fields
			TrickyString tsFields = new TrickyString(es.getEscapedText());
			while (!tsFields.isEmpty()) {
				String field = tsFields.getNextStringUntil(",", false);
				
				if (field == null)
					field = tsFields.getLine();
							
				getFields().add(new Field(es.deescapeString(field)));
				
				if (tsFields.length() > 0)
					tsFields.expectedPrintableCharacter(",");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this;
	}
	
	public boolean isFieldsList() {
		return this.fields != null && this.fields.size() > 0;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_SPACE + GENERATE.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (!getButcheredPigScript().getChunkPrev(getId()).isFinished()) {
			LibsLogger.info(Generate.class, "Source for " + this + " not ready yet");
			return;
		}
		
		if (!getButcheredPigScript().getChunkPrev(getId()).isDone()) {
			LibsLogger.info(Generate.class, "Source for " + this + " is not " + DONE + ", finishing");
			fail("Source for " + this + " is not " + DONE + ", finishing");
			return;
		}
		
		// TODO it is not implemented, right?
//		for (Row row : iterateOutputRows(0, 1)) {
//			if (row == null)
//				continue;
//			
//			Row newNew = new Row(row.getPk());
//			
//			for (Field field : getFields()) {
//				if (field.isName()) {
//					newNew.addColumn(field.getName(), row.get(field.getName()));
//				} else {
//					throw new NotImplementedException("not implemented");
//				}
//			}
//		}
		
		getProgress().done("done");
		save();
	}
	
	public boolean isInNestedForeach() {
		return getAliasNew() == null;
	}
	
	@Override
	public Iterator<Row> iterator() {
		return null;
	}

	public List<Field> getFields() {
		if (fields == null)
			fields = new ArrayList<>();
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getButcheredPigScript().getChunkPrev(getId()).getSourceApproxBytesSize();
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getButcheredPigScript().getChunkPrev(getId()).iterateSourceRows(gv, splitNr, splitCount);
	}
	
	public Iterator<Row> iteratorNonAggregate(GroupValue gv) {
		try {
			Chunk ch = getButcheredPigScript().getChunkPrev(getId());
			
			return new Iterator<Row>() {
				// TODO implement isNestedForeach() ? getNestedScript().iter
				private Iterator<Row> allRows = ch.iterateOutputRows(gv, 0, 1).iterator();
				
				@Override
				public Row next() {
					if (!hasNext())
						return null;
					
					Row row = allRows.next();
					if (row == null)
						return null;
					Row newNew = new Row(row.getPk());
					for (Field field : getFields()) {
						if (field.isName()) {
							newNew.addColumn(field.getName(), 
									row.get(field.getName()));
						} else if (field.getExpression().toString().toLowerCase().startsWith("flatten(")) {
							newNew.addColumns(row.getColumns());
						} else if (field.isExpression()) {
							// TODO DEBUG
							if (field.getExpression().getMathExpr().toLowerCase().startsWith("dsid(")) {
								try {
									Tuple t = TupleFactory.getInstance().newTuple(1);
									t.set(0, row.get("tbid"));
									newNew.addColumn(field.getColumnList().get(0).getName(),
											new DsId().exec(t));
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else
								newNew.addColumn(field.getColumnList().get(0).getName(), 
										row.compute(field.getExpression().toString(), null));
						} else {
							throw new NotImplementedException("not implemented");
						}
					}
					return newNew;
				}
				
				@Override
				public boolean hasNext() {
					return allRows.hasNext();
				}
			};
		} catch (IOException | ValidationException e) {
			LibsLogger.error(Foreach.class, "Cannot open iterator for Generate command", e);
			return null;
		}
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		if (gv == null)
			return new IterateAllGroups(this);
		
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return iteratorNonAggregate(gv);
			}
		};
		
//		Chunk ch = getButcheredPigScript().getChunkPrev(getId());
//		
//		// TODO it is identical to Foreach
//		return new Iterable<Row>() {
//			
//			@Override
//			public Iterator<Row> iterator() {
//				return new Iterator<Row>() {
//					private Row currentRow = null;
//					private Iterator<GroupValue> groupValiter = null;
//					private GroupValue currentGroupValue = null;
//					private Iterator<Row> groupValueRows = null;
//					
//					@Override
//					public Row next() {
//						if (groupValiter == null)
//							hasNext();
//						return currentRow;
//					}
//					
//					@Override
//					public boolean hasNext() {
//						if (groupValueRows != null) {
//							if (groupValueRows.hasNext()) {
//								currentRow.addColumns(groupValueRows.next().getColumns());
//								return true;
//							} else {
//								groupValueRows = null; // no more values for flatten
//							}
//						}
//						
//						if (groupValiter == null) {
////							try {
//								// TODO parallel
//								groupValiter = ((Group) ch).iterateGroupValues().iterator();
////							} catch (IOException e) {
////								throw new RuntimeException("Canot iterate over group values");
////							}
//						}
//						
//						currentGroupValue = groupValiter.hasNext() ? groupValiter.next() : null;
//						
//						if (currentGroupValue == null)
//							return false;
//						
//						// TODO implement FLATTEN
//						
//						currentRow = new ButcheredRow(UUID.random());
//						int fieldIdx = 0;
//						for (Field field : getFields()) {
//							if (field.isName()
//									&& field.getName().equals("group"))
//								currentRow.addColumn(field.getColumnList().get(0).getName(), 
//										currentGroupValue.getValues().get(0)); // TODO implement multi values
//							else if (field.isExpression()
//									&& field.getExpression().toString().equals("group")) {
//								currentRow.addColumn(field.getColumnList().get(0).getName(), 
//										currentGroupValue.getValues().get(0)); // TODO implement multi values
////							else if (field.isSUM()) {
////								currentRow.addColumn(field.getColumnList().get(0).getName(), 
////										getGroupSum().get(fieldIdx)); // TODO implement multi values
////							} else if (field.isMIN()) {
////								currentRow.addColumn(field.getColumnList().get(0).getName(), 
////										getGroupMin().get(fieldIdx)); // TODO implement multi values
////							} else if (field.isMAX()) {
////								currentRow.addColumn(field.getColumnList().get(0).getName(), 
////										getGroupMax().get(fieldIdx)); // TODO implement multi values
//							} else if (field.isFlatten()) {
////								try {
//									groupValueRows = ((Group) ch).iterateRows(currentGroupValue).iterator();
//									
//									if (groupValueRows.hasNext()) {
//										// TODO it will get only one results from thish groupValueRows? What about the rest of the rows?
//										currentRow.addColumns(groupValueRows.next().getColumns());
//										return true;
//									}
////								} catch (IOException e) {
//									// TODO Auto-generated catch block
////									e.printStackTrace();
////								}
//								
//							} else
//								throw new NotImplementedException("Unimplemented case for field " + field);
//							fieldIdx++;
//						}
//						
//						return true;
//					}
//				};
//			}
//		};
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getButcheredPigScript().getChunkPrev(getId()).getSourceColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
//		return getSourceColumnDefList();
		ColumnDefList cdf = new ColumnDefList();
		for (Field f : getFields()) {
			if (f.isFlatten()) {
				for (ColumnDef col : f.getColumnList())
					cdf.add(col);
			} else {
				cdf.addColumnDef(new ColumnDef(f.getName(), null, ColumnType.UNKNOWN)); // TODO implement it
			}
		}
		return cdf;
	}

//	public UUID getForeachId() {
//		return foreachId;
//	}
//
//	public void setForeachId(UUID foreachId) {
//		this.foreachId = foreachId;
//	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return true;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getButcheredPigScript().getChunkPrev(getId()).iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
