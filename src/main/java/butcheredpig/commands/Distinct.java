package butcheredpig.commands;

import static butcheredpig.Const.DISTINCT;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.sha.SHAManager;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Distinct extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String aliasSource = null;
	
	private List<Chunk> distinctChunks = null;
	
	private Chunk aliasSourceChunk = null;
	
	public Distinct() {
		
	}
	
	public Distinct(String toParse, ButcheredPigScript butcheredPigScript, Chunk prevChunk) throws IOException, ValidationException {
		super(butcheredPigScript, prevChunk);
		parse(null, toParse);
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// DISTINCT
		ts.expectedAlphanumericWord(DISTINCT.toLowerCase());
		
		// source alias
		setAliasSource(ts.getNextAlphanumericWord());
		
		return this;
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = DISTINCT %s", 
				getAliasNew(), 
				getAliasSource());
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().trim();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + DISTINCT.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// check if source alias is ready
		if (getAliasSourceChunk() == null) {
			LibsLogger.error(Distinct.class, "Source chunk is not defined, something is wrong, failing " + getClass().getSimpleName());
			getProgress().setStatus(Status.FAILED);
			save();
			return;
		} else if (getAliasSourceChunk().getProgress().getStatus() == Status.FAILED) {
			LibsLogger.error(Distinct.class, "Source alias failed, failing " + getClass().getSimpleName());
			getProgress().setStatus(Status.FAILED);
			save();
			return;
		} else if (getAliasSourceChunk().getProgress().getStatus() == Status.NEW
				|| getAliasSourceChunk().getProgress().getStatus() == Status.RUNNING) {
			LibsLogger.info(Distinct.class, "Source alias not ready yet, waiting...");
			return;
		} else if (getAliasSourceChunk().getProgress().getStatus() == Status.DONE) {
			// source alias is ready, go further
		} else {
			LibsLogger.error(Distinct.class, "Source alias has not implemented status: " + getAliasSourceChunk().getProgress());
			getProgress().setStatus(Status.FAILED);
			save();
			return;
		}
		
		// Plan for parallel distinct
		// 1. create Order chunk
		// 2. chunks will work on Connectors which on-the-fly add column _sha
		// 3. sorting over _sha column
		// 4. on-the-fly distinct filtering based on _sha column
		
		// if the source alias is DONE then this distinct is DONE too
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputRows(gv, splitNr, splitCount);
	}
	
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					
					private long all = 0;
					private long distinct = 0;
					
					private Iterator<Row> allRows = null;
					
					private Set<String> uniqSHA = new HashSet<>();
					
					private Row rowNext = null;
					
					@Override
					public Row next() {
						return rowNext;
					}
					
					@Override
					public boolean hasNext() {
						if (allRows == null) {
							try {
								allRows = getAliasSourceChunk().iterateOutputRows(gv, splitNr, splitCount).iterator();
							} catch (Throwable e) {
								LibsLogger.error(Distinct.class, "Cannot open source alias iterator", e);
								return false;
							}
						}
						
						while (allRows.hasNext()) {
							Row tmpRow = allRows.next();
							all++;
							try {
								String sha = SHAManager.getSHA128(tmpRow.toJsonColumns());
								
								if (uniqSHA.add(sha)) {
									rowNext = tmpRow;
									distinct++;
									return true;
								}
							} catch (IOException e) {
								LibsLogger.error(Distinct.class, "Cannot compute SHA for the row " + tmpRow, e);
							}
						}
						
						LibsLogger.info(Distinct.class, "All rows ", all, ", distinct ", distinct);
						return false;
					}
				};
			}
		};
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		if (getAliasSourceChunk() != null
				&& getAliasSourceChunk().getProgress().isDone())
			return getAliasSourceChunk().getSourceApproxBytesSize();
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}

	public String getAliasSource() {
		return aliasSource;
	}

	public void setAliasSource(String aliasSource) {
		this.aliasSource = aliasSource;
	}

	private Chunk getAliasSourceChunk() throws IOException {
		if (aliasSourceChunk == null)
			aliasSourceChunk = getButcheredPigScript().getChunkByAlias(getAliasSource(), this.getPrevChunkId());
		return aliasSourceChunk;
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getAliasSourceChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	private void setAliasSourceChunk(Chunk aliasSourceChunk) {
		this.aliasSourceChunk = aliasSourceChunk;
	}

	private List<Chunk> getDistinctChunks() {
		if (distinctChunks == null)
			distinctChunks = new ArrayList<>();
		return distinctChunks;
	}

	private void setDistinctChunks(List<Chunk> distinctChunks) {
		this.distinctChunks = distinctChunks;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getAliasSourceChunk().isOutputGrouped();
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getAliasSourceChunk().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
