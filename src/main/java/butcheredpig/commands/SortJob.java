package butcheredpig.commands;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.ChunkFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class SortJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID sortId = null;

	public SortJob() {
		
	}
	
	public SortJob(UUID sortId) {
		setSortId(sortId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.debug(SortJob.class, "Sorting ", getSortId());
		
		Chunk sort = ChunkFactory.getChunk(getSortId());
		
		if (sort == null)
			return;
			
		try {
			sort.run();
		} catch (Throwable t) {
			LibsLogger.error(SortJob.class, "Cannot sort chunk, failing job", t);
			
			sort.getProgress().fail("Cannot sort chunk: " + (t != null && t.getMessage() != null ? t.getMessage() : ""));
			sort.save();
		}
	}

	public UUID getSortId() {
		return sortId;
	}

	public void setSortId(UUID sortId) {
		this.sortId = sortId;
	}
}
