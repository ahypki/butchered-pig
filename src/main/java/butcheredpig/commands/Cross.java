package butcheredpig.commands;

import static butcheredpig.Const.CROSS;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.IterateCross;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Cross extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private List<String> toCross = null;
	
	private ColumnDefList outputColumnDefList = null;
	
	public Cross() {
		
	}
	
	public Cross(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = %s %s", 
				getAliasNew(), 
				CROSS.toString(),
				StringUtilities.join(getToCross(), ", "));
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// CROSS
		ts.expectedAlphanumericWord(CROSS.toLowerCase());
		
		// parsing aliases to join
		while (ts.length() > 0) {
			String aliasToToin = ts.getNextAlphanumericWordWithUnderscore();
			
			getToCross().add(aliasToToin);
			
			if (ts.checkNextPrintableCharacter() != null && ts.checkNextPrintableCharacter().equals(","))
				ts.expectedPrintableCharacter(",");
		}
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().trim();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + CROSS.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// nothing to do actually, output rows are computed on-the-fly
		getProgress().setStatus(Status.DONE);
		save();
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		long bytes = 0;
		for (String c : getToCross())
			bytes += getButcheredPigScript().getChunkByAlias(c, this.getPrevChunkId()).getSourceApproxBytesSize();
		return bytes;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		long bytes = 0;
		for (String c : getToCross())
			bytes *= getButcheredPigScript().getChunkByAlias(c, this.getPrevChunkId()).getSourceApproxBytesSize();
		return bytes;
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		assertTrue(gv == null, CROSS.toString() + " is not implemented for groups, no " + GroupValue.class.getSimpleName() 
				+ " was expected");
		List<Chunk> chunks = new ArrayList<>();
		for (String chStr : getToCross())
			chunks.add(getButcheredPigScript().getChunkByAlias(chStr, this.getPrevChunkId()));
		return new IterateCross(splitNr, splitCount, chunks);
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return new ColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		if (outputColumnDefList == null) {
			outputColumnDefList = new ColumnDefList();
	
			for (int i = 0; i < getToCross().size(); i++) {
				String c = getToCross().get(i);
				Chunk chunkToJoin = getButcheredPigScript().getChunkByAlias(c, this.getPrevChunkId());
				
				for (ColumnDef col : chunkToJoin.getOutputColumnDefList())
					outputColumnDefList.add(new ColumnDef(c + "::" + col.getName(), null, col.getType()));
			}
		}
		
		return outputColumnDefList;
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}

	public List<String> getToCross() {
		if (toCross == null)
			toCross = new ArrayList<>();
		return toCross;
	}

	public void setToCross(List<String> toCross) {
		this.toCross = toCross;
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
