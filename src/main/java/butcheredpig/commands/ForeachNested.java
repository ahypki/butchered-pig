package butcheredpig.commands;

import static butcheredpig.Const.FOREACH;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static butcheredpig.Settings.isLogDebug;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.ChunkFactory;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ForeachNested extends Chunk {
		
	@Expose
	@NotNull
	@AssertValid
	private UUID groupSourceId = null;
	
	@Expose
	@NotNull
	@AssertValid
	private List<UUID> nestedChunkIds = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupSum = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupMin = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupMax = null;
	
	@Expose
	@AssertValid
	private Map<Integer, Object> groupCount = null;
	
	// cache
	private Chunk aliasSourceChunk = null;
	private Group groupSourceCache = null;
	private ColumnDefList outputColumnDefList = null;
	
	public ForeachNested() {
		
	}
	
	@Override
	public String toStringScript() {
		String source = "";
		try {
			source = getAliasSourceChunk().toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		return String.format("%s = FOREACH %s GENERATE %s", 
				getAliasNew(), 
				source,
				sb.toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		throw new NotImplementedException("Direct usage of this class is not implemented, use Foreach class");
	}
		
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + FOREACH.toLowerCase() + REGEX_SPACE + ".*");
	}
	
	@Override
	public void run() throws IOException, ValidationException {		
		if (!getGroupSourceCache().isFinished())
			return;
		
		if (!getGroupSourceCache().isDone()) {
			getProgress().fail("Source group failed");
			return;
		}
		
		if (isFinished())
			return;
		
		if (isLogDebug()) {
			for (GroupValue gv : getSourceGroup().iterateOutputGroupValue())
				if (gv != null)
					LibsLogger.info(ForeachNested.class, getAliasNew(), " ", getClass().getSimpleName(), 
							" group ", gv.toString().replace('\n', ' '));
		}
		
		Chunk lastChunk = getNestedChunkIds().size() > 0 ? 
				ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1)):
				null;
		if (lastChunk instanceof Generate) {
			if (lastChunk.isDone()) {
				done(null);
				save();
			} else if (lastChunk.getProgress().isFailed()) {
				fail(lastChunk.getProgress().getMessage());
				save();
			}
		}
	}
	
	
//	public Iterator<Row> iteratorNonAggregate(GroupValue gv) {
//		try {
////			try {
////				LibsLogger.info(Foreach.class, "" + getAliasSourceChunk().isOutputGrouped());
////				if (getAliasSourceChunk().isOutputGrouped()) {
////					return new Iterator<Row>() {
////						Iterator<GroupValue> gvIter = getAliasSourceChunk().iterateOutputGroupValue().iterator();
////						private Iterator<Row> allRows = null;
////						
////						@Override
////						public Row next() {
////							if ()
////							return null;
////						}
////						
////						@Override
////						public boolean hasNext() {
////							// TODO Auto-generated method stub
////							return false;
////						}
////					};
//////					for (GroupValue gv : getAliasSourceChunk().iterateOutputGroupValue())
//////						LibsLogger.info(Foreach.class, "" + gv);
////				}
////			} catch (Throwable t) {
////				System.out.println(t);
////			}
//			
//			return new Iterator<Row>() {
//				// TODO implement isNestedForeach() ? getNestedScript().iter
//				private Iterator<Row> allRows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
//				
//				@Override
//				public Row next() {
//					Row row = allRows.next();
//					if (row == null)
//						return null;
//					
//					Row newNew = new Row(row.getPk());
//					
//					for (Field field : getFields()) {
//						if (field.isName()) {
//							newNew.addColumn(field.getNewName(), 
//									row.get(field.getName()));
//						} else if (field.isExpression()) {
//							// TODO DEBUG
//							if (field.getExpression().getMathExpr().toLowerCase().startsWith("dsid(")) {
//								try {
//									Tuple t = TupleFactory.getInstance().newTuple(1);
//									t.set(0, row.get("tbid"));
//									newNew.addColumn(field.getColumnList().get(0).getName(),
//											new DsId().exec(t));
//								} catch (IOException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							} else
//								newNew.addColumn(field.getColumnList().get(0).getName(), 
//										row.compute(field.getExpression().toString(), null));
//						} else {
//							throw new NotImplementedException("not implemented");
//						}
//					}
//					return newNew;
//				}
//				
//				@Override
//				public boolean hasNext() {
//					return allRows.hasNext();
//				}
//			};
//		} catch (IOException | ValidationException e) {
//			LibsLogger.error(ForeachNested.class, "Cannot open iterator for " + getAliasNew(), e);
//			return null;
//		}
//	}

//	public Iterator<Row> iterator() {
//		if (isAggregate()) {
//			return new Iterator<Row>() {
//				private Row currentRow = null;
//				private Iterator<GroupValue> groupValiter = null;
//				private GroupValue currentGroupValue = null;
//				private Iterator<Row> groupValueRows = null;
//				
//				@Override
//				public Row next() {
//					if (groupValiter == null)
//						hasNext();
//					return currentRow;
//				}
//				
//				@Override
//				public boolean hasNext() {
//					if (groupValueRows != null) {
//						if (groupValueRows.hasNext()) {
//							currentRow.addColumns(groupValueRows.next().getColumns());
//							return true;
//						} else {
//							groupValueRows = null; // no more values for flatten
//						}
//					}
//					
//					if (groupValiter == null) {
//						try {
//							// TODO parallel
//							groupValiter = ((Group) ForeachNested.this.getAliasSourceChunk()).iterateOutputGroupValue().iterator();
//						} catch (IOException | ValidationException e) {
//							throw new RuntimeException("Canot iterate over group values");
//						}
//					}
//					
//					currentGroupValue = groupValiter.hasNext() ? groupValiter.next() : null;
//					
//					if (currentGroupValue == null)
//						return false;
//					
//					// TODO implement FLATTEN
//					
//					currentRow = new ButcheredRow(UUID.random());
//					int fieldIdx = 0;
//					for (Field field : getFields()) {
//						if (field.isName()
//								&& field.getName().equals("group"))
//							currentRow.addColumn(field.getColumnList().get(0).getName(), 
//									currentGroupValue.getValues().get(0)); // TODO implement multi values
//						else if (field.isExpression()
//								&& field.getExpression().toString().equals("group"))
//							currentRow.addColumn(field.getColumnList().get(0).getName(), 
//									currentGroupValue.getValues().get(0)); // TODO implement multi values
//						else if (field.isSUM()) {
////							String noSUM = field.getExpression().toString().trim();
////							noSUM = noSUM.substring("SUM(".length()); // TODO do it once only
////							noSUM = noSUM.substring(0, noSUM.lastIndexOf(')'));
//							currentRow.addColumn(field.getColumnList().get(0).getName(), 
//									getGroupSum().get(fieldIdx)); // TODO implement multi values
//						} else if (field.isMIN()) {
//							currentRow.addColumn(field.getColumnList().get(0).getName(), 
//									getGroupMin().get(fieldIdx)); // TODO implement multi values
//						} else if (field.isMAX()) {
//							currentRow.addColumn(field.getColumnList().get(0).getName(), 
//									getGroupMax().get(fieldIdx)); // TODO implement multi values
//						} else if (field.isFlatten()) {
//							try {
//								groupValueRows = ((Group) ForeachNested.this.getAliasSourceChunk()).iterateRows(currentGroupValue).iterator();
//								
//								if (groupValueRows.hasNext()) {
//									// TODO it will get only one results from thish groupValueRows? What about the rest of the rows?
//									currentRow.addColumns(groupValueRows.next().getColumns());
//									return true;
//								}
//							} catch (IOException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//							
//						} else
//							throw new NotImplementedException("Unimplemented case for field " + field);
//						fieldIdx++;
//					}
//					
//					return true;
//				}
//			};
//		} else {
//			try {
//				if (getAliasSourceChunk().isOutputGrouped()) {
//					return new Iterator<Row>() {
//						private Iterator<GroupValue> gvIter = null;
//						private GroupValue gv = null;
//						private Iterator<Row> rows = null;
//						
//						@Override
//						public Row next() {
//							if (!hasNext())
//								return null;
//							
//							if (rows.hasNext())
//								return rows.next();
//							
//							rows = null;
//							if (!hasNext())
//								return null;
//							
//							return null;
//						}
//						
//						@Override
//						public boolean hasNext() {
//							if (gvIter == null) {
//								try {
//									gvIter = getAliasSourceChunk().iterateOutputGroupValue().iterator();
//								} catch (IOException | ValidationException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}
//							
//							if (rows == null) {
//								gv = gvIter.next();
//								if (gv == null)
//									return false;
//								
//								try {
//									rows = getAliasSourceChunk().iterateOutputRows(gv, 0, 1).iterator();
//								} catch (IOException | ValidationException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}
//							
//							return rows.hasNext();
//						}
//					};
//				}
//			} catch (IOException | ValidationException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			return iteratorNonAggregate(null);
//		}
//	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1)).getSourceApproxBytesSize();
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
//		return getSourceConnector().iterateRows(null, splitNr, splitCount);
		
			// it is nested foreach, getting the last Chunk, it should be generate chunk
			Chunk lastChunk = ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1));
			if (lastChunk instanceof Generate) {
				return lastChunk.iterateOutputRows(gv, splitNr, splitCount);
			} else {
				throw new ValidationException("Unimplemented case for FOREACH " + toString() + ", it is nested FOREACH, but "
						+ "the last chunk in the nested list is not GENERATE");
			}
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		// it is nested foreach, getting the last Chunk, it should be GENERATE chunk
		Chunk lastChunk = ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1));
		Iterable<Row> i = lastChunk.iterateOutputRows(gv, splitNr, splitCount);
		return i;
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getGroupSourceCache().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		if (outputColumnDefList == null) {
			
				// it is nested foreach, getting the last Chunk, it should be generate chunk
				Chunk lastChunk = ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1));
				if (lastChunk instanceof Generate) {
					outputColumnDefList = lastChunk.getOutputColumnDefList();
				} else {
					throw new ValidationException("Unimplemented case for FOREACH " + toString() + ", it is nested FOREACH, but "
							+ "the last chunk in the nested list is not GENERATE");
				}
			
		}
		return outputColumnDefList;
	}

	private Chunk getAliasSourceChunk() throws IOException {
		if (aliasSourceChunk == null)
			aliasSourceChunk = ChunkFactory.getChunk(getGroupSourceId());//getButcheredPigScript().getChunkByAlias(getSourceGroup(), this.getId());
		return aliasSourceChunk;
	}

	private void setAliasSourceChunk(Chunk aliasSourceChunk) {
		this.aliasSourceChunk = aliasSourceChunk;
	}

	private Map<Integer, Object> getGroupSum() {
		if (groupSum == null)
			groupSum = new HashMap<>();
		return groupSum;
	}

	private void setGroupSum(Map<Integer, Object> groupSum) {
		this.groupSum = groupSum;
	}

	private Map<Integer, Object> getGroupCount() {
		if (groupCount == null)
			groupCount = new HashMap<>();
		return groupCount;
	}

	private void setGroupCount(Map<Integer, Object> groupCount) {
		this.groupCount = groupCount;
	}

	private Group getGroupSourceCache() {
		if (groupSourceCache == null && groupSourceId != null) {
			groupSourceCache = (Group) ChunkFactory.getChunk(groupSourceId);
		} else if (groupSourceCache == null && groupSourceId == null) {
			groupSourceCache = new Group();
			groupSourceCache.setButcheredPigScriptId(getButcheredPigScriptId());
			groupSourceCache.setUserId(getUserId());
			
			this.groupSourceId = groupSourceCache.getId();
		}
		return groupSourceCache;
	}
	
	public UUID getGroupSourceId() {
		return this.groupSourceId;
	}

	public void setGroupSourceId(UUID groupSourceId) {
		this.groupSourceId = groupSourceId;
	}

	public List<UUID> getNestedChunkIds() {
		if (nestedChunkIds == null)
			nestedChunkIds = new ArrayList<>();
		return nestedChunkIds;
	}

	private void setNestedChunkIds(List<UUID> nestedChunkIds) {
		this.nestedChunkIds = nestedChunkIds;
	}

	public Group getSourceGroup() {

//		try {
//			// TODO DEBUG
//			for (GroupValue gv : getGroupSourceCache().iterateOutputGroupValue())
//				System.out.println(gv);
//		} catch (IOException | ValidationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return getGroupSourceCache();
	}

	private Map<Integer, Object> getGroupMin() {
		if (groupMin == null)
			groupMin = new HashMap<>();
		return groupMin;
	}

	private void setGroupMin(Map<Integer, Object> groupMin) {
		this.groupMin = groupMin;
	}

	private Map<Integer, Object> getGroupMax() {
		if (groupMax == null)
			groupMax = new HashMap<>();
		return groupMax;
	}

	private void setGroupMax(Map<Integer, Object> groupMax) {
		this.groupMax = groupMax;
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		
			Chunk lastChunk = ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1));
			return lastChunk.isOutputGrouped();
		
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		Chunk lastChunk = ChunkFactory.getChunk(getNestedChunkIds().get(getNestedChunkIds().size() - 1));
		return lastChunk.iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
