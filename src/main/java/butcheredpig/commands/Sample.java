package butcheredpig.commands;

import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static butcheredpig.Const.SAMPLE;

import java.io.IOException;
import java.util.Iterator;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.TrickyString;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Sample extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private String toDump = null;
	
	@Expose
	@NotNull
	@AssertValid
	private double sample = 1.01;
	
	private Chunk toDumpChunk = null;
	
	public Sample() {
		
	}
	
	public Sample(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);

		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// DUMP keyword
		ts.expectedAlphanumericWord(SAMPLE.toLowerCase());
		
		// source alias
		setToDump(ts.getNextAlphanumericWord());
		
		// sample rate
		setSample(ts.getNextNumber());
		
		return this;
	}
	
	@Override
	public String toStringScript() {
		return String.format("%s = SAMPLE %s %lf", 
				getAliasNew(), 
				getToDump(),
				getSample());
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + SAMPLE.toLowerCase() 
			+ REGEX_SPACE + REGEX_ALIAS + REGEX_SPACE + RegexUtils.DOUBLE + REGEX_SPACE + ".*");
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		if (getToDumpChunk() != null
				&& getToDumpChunk().getProgress().isDone())
			return getToDumpChunk().getSourceApproxBytesSize();
		return -1;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return (long) (getSourceApproxBytesSize() * getSample());
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return iterateOutputRows(gv, splitNr, splitCount);
	}
		
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		if (getToDumpChunk() == null)
			return null;
		
		try {
			Iterator<Row> all = getToDumpChunk().iterateOutputRows(gv, splitNr, splitCount).iterator();
			
			return new Iterable<Row>() {
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							Row r = null;
							
							while (hasNext()) {
								r = all.next();
								if (RandomUtils.nextDouble() < getSample())
									return r;
							}
							
							return r;
						}
						
						@Override
						public boolean hasNext() {
							return all.hasNext();
						}
					};
				}
			};
		} catch (Exception e) {
			LibsLogger.error(Sample.class, "Cannot iterate sample", e);
			return null;
		}
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (getToDumpChunk() == null || !getToDumpChunk().getProgress().isDone()) {
			LibsLogger.info(Sample.class, "Dump is not ready yet");
			return;
		}
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
	private Chunk getToDumpChunk() throws IOException {
		if (toDumpChunk == null)
			toDumpChunk = getButcheredPigScript().getChunkByAlias(getToDump(), this.getPrevChunkId());
		return toDumpChunk;
	}

	// TODO name
	public String getToDump() {
		return toDump;
	}

	public void setToDump(String toDump) {
		this.toDump = toDump;
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getToDumpChunk().getOutputColumnDefList();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}

	public double getSample() {
		return sample;
	}

	public void setSample(double sample) {
		this.sample = sample;
	}
	
	public void setSample(String sample) {
		this.sample = NumberUtils.toDouble(sample);
	}
	

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getToDumpChunk().isOutputGrouped();
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getToDumpChunk().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
