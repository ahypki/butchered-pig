package butcheredpig.commands;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;

public class Comment extends Chunk {
	
	@Expose
	private String comment = null;
	
	public Comment() {
		
	}
	
	public Comment(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {		
		setComment(toParse);
		return this;
	}
	
	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		return toParse.trim().startsWith("--");
	}

	@Override
	public void run() throws IOException, ValidationException {
		// do nothing
		
		getProgress().done(null);
		save();
	}
	
	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return 0;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return 0;
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}
		
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		return new EmptyRowIterator();
	}

	public String getComment() {
		return comment;
	}
	
	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return null;
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getSourceColumnDefList();
	}
	
	@Override
	public String toString() {
		return toStringScript();
	}
	
	@Override
	public String toStringScript() {
		return getComment() != null ? "-- " + getComment() : null;
	}

	public void setComment(String comment) {
		if (comment == null) {
			// do nothing
		} else {
			if (comment.startsWith("--"))
				this.comment = comment.substring(2);
			else
				this.comment = comment;
		}
	}


	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		// do nothing
	}
}
