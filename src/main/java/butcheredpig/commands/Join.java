package butcheredpig.commands;

import static butcheredpig.Const.BY;
import static butcheredpig.Const.JOIN;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.javatuples.Pair;

import com.google.gson.annotations.Expose;

import butcheredpig.Const;
import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.Chunk;
import butcheredpig.chunks.ChunkFactory;
import butcheredpig.chunks.IterateCross;
import butcheredpig.chunks.Progress;
import butcheredpig.chunks.SortedGroupChunk;
import butcheredpig.chunks.Status;
import butcheredpig.commands.utils.EmptyGroupValueIterator;
import butcheredpig.commands.utils.EmptyRowIterator;
import butcheredpig.commands.utils.GroupValue;
import butcheredpig.commands.utils.PairSerializer;
import butcheredpig.commands.utils.RewindableChunk;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.CrossConnector;
import net.beanscode.model.plugins.RewindableConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.math.Expression;
import net.hypki.libs5.utils.math.ExpressionList;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Join extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private List<Pair<String, ExpressionList>> toJoin = null;
	
	@Expose
	@NotNull
	@AssertValid
	private List<JoinType> joinType = null;
	
	@Expose
	private List<UUID> sortJobs = null;

	static {
		JsonUtils.registerTypeAdapter(Pair.class, new PairSerializer());
	}
	
	public Join() {
		
	}
	
	public Join(String toParse) throws IOException, ValidationException {
		parse(null, toParse);
	}
	
	@Override
	public String toStringScript() {
		StringBuilder sb = new StringBuilder();
		for (Pair<String, ExpressionList> pair : getToJoin()) {
			sb.append(pair.getValue0());
			sb.append(" BY ");
			sb.append(pair.getValue1().toString());
		}
		return String.format("%s = JOIN %s", 
				getAliasNew(), 
				sb.toString());
	}
	
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		TrickyString ts = new TrickyString(toParse);
		
		// new alias
		setAliasNew(ts.getNextAlphanumericWord());
		
		// =
		ts.expectedPrintableCharacter("=");

		// JOIN
		ts.expectedAlphanumericWord(JOIN.toLowerCase());
		
		// parsing aliases to join
		while (ts.length() > 0) {
			String aliasToToin = ts.getNextAlphanumericWordWithUnderscore();
			
			Pair<String, ExpressionList> toJoin = new Pair<>(aliasToToin, new ExpressionList());
			
			ts.expectedAlphanumericWord(BY.toLowerCase());
			
			// parsing field, or a list of fields
			if (ts.checkNextPrintableCharacter().equals("(")) {
				EscapeString es = new EscapeString(ts.checkLine());
				es.escapeAll("(\\([^\\)\\(]*\\))");
				
				int semiIdx = es.getEscapedText().indexOf(',');
				String fields = semiIdx > 0 ? es.getEscapedText().substring(0, semiIdx) : es.getEscapedText();
				fields = es.deescapeString(fields);
				
				String newTs = semiIdx > 0 ? es.getEscapedText().substring(semiIdx) : "";
				newTs = es.deescapeString(newTs);
				ts = new TrickyString(newTs);
								
				toJoin = toJoin.setAt1(new ExpressionList(fields));
				
			} else {
				// one field
				toJoin.getValue1().getExpressions().add(new Expression(ts.getNextAlphanumericWordWithUnderscore()));
			}
			
			getToJoin().add(toJoin);
			
			// reading LEFT|RIGHT|FULL] [OUTER
			if (ts.checkNextAlphabeticalWord() != null && ts.checkNextAlphabeticalWord().equalsIgnoreCase(JoinType.LEFT.toString())) {
				ts.expectedAlphanumericWord(JoinType.LEFT.toString());
				getJoinType().add(JoinType.LEFT);
			} else if (ts.checkNextAlphabeticalWord() != null && ts.checkNextAlphabeticalWord().equalsIgnoreCase(JoinType.RIGHT.toString())) {
				ts.expectedAlphanumericWord(JoinType.RIGHT.toString());
				getJoinType().add(JoinType.RIGHT);
			} else {
				getJoinType().add(JoinType.FULL);
			}
			
			if (ts.checkNextAlphabeticalWord() != null && ts.checkNextAlphabeticalWord().equalsIgnoreCase(Const.OUTER.toString())) {
				ts.expectedAlphanumericWord(Const.OUTER.toString());
			}
			
			if (ts.checkNextPrintableCharacter() != null && ts.checkNextPrintableCharacter().equals(","))
				ts.expectedPrintableCharacter(",");
		}
		
		return this;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().trim();
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + JOIN.toLowerCase() + REGEX_SPACE + REGEX_ALIAS + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		final int nrJoins = getToJoin().size();
		
		// first sorting all input sources
		if (getSortJobs().size() == 0) {
			// creating jobs to sort input sources
			for (int i = 0; i < nrJoins; i++) {
				Pair<String, ExpressionList> p = getToJoin().get(i);
				Chunk chunkToJoin = getButcheredPigScript().getChunkByAlias(p.getValue0(), this.getPrevChunkId());
				
				List<String> sortBy = new ArrayList<>();
				for (Expression ex : p.getValue1()) {
					sortBy.add(ex.toString()); // TODO implement other
				}
				SortedGroupChunk sc;
				try {
					sc = new SortedGroupChunk(chunkToJoin, null, sortBy);
					sc.save();
					
					getSortJobs().add(sc.getId());
				} catch (ValidationException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			if (getSortJobs().size() == 0)
				getProgress().setStatus(Status.DONE);
			else
				getProgress().setStatus(Status.RUNNING);
			save();
			return;
		} else {
			// checking if all sort jobs were sorted
			Progress finalProgress = ChunkFactory.getFinalProgress(getSortJobs());
			
			LibsLogger.info(Join.class, "JOIN progress " + finalProgress);
			
			if (!(finalProgress.isDone() || finalProgress.isFailed()))
				return; // wait
		}

		// source chunks are sorted, getting chunks instances
		List<Chunk> chunks = new ArrayList<>();
		for (UUID sortJob : getSortJobs()) {
			chunks.add(ChunkFactory.getChunk(sortJob));
		}
		
//		for (Chunk ch : chunks) {
//			for (Row r : ch.iterateOutputRows(null, 0, 1))
//				LibsLogger.info(Join.class, r.get("id1") + "  " + r.get("id2"));
//		}
		
		try {
			// creating the connector for joined rows
			H5Connector h5Joined = new H5Connector(getId(), new File("chunk-" + getId()));
		
			// setting output columns defs
			h5Joined.setColumnDefs(getDestColumnDefList());
					
			// opening iterators to chunks
			List<Iterator<Row>> chunkIter = new ArrayList<>();
			List<RewindableConnector> chunkRewindableConn = new ArrayList<>();
			List<Row> chunkLastRow = new ArrayList<>();
			List<String> chuckAliasNew = new ArrayList<>();
			chunkIter.add(null); // left row
			chunkRewindableConn.add(new RewindableConnector(chunks.get(0).getOutputColumnDefList()));
			chunkLastRow.add(null);
			chuckAliasNew.add(getToJoin().get(0).getValue0());
			for (int i = 1; i < chunks.size(); i++) {
				chunkIter.add(chunks.get(i).iterateOutputRows(null, 0, 1).iterator());
				chunkRewindableConn.add(new RewindableConnector(chunks.get(i).getOutputColumnDefList()));
				chunkLastRow.add(null);
				chuckAliasNew.add(getToJoin().get(i).getValue0());
			}
			
			// iterate left rows
			for (Row leftRow : chunks.get(0).iterateOutputRows(null, 0, 1)) {
				Pair<String, ExpressionList> pLeft = getToJoin().get(0);
				
				chunkRewindableConn.get(0).clear();
				chunkRewindableConn.get(0).write(leftRow);
				
				// check if the new row has the same values as the new row
				if (chunkLastRow.get(0) != null && compare(pLeft, pLeft, chunkLastRow.get(0), leftRow) == 0) {
					// reusing right connectors, do nothing here = reuse RewindableConnectors
				} else {
//					for (RewindableConnector rc : chunkRewindableConn)
//						rc.clear();
					
//					chunkRewindableConn.get(0).write(leftRow);
					
					// collecting rows which were already read, so they are not lost
					for (int i = 1; i < chunks.size(); i++) {
						chunkRewindableConn.get(i).clear();
						
						if (chunkLastRow.get(i) != null) {
							Pair<String, ExpressionList> pRight = getToJoin().get(i);
							if (compare(pLeft, pRight, leftRow, chunkLastRow.get(i)) == 0) {
								chunkRewindableConn.get(i).write(chunkLastRow.get(i));
							}
						}
					}
					
					// reading right rows, until the row is larger than the leftRow, then stop
					for (int i = 1; i < chunks.size(); i++) {
						Pair<String, ExpressionList> pRight = getToJoin().get(i);
						while (chunkIter.get(i).hasNext()) {
							Row rightRow = chunkIter.get(i).next();
							int comp = compare(pLeft, pRight, leftRow, rightRow);
							if (comp == 0) {
//								if (rightRow.getAsDouble("escEvoBin::xlum1f") > 2000) // TODO DEBUG
//									LibsLogger.info(Join.class, "break");
								chunkRewindableConn.get(i).write(rightRow);
								continue;
							} 
							
							// checking if the row went too far
							if (comp < 0) {// leftDec.compareTo(rightDec) < 0) {
								chunkLastRow.set(i, rightRow); // just caching
								break;
							}
						}
					}
				}
				
				chunkLastRow.set(0, leftRow);
							
				// now computing cross for this join and saving the rows
				for (Row row : new CrossConnector(chunkRewindableConn, chuckAliasNew).iterateRows(null)) {
//					if (((double)row.getColumnWhichEndsWith("xlum1f")) > 2000)
//						LibsLogger.info(Join.class, "break");
					h5Joined.write(row);
				}
				
			}
					
			h5Joined.close();
			
			// clearing sorted chunks, they are not needed anymore
			for (Chunk ch : chunks)
				ch.clear();
		} catch (Throwable t) {
			LibsLogger.error(Join.class, "Cannot make a join", t);
		}
		
		getProgress().setStatus(Status.DONE);
		save();
	}
	
//	private void write(Pair<String, ExpressionList> pLeft, Pair<String, ExpressionList> pRight, Row leftRow, Row rightRow, H5Connector h5Joined) throws IOException {
//		// the row goes to output - it has the same keys/expression for all aliases
//		Row rowJoined = new Row(UUID.random().getId());
//		rowJoined.addColumns(leftRow.getColumns(), pLeft.getValue0() + "::");
//		rowJoined.addColumns(rightRow.getColumns(), pRight.getValue0() + "::");
//		
//		h5Joined.write(rowJoined);
//	}
	
	// TODO remove later
	private boolean isEqual(Pair<String, ExpressionList> pLeft, Pair<String, ExpressionList> pRight, Row r) {
		int comp = 0;
		for (int j = 0; j < pLeft.getValue1().getExpressions().size(); j++) {
			Comparable o1 = r.contains(pLeft.getValue0() + "::" + pLeft.getValue1().getExpressions().get(j).toString()) ?
							(Comparable) r.get(pLeft.getValue0() + "::" + pLeft.getValue1().getExpressions().get(j).toString()) : 
								r.compute(pLeft.getValue0() + "::" + pLeft.getValue1().getExpressions().get(j).toString(), null);
			Comparable o2 = r.contains(pRight.getValue0() + "::" + pRight.getValue1().getExpressions().get(j).toString()) ?
							(Comparable) r.get(pRight.getValue0() + "::" + pRight.getValue1().getExpressions().get(j).toString()) : 
								r.compute(pRight.getValue0() + "::" + pRight.getValue1().getExpressions().get(j).toString(), null);
			comp = o1.compareTo(o2);
			if (comp != 0) {
				return false;
			}
		}
		return true;
	}

	// TODO remove later
	private int compare(Pair<String, ExpressionList> pLeft, Pair<String, ExpressionList> pRight, Row leftRow, Row rightRow) {
		try {
			int comp = 0;
			for (int j = 0; j < pLeft.getValue1().getExpressions().size(); j++) {
				Comparable o1 = leftRow.contains(pLeft.getValue1().getExpressions().get(j).toString()) ?
								(Comparable) leftRow.get(pLeft.getValue1().getExpressions().get(j).toString()) : 
									leftRow.compute(pLeft.getValue1().getExpressions().get(j).toString(), null);
				Comparable o2 = rightRow.contains(pRight.getValue1().getExpressions().get(j).toString()) ?
								(Comparable) rightRow.get(pRight.getValue1().getExpressions().get(j).toString()) : 
									rightRow.compute(pRight.getValue1().getExpressions().get(j).toString(), null);
				comp = o1.compareTo(o2);
				if (comp == -1)
					return -1;
				else if (comp == 1)
					return 1;
			}
			return 0;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private ColumnDefList getDestColumnDefList() throws IOException, ValidationException {
		try {
			final ColumnDefList columnsDefAll = new ColumnDefList();

			for (int i = 0; i < getToJoin().size(); i++) {
				Pair<String, ExpressionList> p = getToJoin().get(i);
				Chunk chunkToJoin = getButcheredPigScript().getChunkByAlias(p.getValue0(), this.getPrevChunkId());
				
				for (ColumnDef col : chunkToJoin.getOutputColumnDefList())
					columnsDefAll.add(new ColumnDef(p.getValue0() + "::" + col.getName(), null, col.getType()));
			}
			
			return columnsDefAll;
		} catch (Throwable e) {
			LibsLogger.error(Join.class, " ", e);
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ColumnDefList();
		}
	}

	public List<Pair<String, ExpressionList>> getToJoin() {
		if (toJoin == null)
			toJoin = new ArrayList<>();
		return toJoin;
	}

	public void setToJoin(List<Pair<String, ExpressionList>> toJoin) {
		this.toJoin = toJoin;
	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		long bytes = 0;
		for (Pair<String, ExpressionList> pair : getToJoin())
			bytes += getButcheredPigScript().getChunkByAlias(pair.getValue0(), this.getPrevChunkId()).getSourceApproxBytesSize();
		return bytes;
	}
	
	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getSourceApproxBytesSize();
	}
	
	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
//		throw new IOException("There is multiple sources for JOIN");
		return new EmptyRowIterator();
	}
	
	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount) throws IOException, ValidationException {
		assertTrue(gv == null, JOIN.toString() + " is not implemented for groups, no " + GroupValue.class.getSimpleName() 
				+ " was expected");
		return new H5Connector(getId(), new File("chunk-" + getId())).iterateRows(null, splitNr, splitCount);
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return new H5Connector(getId(), new File("chunk-" + getId())).getColumnDefs();
	}
	
	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getDestColumnDefList();
	}

	public List<UUID> getSortJobs() {
		if (sortJobs == null)
			sortJobs = new ArrayList<>();
		return sortJobs;
	}

	public void setSortJobs(List<UUID> sortJobs) {
		this.sortJobs = sortJobs;
	}
	
	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return false;
	}
	
	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return new EmptyGroupValueIterator();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		new FileExt("chunk-" + getId()).deleteIfExists();
		
		for (UUID uuid : getSortJobs()) {
			Chunk ch = ChunkFactory.getChunk(uuid);
			if (ch != null)
				ch.clear();
		}
	}

	public List<JoinType> getJoinType() {
		if (joinType == null)
			joinType = new ArrayList<>();
		return joinType;
	}

	public void setJoinType(List<JoinType> joinType) {
		this.joinType = joinType;
	}
}
