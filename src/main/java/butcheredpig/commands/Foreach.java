package butcheredpig.commands;

import static butcheredpig.Const.BY;
import static butcheredpig.Const.FOREACH;
import static butcheredpig.Const.GENERATE;
import static butcheredpig.Const.GROUP;
import static butcheredpig.Const.REGEX_ALIAS;
import static butcheredpig.Const.REGEX_SPACE;
import static net.hypki.libs5.utils.string.StringUtilities.areParenthesesMatching;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import butcheredpig.chunks.ButcheredPigScript;
import butcheredpig.chunks.ButcheredPigScriptFactory;
import butcheredpig.chunks.Chunk;
import butcheredpig.commands.utils.GroupValue;
import butcheredpig.query.Field;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.math.ExpressionList;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Foreach extends Chunk {
	
	@Expose
	@NotNull
	@AssertValid
	private Chunk foreach = null;
	
	public Foreach() {
		
	}
	
	@Override
	public String toStringScript() {
		return getForeach().toStringScript();
	}
		
	@Override
	public Chunk parse(ButcheredPigScript butcheredPigScript, String toParse) throws IOException, ValidationException {
		try {
			TrickyString ts = new TrickyString(toParse);
			
			// alias
			setAliasNew(ts.getNextAlphanumericWordWithUnderscore());
			
			// = 
			ts.expectedPrintableCharacter("=");
			
			// FOREACH
			ts.expectedAlphanumericWord(FOREACH.toLowerCase());
			
			Group sourceGroup = null;
			Chunk sourceChunk = null;
			
			if (ts.checkLine().trim().startsWith("(")) {
				// source alias is a group
				ts.expectedPrintableCharacter("(");
				ts.expectedAlphanumericWord(GROUP.toLowerCase());
				
				sourceGroup = new Group(); // TODO save it later
				sourceGroup.setButcheredPigScript(butcheredPigScript);
				sourceGroup.setUserId(getUserId());
				sourceGroup.setAliasSource(ts.getNextAlphanumericWord());
				sourceGroup.setPrevChunkId(this.getPrevChunkId());
				
				ts.expectedAlphanumericWord(BY.toLowerCase());
				
				// parsing field, or a list of fields
				if (ts.checkNextPrintableCharacter().equals("(")) {
					EscapeString es = new EscapeString(ts.checkLine());
					es.escapeAll("(\\([^\\)\\(]*\\))");
					
					int semiIdx = es.getEscapedText().indexOf(',');
					String fields = semiIdx > 0 ? es.getEscapedText().substring(0, semiIdx) : es.getEscapedText();
					fields = es.deescapeString(fields);
					
					String newTs = semiIdx > 0 ? es.getEscapedText().substring(semiIdx) : "";
					newTs = es.deescapeString(newTs);
					ts = new TrickyString(newTs);
					
					sourceGroup.setByTuple(new ExpressionList(fields));
				} else {
					// one field
					String fieldOrExpr = ts.getNextAlphanumericWordWithUnderscore();
					sourceGroup.setByField(fieldOrExpr.trim());
				}
				
				sourceGroup.save();
				
				ts.expectedPrintableCharacter(")");
			} else {
				// one source alias
				final String sourceAliasName = ts.getNextAlphanumericWord();
				final Chunk sourceAlias = butcheredPigScript.getChunkByAlias(sourceAliasName, getPrevChunkId());
				if (sourceAlias.isOutputGrouped()) {
					// source alias is grouped
					sourceChunk = sourceAlias;
				} else {
					// source alias is not grouped
					sourceChunk = sourceAlias;
				}
			}
			
			if (ts.checkLine().trim().startsWith("{")) {
				// nested FOREACH
				setForeach(new ForeachNested());
				
				// source alias has to be grouped
				if (sourceChunk != null && sourceChunk.isOutputGrouped()) {
					sourceGroup = (Group) sourceChunk;
					sourceChunk = null;
				}
				assertTrue(sourceChunk == null, "For nested FOREACH it is expected that source alias is grouped");
				
				getForeachNested().setGroupSourceId(sourceGroup.getId());
				getForeach().setButcheredPigScript(butcheredPigScript);
				getForeach().setUserId(getUserId());
				getForeach().setPrevChunkId(getId());
				getForeach().setAliasNew(getAliasNew());
				
				String nestedScript = ts.checkLine().trim().substring(ts.checkLine().trim().indexOf('{') + 1);
				nestedScript = nestedScript.substring(0, nestedScript.lastIndexOf('}'));
				
				UUID prevChunkId = getId();
				for (Chunk ch : ButcheredPigScriptFactory.parseToChunks(nestedScript, getUserId(), butcheredPigScript)) {
					ch.setPrevChunkId(prevChunkId);
					ch.save();
					
					prevChunkId = ch.getId();
					
					getForeachNested().getNestedChunkIds().add(ch.getId());
				}
				
				getForeach().save();
				
			} else {
				// grouped or not grouped simple GENERATE command
				if (sourceGroup != null) {
					setForeach(new ForeachNotNestedGrouped());
					getForeachNotNestedGrouped().setSourceGroup(sourceGroup);
				} else if (sourceChunk.isOutputGrouped()) {
					setForeach(new ForeachNotNestedGrouped());
					getForeachNotNestedGrouped().setSourceGroup(sourceChunk);
				} else {
					setForeach(new ForeachNotNestedNotGrouped());
					getForeachNotNestedNotGrouped().setAliasSource(sourceChunk);
				}
				getForeach().setButcheredPigScript(butcheredPigScript);
				getForeach().setUserId(getUserId());
				getForeach().setPrevChunkId(getId());
				getForeach().setAliasNew(getAliasNew());
				
				// GENERATE
				ts.expectedAlphanumericWord(GENERATE.toLowerCase());
				
				// fields
				EscapeString es = new EscapeString(ts.checkLine());
				es.escapeAll("(\\([^\\)\\(]*\\))");
				
				// list of fields
				TrickyString tsFields = new TrickyString(es.getEscapedText());
				while (!tsFields.isEmpty()) {
					String field = null;
					try {
						field = tsFields.getNextStringUntil(",", false);
						
						while (!areParenthesesMatching(field)) {
							// parentheses have to mach
							if (tsFields.length() > 0
									&& tsFields.checkNextPrintableCharacter().equals(",")) {
								tsFields.expectedPrintableCharacter(",");
								field += ",";
							}
							
							String t = tsFields.getNextStringUntil(",", true);
							if (t == null) {
								field += tsFields.getLine();
								break;
							} else
								field += t;
						}
						
						if (field == null)
							field = tsFields.getLine();
						
						Field fieldF = new Field(es.deescapeString(field));
						if (getForeach() instanceof ForeachNotNestedGrouped)
							getForeachNotNestedGrouped().getFields().add(fieldF);
						else
							getForeachNotNestedNotGrouped().getFields().add(fieldF);
						
						if (tsFields.length() > 0
								&& tsFields.checkNextPrintableCharacter().equals(","))
							tsFields.expectedPrintableCharacter(",");
					} catch (Exception e) {
						LibsLogger.error(Foreach.class, "Cannot  parse " + field + " in " + tsFields, e);
						throw new IOException("Cannot parse " + tsFields, e);
					}
				}
				
				getForeach().save();
			}
		} catch (Exception e) {
			throw new IOException("Cannot parse Foreach command", e);
		}
		
		return this;
	}
	
	@Override
	public void setButcheredPigScript(ButcheredPigScript butcheredPigScript) {
		super.setButcheredPigScript(butcheredPigScript);
		if (getForeach() != null)
			getForeach().setButcheredPigScript(butcheredPigScript);
	}
	
	@Override
	public void setButcheredPigScriptId(UUID butcheredPigScriptId) {
		super.setButcheredPigScriptId(butcheredPigScriptId);
		if (getForeach() != null)
			getForeach().setButcheredPigScriptId(butcheredPigScriptId);
	}
	
	public ForeachNested getForeachNested() {
		return (ForeachNested) getForeach();
	}
	
	public ForeachNotNestedNotGrouped getForeachNotNestedNotGrouped() {
		return (ForeachNotNestedNotGrouped) getForeach();
	}
	
	public ForeachNotNestedGrouped getForeachNotNestedGrouped() {
		return (ForeachNotNestedGrouped) getForeach();
	}
	
	public Chunk getForeach() {
		return foreach;
	}

	public void setForeach(Chunk foreach) {
		this.foreach = foreach;
	}

	@Override
	public boolean whoWantsToParse(String toParse) throws IOException {
		toParse = toParse.toLowerCase().replace("\n", " ").replace("\r", " ");
		return toParse.matches(REGEX_ALIAS + REGEX_SPACE + "=" + REGEX_SPACE + FOREACH.toLowerCase() + REGEX_SPACE + ".*");
	}

	@Override
	public void run() throws IOException, ValidationException {
		if (isFinished())
			return;
		
		getForeach().run();
		
		if (getForeach().isFinished()) {
			setProgress(getForeach().getProgress());
			save();
		}
	}
//	
//	@Override
//	public Progress getProgress() {
//		return foreach != null ? getForeach().getProgress() : new Progress(0, Status.NEW);
//	}
//	
//	@Override
//	public void setProgress(Progress progress) {
//		if (progress != null
//				&& foreach != null)
//		getForeach().setProgress(progress);
//	}

	@Override
	public long getSourceApproxBytesSize() throws IOException, ValidationException {
		return getForeach().getSourceApproxBytesSize();
	}

	@Override
	public ColumnDefList getSourceColumnDefList() throws IOException, ValidationException {
		return getForeach().getSourceColumnDefList();
	}

	@Override
	public Iterable<Row> iterateSourceRows(GroupValue gv, int splitNr, int splitCount)
			throws IOException, ValidationException {
		return getForeach().iterateSourceRows(gv, splitNr, splitCount);
	}

	@Override
	public long getOutputApproxBytesSize() throws IOException, ValidationException {
		return getForeach().getOutputApproxBytesSize();
	}

	@Override
	public ColumnDefList getOutputColumnDefList() throws IOException, ValidationException {
		return getForeach().getOutputColumnDefList();
	}

	@Override
	public Iterable<Row> iterateOutputRows(GroupValue gv, int splitNr, int splitCount)
			throws IOException, ValidationException {
		return getForeach().iterateOutputRows(gv, splitNr, splitCount);
	}

	@Override
	public boolean isOutputGrouped() throws IOException, ValidationException {
		return getForeach().isOutputGrouped();
	}

	@Override
	public Iterable<GroupValue> iterateOutputGroupValue() throws IOException, ValidationException {
		return getForeach().iterateOutputGroupValue();
	}
	
	@Override
	public void clear() throws IOException, ValidationException {
		getForeach().clear();
	}
}
