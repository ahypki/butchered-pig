-- name: edergreger
-- create: 2023-02-24T18:53:41.669+01:00
-- notebookId: b55edd26686b64a89ecb5d664125cee5
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca" tables="system"' using BeansTable();
dump sys;


-- name: path
-- create: 2023-02-28T14:56:07.630+01:00
-- notebookId: 27031cca061b78a578cc937c9410d0ae
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca" tables="system"' using BeansTable();

sys = foreach sys generate tphys, DSPARAMSTRING(DSID(tbid), 'path') as path:chararray;

store sys into 'name="sys with paths"' using BeansTable();


-- name: Testing scripts for plain, gziped an h5 files
-- create: 2023-07-19T09:22:43.655+02:00
-- notebookId: 6473a4b72eeb54536c000a6e65b2a73d
-- user: s@hypki.net
sysPlain = load 'datasets="mocca plain==1" tables="system"' using BeansTable();

sysPlainGr = group sysPlain all;
sysPlain = foreach sysPlainGr generate SUM(sysPlain.smt) as smtSum;

store sysPlain into 'name="sysPlain"' using BeansTable();




-- name: Testing scripts for plain, gziped an h5 files
-- create: 2023-07-19T09:22:43.655+02:00
-- notebookId: 6473a4b72eeb54536c000a6e65b2a73d
-- user: s@hypki.net
sysPlain = load 'datasets="mocca plain==1" tables="snapshot"' using BeansTable();

sysPlainGr = group sysPlain all;
sysPlain = foreach sysPlainGr generate SUM(sysPlain.im) as imSum;

store sysPlain into 'name="sysPlain"' using BeansTable();




-- name: snapshot, plain
-- create: 2023-07-19T09:22:43.655+02:00
-- notebookId: 6473a4b72eeb54536c000a6e65b2a73d
-- user: s@hypki.net
a = load 'datasets="mocca plain==1" tables="snapshot"' using BeansTable();

aGr = group a all;
a = foreach aGr generate SUM(a.im) as imSum;

store a into 'name="snapPlain"' using BeansTable();




-- name: snapshot, h5
-- create: 2023-07-19T09:22:43.655+02:00
-- notebookId: 6473a4b72eeb54536c000a6e65b2a73d
-- user: s@hypki.net
a = load 'datasets="mocca h5==1" tables="snapshot"' using BeansTable();

aGr = group a all;
a = foreach aGr generate SUM(a.im) as imSum;

store a into 'name="snapPlain"' using BeansTable();




-- name: Searching for BHs
-- create: 2022-09-30T08:34:51.264+02:00
-- notebookId: ec8d65adbe22537fb14a3beab540c8ac
-- user: s@hypki.net



-- name: wd ids
-- create: 2023-02-22T08:22:22.460+01:00
-- notebookId: fcff384e2710d6972073b17c729ce2d5
-- user: arkadiusz@hypki.net
wdsBin = load 'DATASETS="MOCCA"  TABLES="inter binevol"' using BeansTable();
wdsSin = load 'DATASETS="MOCCA"  TABLES="inter sinevol"' using BeansTable();
-- FILTER="ik1f > 9 OR ik2f > 9"

wds1 = filter wdsBin by ik1f >= 10 and ik1f <= 12;
wds1 = foreach wds1 generate
		tbid		as tbidEvol,
        ik1f		as ik,
        id1f		as id;
wds1 = distinct wds1;

wds2 = filter wdsBin by ik2f >= 10 and ik2f <= 12;        
wds2 = foreach wds2 generate
		tbid		as tbidEvol,
        ik2f		as ik,
        id2f		as id;
wds2 = distinct wds2;

wds3 = filter wdsSin by ik1f >= 10 and ik1f <= 12;        
wds3 = foreach wds3 generate
		tbid		as tbidEvol,
        ik1f		as ik,
        id1f		as id;
wds3 = distinct wds3;

wds = union wds1, wds2, wds3;
wds = distinct wds;

wds = foreach wds generate
		DSID(tbidEvol) as dsid,
        ik,
        id;

store wds into ' name="Unique WDs IDs (Survey5)" ' using BeansTable();


-- name: initmodel
-- create: 2023-02-22T08:22:23.153+01:00
-- notebookId: fcff384e2710d6972073b17c729ce2d5
-- user: arkadiusz@hypki.net
wds = load 'datasets="Histories wds" tables="Unique WD IDs Survey5"' using BeansTable();

-- initmodel.dat
init = LOAD 'datasets="MOCCA" tables="initmodel"' using BeansTable();
history1 = JOIN init BY (DSID(tbid), idd1), wds by (dsid, id);
init = filter init by sm2 > 0.0;
history2 = JOIN init BY (DSID(tbid), idd2), wds by (dsid, id);

history1 = FOREACH history1 GENERATE DSID(init::tbid) as dsid, 
                FLATTEN(mocca.HistoryLine3(init::idd1, *)) as (lineType:chararray, 
outputId:long, timenr:int, tphys:double, id:int, newId:int, summary:chararray, r:double, 
oldVr:double, oldVt:double, newVr:double, newVt:double, 
oldBinType:int, newBinType:int, oldMass:double, newMass:double, 
oldStarType:int, newStarType:int, oldRadius:double, newRadius:double, oldSpin:double, 
newSpin:double, oldSep:double, newSep:double, oldEcc:double, newEcc:double, oldCompId:int, 
newCompId:int, oldName:int, newName:int, chist:long,  oldCompOldMass:double, 
oldCompNewMass:double, oldCompOldStarType:int, oldCompNewStarType:int, 
oldCompOldRadius:double, oldCompNewRadius:double, oldCompOldSpin:double, 
oldCompNewSpin:double, oldCompNewSep:double, oldCompNewEcc:double, 
oldCompNewCompId:int, oldCompNewName:int, oldCompChist:long,  newCompOldMass:double, 
newCompNewMass:double, newCompOldStarType:int, newCompNewStarType:int, 
newCompOldRadius:double, newCompNewRadius:double, newCompOldSpin:double, 
newCompNewSpin:double, newCompOldSep:double, newCompOldEcc:double, 
newCompOldCompId:int, newCompOldName:int, newCompChist:long,  merger1Id:int, 
merger1OldMass:double, merger1OldStarType:int, merger1OldRadius:double, 
merger1OldSpin:double, merger1OldChist:long,  merger2Id:int, merger2OldMass:double, 
merger2OldStarType:int, merger2OldRadius:double, merger2OldSpin:double, 
merger2OldChist:long,  merger3Id:int, merger3OldMass:double, merger3OldStarType:int, 
merger3OldRadius:double, merger3OldSpin:double, merger3OldChist:long, 
interOldMass1:double, interOldMass2:double, interOldSep:double, interOldStarType1:int,
interOldStarType2:int);

history2 = FOREACH history2 GENERATE DSID(init::tbid) as dsid, 
                FLATTEN(mocca.HistoryLine3(init::idd2, *)) as (lineType:chararray, 
outputId:long, timenr:int, tphys:double, id:int, newId:int, summary:chararray, r:double, 
oldVr:double, oldVt:double, newVr:double, newVt:double, 
oldBinType:int, newBinType:int, oldMass:double, newMass:double, 
oldStarType:int, newStarType:int, oldRadius:double, newRadius:double, oldSpin:double, 
newSpin:double, oldSep:double, newSep:double, oldEcc:double, newEcc:double, oldCompId:int, 
newCompId:int, oldName:int, newName:int, chist:long,  oldCompOldMass:double, 
oldCompNewMass:double, oldCompOldStarType:int, oldCompNewStarType:int, 
oldCompOldRadius:double, oldCompNewRadius:double, oldCompOldSpin:double, 
oldCompNewSpin:double, oldCompNewSep:double, oldCompNewEcc:double, 
oldCompNewCompId:int, oldCompNewName:int, oldCompChist:long,  newCompOldMass:double, 
newCompNewMass:double, newCompOldStarType:int, newCompNewStarType:int, 
newCompOldRadius:double, newCompNewRadius:double, newCompOldSpin:double, 
newCompNewSpin:double, newCompOldSep:double, newCompOldEcc:double, 
newCompOldCompId:int, newCompOldName:int, newCompChist:long,  merger1Id:int, 
merger1OldMass:double, merger1OldStarType:int, merger1OldRadius:double, 
merger1OldSpin:double, merger1OldChist:long,  merger2Id:int, merger2OldMass:double, 
merger2OldStarType:int, merger2OldRadius:double, merger2OldSpin:double, 
merger2OldChist:long,  merger3Id:int, merger3OldMass:double, merger3OldStarType:int, 
merger3OldRadius:double, merger3OldSpin:double, merger3OldChist:long, 
interOldMass1:double, interOldMass2:double, interOldSep:double, interOldStarType1:int,
interOldStarType2:int);

history = union history1, history2;

STORE history INTO 'NAME "Histories of all WDs (Survey5, part, initmodel)" ' using BeansTable();


-- name: Sum of tphys from snapshot - 2/5 splits
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = load 'datasets="mocca 3d35" tables="snapshot"' using BeansTable();
snap = filter snap by tphys <= 0.0;
store snap into 'name="snap 0"' using BeansTable();


-- name: Sum of tphys from snapshot - 1/1 splits
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = load 'datasets="mocca 3d35" tables="snapshot"' using BeansTable();
snap = filter snap by tphys <= 0.0;
store snap into 'name="snap 0"' using BeansTable();


-- name: snap uniq tphys
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = load 'datasets="mocca 3d35" tables="snapshot"' using BeansTable();
snap = foreach snap generate tphys;
snap = distinct snap;
store snap into 'name="snap uniq tphys"' using BeansTable();


-- name: snap uniq tphys
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = load 'datasets="mocca plain" tables="snapshot"' using BeansTable();
snap = foreach snap generate tphys;
snap = distinct snap;
store snap into 'name="snap uniq tphys"' using BeansTable();


-- name: aux plain
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = LOAD ' DATASETS="MOCCA plain" TABLES="snapshot" FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' USING BeansTable();

snap = FOREACH snap GENERATE 
							tbid AS tbid1, 
							DSID(tbid) as dsid,
                            tphys,
                            timenr, 
                            FLATTEN(mocca.Projection2D(r).$0) AS (r:double),
                            sm1+sm2 AS sm, 
                            lum1+lum2 AS lum, 
                            0.0 AS cumSmR,
                            0.0 AS cumLumR;
                                            
STORE snap INTO ' NAME "survey5 selected snapshots rows only" ' using BeansTable();

snapp = FOREACH snap GENERATE tbid1, tphys, timenr;
snapp = distinct snapp; 
STORE snapp INTO ' NAME "tbid1, tphys, timenr only for nsg==1" ' using BeansTable();

uniqdsid = foreach snap generate dsid;
uniqdsid = distinct uniqdsid;
store uniqdsid into 'name="mocca survey5 uniq dsid"' using BeansTable();



-- name: aux plain
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = LOAD ' DATASETS="MOCCA plain" TABLES="snapshot"' USING BeansTable();
snap = foreach snap generate DSID(tbid) as dsid, tphys;
uniqdsid = distinct snap;
store uniqdsid into 'name="MOCCA plain uniq dsid"' using BeansTable();



-- name: aux h5
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = LOAD ' DATASETS="MOCCA h5" TABLES="snapshot"' USING BeansTable();
snap = foreach snap generate DSID(tbid) as dsid, tphys;
uniqdsid = distinct snap;
store uniqdsid into 'name="MOCCA plain uniq dsid"' using BeansTable();



-- name: aux h5
-- create: 2023-10-23T11:25:12.115+02:00
-- notebookId: 42f8613a3805abf4b69e543cb39b5ced
-- user: arkadiusz@hypki.net
snap = LOAD ' DATASETS="MOCCA h5" TABLES="snapshot" FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' USING BeansTable();

snap = FOREACH snap GENERATE 
							tbid AS tbid1, 
							DSID(tbid) as dsid,
                            tphys,
                            timenr, 
                            FLATTEN(mocca.Projection2D(r).$0) AS (r:double),
                            sm1+sm2 AS sm, 
                            lum1+lum2 AS lum, 
                            0.0 AS cumSmR,
                            0.0 AS cumLumR;
                                            
STORE snap INTO ' NAME "survey5 selected snapshots rows only" ' using BeansTable();

snapp = FOREACH snap GENERATE tbid1, tphys, timenr;
snapp = distinct snapp; 
STORE snapp INTO ' NAME "tbid1, tphys, timenr only for nsg==1" ' using BeansTable();

uniqdsid = foreach snap generate dsid;
uniqdsid = distinct uniqdsid;
store uniqdsid into 'name="mocca survey5 uniq dsid"' using BeansTable();



-- name: Just reading from some csv file (TEZ_LOCAL)
-- create: 2023-10-19T13:41:40.954+02:00
-- notebookId: b75991789f49f330a8706dfecf0f8735
-- user: arkadiusz@hypki.net
a = LOAD '/home/ahypki/temp/mocca/data1.csv' USING PigStorage (',') AS (f1:int, f2:int, f3:int, f4:int);
a = filter a by f1 == 77;
store a into 'name="just a"' using BeansTable();


-- name: Just reading from some csv file (TEZ)
-- create: 2023-10-19T13:41:40.954+02:00
-- notebookId: b75991789f49f330a8706dfecf0f8735
-- user: arkadiusz@hypki.net
a = LOAD '/data1.csv' USING PigStorage (',') AS (f1:int, f2:int, f3:int, f4:int);
a = filter a by f1 == 77;
store a into 'name="just a"' using BeansTable();


-- name: Just reading from some csv file (TEZ_LOCAL)
-- create: 2023-10-19T13:41:40.954+02:00
-- notebookId: b75991789f49f330a8706dfecf0f8735
-- user: arkadiusz@hypki.net
a = LOAD '/home/ahypki/temp/mocca/data1.csv' USING net.beanscode.model.cass.BeansTable2 (',') AS (f1:int, f2:int, f3:int, f4:int);
a = filter a by f1 == 77;
store a into 'name="just a"' using BeansTable();


-- name: Just reading from some csv file (TEZ)
-- create: 2023-10-19T13:41:40.954+02:00
-- notebookId: b75991789f49f330a8706dfecf0f8735
-- user: arkadiusz@hypki.net
a = LOAD '/data1.csv' USING net.beanscode.model.cass.BeansTable2 (',') AS (f1:int, f2:int, f3:int, f4:int);
a = filter a by f1 == 77;
store a into 'name="just a"' using BeansTable();


-- name: Reading 16GB data1.csv file from hdfs (TEZ)
-- create: 2023-10-19T13:41:40.954+02:00
-- notebookId: b75991789f49f330a8706dfecf0f8735
-- user: arkadiusz@hypki.net
a = LOAD '/data2.csv' USING net.beanscode.model.cass.BeansTable2 (',') AS (f1:int, f2:int, f3:int, f4:int, f5:int, f6:int, f7:int, f8:int, f9:int, f10:int, f11:int, f12:int, f13:int, f14:int, f15:int, f16:int, f17:int, f18:int, f19:int, f20:int, f21:int, f22:int, f23:int, f24:int, f25:int, f26:int, f27:int, f28:int, f29:int, f30:int, f31:int, f32:int, f33:int, f34:int, f35:int, f36:int, f37:int, f38:int, f39:int, f40:int, f41:int, f42:int, f43:int, f44:int, f45:int, f46:int, f47:int, f48:int, f49:int, f50:int, f51:int, f52:int, f53:int, f54:int, f55:int, f56:int, f57:int, f58:int, f59:int, f60:int, f61:int, f62:int, f63:int, f64:int, f65:int, f66:int, f67:int, f68:int, f69:int, f70:int, f71:int, f72:int, f73:int, f74:int, f75:int, f76:int, f77:int, f78:int, f79:int, f80:int, f81:int, f82:int, f83:int, f84:int, f85:int, f86:int, f87:int, f88:int, f89:int, f90:int, f91:int, f92:int, f93:int, f94:int, f95:int, f96:int, f97:int, f98:int, f99:int, f100:int);
a = filter a by f1 == 88;
store a into 'name="just a"' using BeansTable();


-- name: Now, reading 10GB snapshot from mocca.h5 (TEZ)
-- create: 2023-10-19T13:41:40.954+02:00
-- notebookId: b75991789f49f330a8706dfecf0f8735
-- user: arkadiusz@hypki.net
a = LOAD 'datasets="mocc 942" tables="snapshot"' USING BeansTable();
a = filter a by idd1 == 88;
store a into 'name="just a"' using BeansTable();


-- name: sum of columns
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 plain==1" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;

sys = load 'datasets="mocca survey==1 gzip==1" tables="system"' using BeansTable();
sysGr = group sys all;
sys2 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 2 as type;

sys = load 'datasets="mocca survey==1 h5==1" tables="system"' using BeansTable();
sysGr = group sys all;
sys3 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 3 as type;

sys = union sys1, sys2, sys3;
store sys into 'name="sys survey1"' using BeansTable();




-- name: sum of columns - survey1, plain, system
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 plain" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="sys survey1 plain"' using BeansTable();




-- name: sum of columns - survey1, plain, snapshot
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 plain" tables="snapshot"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="snapshot survey1 plain"' using BeansTable();




-- name: sum of columns - survey1, gzip, system
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 gzip" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="sys survey1 gzip"' using BeansTable();




-- name: sum of columns - survey1, gzip, snapshot
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 gzip" tables="snapshot"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="snapshot survey1 gzip"' using BeansTable();




-- name: sum of columns - survey1, h5, system
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 h5" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="sys survey1 h5"' using BeansTable();




-- name: sum of columns - survey1, h5, snapshot
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==1 h5" tables="snapshot"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="snapshot survey1 h5"' using BeansTable();




-- name: sum of columns - survey5, plain, system
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==5 plain" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="sys survey5 plain"' using BeansTable();




-- name: sum of columns - survey5, plain, snapshot
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==5 plain" tables="snapshot"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="snapshot survey5 plain"' using BeansTable();




-- name: sum of columns - survey5, plain, snapshot, with filter
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
snap = load 'datasets="mocca survey==5  plain" tables="snapshot" FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' using BeansTable();
snapGr = group snap all;
snap1 = foreach snapGr generate SUM(snap.tphys) as tphysSum, 1 as survey, 1 as type;

snap = load 'datasets="mocca survey==5  gzip" tables="snapshot" FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' using BeansTable();
snapGr = group snap all;
snap2 = foreach snapGr generate SUM(snap.tphys) as tphysSum, 1 as survey, 2 as type;

snap = load 'datasets="mocca survey==5  h5" tables="snapshot" FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' using BeansTable();
snapGr = group snap all;
snap3 = foreach snapGr generate SUM(snap.tphys) as tphysSum, 1 as survey, 3 as type;

snap = union snap1, snap2, snap3;
store snap into 'name="snap survey5 plain"' using BeansTable();




-- name: sum of columns - survey5, gzip, system
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==5 gzip" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="sys survey5 gzip"' using BeansTable();




-- name: sum of columns - survey5, gzip, snapshot
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==5 gzip" tables="snapshot"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="snapshot survey5 gzip"' using BeansTable();




-- name: sum of columns - survey5, h5, system
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==5 h5" tables="system"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="sys survey5 h5"' using BeansTable();




-- name: sum of columns - survey5, h5, snapshot
-- create: 2023-03-27T17:10:00.732+02:00
-- notebookId: a3c17aaa9d37f45a87bc9765a6375a34
-- user: arkadiusz@hypki.net
sys = load 'datasets="mocca survey==5 h5" tables="snapshot"' using BeansTable();
sysGr = group sys all;
sys1 = foreach sysGr generate SUM(sys.tphys) as tphysSum, 1 as survey, 1 as type;
store sys1 into 'name="snapshot survey5 h5"' using BeansTable();




-- name: simple query
-- create: 2023-02-06T09:15:08.336+01:00
-- notebookId: 718b3d4fed721118c5afdf50ac089ec7
-- user: arkadiusz@hypki.net
sys	= load 'datasets="mocca n_1==24000" tables="system"' using BeansTable();

sys = filter sys by tphys < 400.0;

store sys into 'name="sys400"' using BeansTable();


-- name: Core radii for survey==1
-- create: 2023-10-03T07:20:13.464+02:00
-- notebookId: 7140f16f50394e1d21e5e45bd91d60c9
-- user: arkadiusz@hypki.net
sys = load 'datasets="survey==2" tables="snapshot"' using BeansTable();
sys = order sys by idd1;
store sys into 'name="survey2 rc"' using BeansTable();


-- name: dd
-- create: 2023-02-20T12:38:27.037+01:00
-- notebookId: e9b78820ef1f3c35a77917535e6a3b23
-- user: arkadiusz@hypki.net
snap = load 'datasets="mocca" tables="system"' using BeansTable();
snap = foreach snap generate tphys, 
				(tphys > 100.0 ? 'main sequence' : 'not main sequence') as type;
store snap into 'name="snapsiu"' using BeansTable();


-- name: pig title774
-- create: 2023-02-03T12:25:28.792+01:00
-- notebookId: 28a7f1de20f139c6316bd217cf6d02a5
-- user: arkadiusz@hypki.net



-- name: Additional lagrangian radii (survey5, projection, selected snapshots)
-- create: 2023-04-18T23:10:38.123+02:00
-- notebookId: 58cefe521a00a01244c9d9d6c82d047b
-- user: arkadiusz@hypki.net
snap = LOAD ' DATASETS="MOCCA" TABLES="snapshot" FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' USING BeansTable();

snap = FOREACH snap GENERATE tbid AS tbid1, 
                                            timenr, 
                                            FLATTEN(mocca.Projection2D(r).$0) AS (r:double),
                                            sm1+sm2 AS sm, 
                                            lum1+lum2 AS lum, 
                                            0.0 AS cumSmR,
                                            0.0 AS cumLumR;

snapByTime = GROUP snap BY (tbid1, timenr);

snap = FOREACH snapByTime {
     C1 = ORDER snap BY r ASC;
     GENERATE FLATTEN(CUMULATIVE(C1, 'sm', 'cumSmR', 'lum', 'cumLumR')) AS (tbid1:chararray, timenr:int, r:double, sm:double, lum:double, cumSmR:double, cumLumR:double),
          SUM(snap.sm) AS totm,
          SUM(snap.lum) AS totl;
}

snap = FOREACH snap GENERATE tbid1,
                                                 timenr,
                                                 (cumSmR < 0.01 * totm ? r : 0.0) AS lagrSmR1,  
                                                 (cumSmR < 0.1  * totm ? r : 0.0) AS lagrSmR10,
                                                 (cumSmR < 0.2  * totm ? r : 0.0) AS lagrSmR20,
                                                 (cumSmR < 0.3  * totm ? r : 0.0) AS lagrSmR30,
                                                 (cumSmR < 0.4  * totm ? r : 0.0) AS lagrSmR40,
                                                 (cumSmR < 0.5  * totm ? r : 0.0) AS lagrSmR50,
                                                 (cumSmR < 0.6  * totm ? r : 0.0) AS lagrSmR60,
                                                 (cumSmR < 0.7  * totm ? r : 0.0) AS lagrSmR70,
                                                 (cumSmR < 0.8  * totm ? r : 0.0) AS lagrSmR80,
                                                 (cumSmR < 0.9  * totm ? r : 0.0) AS lagrSmR90,
                                                 (cumLumR < 0.01 * totl ? r : 0.0) AS lagrLumR1,  
                                                 (cumLumR < 0.1  * totl ? r : 0.0) AS lagrLumR10,
                                                 (cumLumR < 0.2  * totl ? r : 0.0) AS lagrLumR20,
                                                 (cumLumR < 0.3  * totl ? r : 0.0) AS lagrLumR30,
                                                 (cumLumR < 0.4  * totl ? r : 0.0) AS lagrLumR40,
                                                 (cumLumR < 0.5  * totl ? r : 0.0) AS lagrLumR50,
                                                 (cumLumR < 0.6  * totl ? r : 0.0) AS lagrLumR60,
                                                 (cumLumR < 0.7  * totl ? r : 0.0) AS lagrLumR70,
                                                 (cumLumR < 0.8  * totl ? r : 0.0) AS lagrLumR80,
                                                 (cumLumR < 0.9  * totl ? r : 0.0) AS lagrLumR90
                                                 ;

snapByTime = GROUP snap BY (tbid1, timenr);

snap = FOREACH snapByTime GENERATE group.tbid1 AS tbid1,
                       group.timenr AS timenr,
                       MAX(snap.lagrSmR1)  AS lagrSmR1,
                       MAX(snap.lagrSmR10) AS lagrSmR10,
                       MAX(snap.lagrSmR20) AS lagrSmR20,
                       MAX(snap.lagrSmR30) AS lagrSmR30,
                       MAX(snap.lagrSmR40) AS lagrSmR40,
                       MAX(snap.lagrSmR50) AS lagrSmR50,
                       MAX(snap.lagrSmR60) AS lagrSmR60,
                       MAX(snap.lagrSmR70) AS lagrSmR70,
                       MAX(snap.lagrSmR80) AS lagrSmR80,
                       MAX(snap.lagrSmR90) AS lagrSmR90,
                       
                       MAX(snap.lagrLumR1)  AS lagrLumR1,
                       MAX(snap.lagrLumR10) AS lagrLumR10,
                       MAX(snap.lagrLumR20) AS lagrLumR20,
                       MAX(snap.lagrLumR30) AS lagrLumR30,
                       MAX(snap.lagrLumR40) AS lagrLumR40,
                       MAX(snap.lagrLumR50) AS lagrLumR50,
                       MAX(snap.lagrLumR60) AS lagrLumR60,
                       MAX(snap.lagrLumR70) AS lagrLumR70,
                       MAX(snap.lagrLumR80) AS lagrLumR80,
                       MAX(snap.lagrLumR90) AS lagrLumR90
                      ;

STORE snap INTO ' NAME "Additional lagrangian radii (survey5, projection, selected snapshots)" ' using BeansTable();


-- name: Fraction of FG in various distance with respect to R_hob (Survey5 only, radii projected) 
-- create: 2023-04-18T23:10:38.123+02:00
-- notebookId: 58cefe521a00a01244c9d9d6c82d047b
-- user: arkadiusz@hypki.net
sys = LOAD 'DATASETS="mocca" TABLES="system"' USING BeansTable();

aux = LOAD 'DATASETS="Auxuliary MOCCA tables" TABLES="Additional lagrangian radii survey5 projection selected snapshots 93ad6810" ' USING BeansTable();
aux = FOREACH aux GENERATE 
                    *, 
                    DSID(tbid1) as dsid; 
          
-- getting some global values from system.dat
sys = FOREACH sys GENERATE 
                    tbid as tbidSys, 
                    DSID(tbid) as dsidSys, 
                    timenr, 
                    r_h, 
                    rhob,
                    rtid,
                    sturn,
                    sturnm,
                    smt,
                    pop1oc,
                    pop2oc,
                    pop1b,
                    pop2b;

-- getting some values from system.dat at T=0 and attaching it to system.dat
sys0 = FILTER sys BY timenr == 0;
sys = JOIN sys BY dsidSys, sys0 BY dsidSys;
sys = FOREACH sys GENERATE sys::tbidSys as tbidSys, 
                    sys::dsidSys    as dsidSys, 
                    sys::timenr         as timenr, 
                    sys::r_h            as r_h, 
                    sys::rhob           as rhob,
                    sys::rtid           as rtid,
                    sys::sturn          as sturn,
                    sys::sturnm         as sturnm,
                    sys::smt            as smt,
                    sys0::pop1oc        as pop1oc0,
                    sys0::pop2oc        as pop2oc0,
                    sys0::pop1b         as pop1b0,
                    sys0::pop2b         as pop2b0;
                    
-- getting some values from snapshots
snap = LOAD 'DATASETS="mocca survey5" TABLES="snapshot"   FILTER="(timenr == 0) OR (tphys>400.0 tphys<430.0) OR (tphys>1000.0 tphys<1030.0) OR (tphys>2000.0 tphys<2030.0) OR (tphys>5000.0 tphys<5030.0) OR (tphys>7000.0 tphys<7030.0) OR (tphys>10000.0 tphys<10030.0) OR (tphys>12000.0 tphys<12030.0) " ' USING BeansTable();

snap = FOREACH snap GENERATE 
                    tbid as tbidSnap, 
                    DSID(tbid) as dsidSnap, 
                    timenr, 
                    tphys, 
                    r * SQRT(1 - POW(RANDOM(), 2.0)) as r,
                    ik1, 
                    ik2, 
                    sm1, 
                    sm2, 
                    popId1,
                    popId2,
                    idd1;

-- joining snapshot.dat with system.dat
snapSys = JOIN snap BY (dsidSnap, timenr), sys BY (dsidSys, timenr);
          
-- searching for stars and binaries from POP1 and POP2, and taking some values from system.dat files
snap = FOREACH snapSys GENERATE snap::tbidSnap as tbidSnap,
                    snap::dsidSnap as dsid,
                    snap::timenr as timenr,
                    snap::tphys as tphys,
                    snap::r     as r,
                    snap::idd1  as idd1,
                    (snap::popId1 == 1 ? 1 : 0)                                         AS pop11, 
                    (snap::popId1 == 1 AND snap::ik1 <= 1 ? 1 : 0)      AS pop11ms, 
                    (snap::popId1 == 2 ? 1 : 0)                                         AS pop12, 
                    (snap::popId1 == 2 AND snap::ik1 <= 1 ? 1 : 0)      AS pop12ms, 
                    (snap::popId2 == 1 ? 1 : 0)                                         AS pop21, 
                    (snap::popId2 == 1 AND snap::ik1 <= 1 ? 1 : 0)      AS pop21ms, 
                    (snap::popId2 == 2 ? 1 : 0)                                         AS pop22, 
                    (snap::popId2 == 2 AND snap::ik1 <= 1 ? 1 : 0)      AS pop22ms, 
                    (snap::popId1 == 1 AND snap::popId2 == 1 ? 1 : 0)                                           AS binpop1,
                    (snap::popId1 == 1 AND snap::popId2 == 1 
                        AND snap::ik1 <= 1 AND snap::ik2 <= 1 
                        AND MAX2(snap::sm1, snap::sm2) > 0.4 
                        AND MIN2(snap::sm1, snap::sm2)/MAX2(snap::sm1, snap::sm2) > 0.5
                        AND MAX2(snap::sm1, snap::sm2) <= sys::sturn 
                        ? 1 : 0)                                                                                                                        AS binObsPop1,
                    (snap::popId1 == 1 AND snap::popId2 == 1 
                        AND snap::ik1 <= 1 AND snap::ik2 <= 1 
                        AND snap::sm1 > 0.4 AND snap::sm2 > 0.4 
                        AND MAX2(snap::sm1, snap::sm2) <= sys::sturn 
                        ? 1 : 0)                                                                                                                        AS binObsMSPop1,
                    (snap::popId1 == 1 AND snap::popId2 == 1 
                        AND snap::ik1 <= 1 AND snap::ik2 <= 1 ? 1 : 0)                                          AS binpop1ms,
                    (snap::popId1 == 2 AND snap::popId2 == 2 ? 1 : 0)                                           AS binpop2,
                    (snap::popId1 == 2 AND snap::popId2 == 2 
                        AND snap::ik1 <= 1 AND snap::ik2 <= 1 
                        AND MAX2(snap::sm1, snap::sm2) > 0.4 
                        AND MIN2(snap::sm1, snap::sm2)/MAX2(snap::sm1, snap::sm2) > 0.5
                        AND MAX2(snap::sm1, snap::sm2) <= sys::sturn 
                        ? 1 : 0)                                                                                                                        AS binObsPop2,
                    (snap::popId1 == 2 AND snap::popId2 == 2 
                        AND snap::ik1 <= 1 AND snap::ik2 <= 1 
                        AND snap::sm1 > 0.4 AND snap::sm2 > 0.4 
                        AND MAX2(snap::sm1, snap::sm2) <= sys::sturn 
                        ? 1 : 0)                                                                                                                        AS binObsMSPop2,
                    (snap::popId1 == 2 AND snap::popId2 == 2 
                        AND snap::ik1 <= 1 AND snap::ik2 <= 1 ? 1 : 0)                                          AS binpop2ms,
                    (snap::popId1 == 1 AND snap::popId2 == 0 ? 1 : 0)                                           AS sinpop1,
                    (snap::popId1 == 1 AND snap::popId2 == 0 AND snap::ik1 <= 1 ? 1 : 0)        AS sinpop1ms,
                    (snap::popId1 == 1 
                                                AND snap::popId2 == 0 
                                                AND snap::ik1 <= 1
                                                AND snap::sm1 > 0.4 ? 1 : 0)                                                                            AS sinpop1obs,
                    (snap::popId1 == 2 AND snap::popId2 == 0 ? 1 : 0)                                           AS sinpop2,
                    (snap::popId1 == 2 AND snap::popId2 == 0 AND snap::ik1 <= 1 ? 1 : 0)        AS sinpop2ms,
                    (snap::popId1 == 2 
                                                AND snap::popId2 == 0 
                                                AND snap::ik1 <= 1
                                                AND snap::sm1 > 0.4 ? 1 : 0)                                                                            AS sinpop2obs,
                    sys::r_h    as r_h,
                    sys::rhob   as rhob,
                    sys::rtid   as rtid,
                    sys::sturn  as sturn,
                    sys::sturnm as sturnm,
                    sys::smt    as smt,
                    sys::pop1oc0 as pop1oc0,
                    sys::pop2oc0 as pop2oc0,
                    sys::pop1b0         as pop1b0,
                    sys::pop2b0         as pop2b0;

snap = foreach snap generate *,
                    (int)((r/rhob)/0.1) as bin:int;

snapGr = GROUP snap BY (tbidSnap, timenr, bin);
snapFl = FOREACH snapGr GENERATE 
                    SUM(snap.pop11)     + SUM(snap.pop21)       as pop1c, 
                    SUM(snap.pop11ms)   + SUM(snap.pop21ms)     as pop1cms, 
                    SUM(snap.pop12)     + SUM(snap.pop22)   as pop2c, 
                    SUM(snap.pop12ms)   + SUM(snap.pop22ms) as pop2cms, 
                    SUM(snap.binpop1)           as binpop1c, 
                    SUM(snap.binObsPop1)        as binObsPop1c, 
                    SUM(snap.binObsMSPop1)      as binObsMSPop1c, 
                    SUM(snap.binpop1ms)         as binpop1cms, 
                    SUM(snap.binpop2)           as binpop2c, 
                    SUM(snap.binObsPop2)        as binObsPop2c, 
                    SUM(snap.binObsMSPop2)      as binObsMSPop2c, 
                    SUM(snap.binpop2ms)         as binpop2cms, 
                    SUM(snap.sinpop1)           as sinpop1c, 
                    SUM(snap.sinpop1ms)         as sinpop1cms,
                    SUM(snap.sinpop1obs)        as sinpop1obs, 
                    SUM(snap.sinpop2)           as sinpop2c, 
                    SUM(snap.sinpop2ms)         as sinpop2cms, 
                    SUM(snap.sinpop2obs)        as sinpop2obs, 
                    MIN(snap.idd1)                      as minId, 
                    FLATTEN(snap);
snapFl = FILTER snapFl BY snap::idd1 == minId;

snapFl = FOREACH snapFl GENERATE pop1c, 
                                        pop1cms, 
                    pop2c, 
                    pop2cms, 
                    binpop1c, 
                    binObsPop1c, 
                    binObsMSPop1c, 
                    binpop1cms, 
                    binpop2c, 
                    binObsPop2c, 
                    binObsMSPop2c, 
                    binpop2cms, 
                    sinpop1c, 
                    sinpop1cms,
                    sinpop1obs, 
                    sinpop2c, 
                    sinpop2cms,
                    sinpop2obs, 
                    minId, 
                                        bin,
                    snap::tbidSnap as tbidSnap, 
                                        DSID(snap::tbidSnap) as dsid, 
                    snap::timenr as timenr, 
                    FLOOR(snap::tphys, 100.0) as tphys, 
                    snap::idd1 as idd1,
                    snap::bin           as bin,
                    snap::r_h           as r_h,
                    snap::rhob          as rhob,
                    snap::rtid          as rtid,
                    snap::sturn         as sturn,
                    snap::sturnm        as sturnm,
                    snap::smt           as smt,
                    snap::pop1oc0       as pop1oc0,
                    snap::pop2oc0       as pop2oc0,
                    snap::pop1b0        as pop1b0,
                    snap::pop2b0        as pop2b0;
                    

snapFl = order snapFl BY dsid, timenr, bin;
                    
STORE snapFl INTO 'NAME="FG fraction survey5 all (radii projected)"   ' USING BeansTable();


