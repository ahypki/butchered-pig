# Butchered Pig

[- This code is under heavy development, please do not use it yet -]

Tired of having optimisation problems with Apache Pig and decided to try my own, simple, but powerfull implementation. Once `butchered-pig` first public version is finished it will be able to analyse in distributed way terabytes of data on a single low-cost machine. 
